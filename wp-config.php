<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wildcoffee');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'RXQos}%4e^)kiOqX=$9-c3}ck&@)r6+5loavj+}9Oys#IDI$Z^OYv$I*c(kawRK$');
define('SECURE_AUTH_KEY',  'O:Gf4<!~]^etc?>FQ:!5FHMsmX*hnR,&0*4paW$|txTEw+YtzJ2f}D^5PmB[xhz/');
define('LOGGED_IN_KEY',    ':`&Hrwv5Pi=K`V4N(<9Mb9[t1(1U+S:4GU3bD9>P*Lq`K5K$lrS=x5(OlX*mk@nW');
define('NONCE_KEY',        'jB>&0_@gz>$TKKSF 7j3*N)2i`HoHG3]-$:u7JktZB}I`tvW=%Pkj!CSK|VmNRpu');
define('AUTH_SALT',        'zk0zK=Pn$!/{z*|85W]ocEZ&>Wcwpa$W}A5J^`q.TsN^0aqk:6n5+,BN]C()!N+$');
define('SECURE_AUTH_SALT', 'c(S#:`@/?Z*97aww/_^W@0ac!^7XJ8m|doZIzEUK)*Tb:c?g()/k[4~Y~bvqKB#:');
define('LOGGED_IN_SALT',   '))PA2#jf%QR ||p=Ig{ZH,ry/%|g27.s,0mm8n>7(^9tExxwMF(@Xq%wPp@HKCMa');
define('NONCE_SALT',       '^q/0d#lsJtPVM,2A84wE&0he;sa4~=E%u/_P+S;WjI`jbk~<Z/B5W/.^zCtNB>F<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
