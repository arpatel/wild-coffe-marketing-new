<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php do_shortcode('[slider]'); ?>

<?php the_field( 'why_choose_title' ); ?>
<?php the_field( 'how_it_work_title' ); ?>
<?php the_field( 'short_descriptions' ); ?>
<?php if ( have_rows( 'steps' ) ) : ?>
	<ul>
		<?php while ( have_rows( 'steps' ) ) : the_row(); ?>
			<li>
				<?php the_sub_field( 'step_name' ); ?>
				<?php the_sub_field( 'sub_title' ); ?>
				<?php the_sub_field( 'step_description' ); ?>
				<a href="<?php the_sub_field( 'button_link' ); ?>"><?php the_sub_field( 'button_text' ); ?></a>	
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>

<?php the_field( 'testimonials' ); ?>
<?php the_field( 'testimonials_description' ); ?>
<?php
$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 8, 'orderby' => 'ID', 'order' => 'DESC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ ?>
<div class="testimonials_slider">
    <ul id="testimonials_slider" >
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li>  
        	<?php $img = get_the_post_thumbnail($recent['ID'], 'full'); if($img != ''){ echo $img;  } else{?> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/banner.jpg"> <?php } ?>
        	<?php the_title() ?>
        	<a href="<?php the_field( 'website_link' ); ?>"><?php echo str_replace(array('http://', 'https://'), ' ', get_field( 'website_link' ));  ?></a>
        <?php the_content(); ?>
        </li>
    <?php  endwhile;?>
    </ul>
</div>
<?php }
wp_reset_query(); ?>

<?php get_footer();
