<?php
/**
 * Template Name: Bio Page
 *
 **/ 

global $shortnametheme;
 
get_header(); ?>



<div class="middle-container">

<div class="breadcrumb-container">
<div class="container">
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>">Home</a> </li>
        <li><?php the_title(); ?></li>
    </ol>
</div>
</div>
<div class="page-title-wrapper">
<div class="container">
<h1><?php the_title(); ?></h1>
</div>
</div>


<div class="bio-container">
    <div class="container">
<div class="col-2">
    <div class="col-lft">
<?php while ( have_posts() ) : the_post();
 the_content();
endwhile; ?>

</div>
    <div class="col-rgt">
        <?php get_sidebar(); ?>
        <?php //dynamic_sidebar( 'blog_right_sidebar' ); ?>
    </div>
</div>
</div>    
</div>
</div>

<?php get_footer(); ?>