<?php
/**
 * Template Name: Login Page
 *
 **/
get_header(); ?>
<div class="middle-container">
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>">Home</a> </li>
            <li><?php echo the_title(); ?></li>
        </ol>
    </div>
</div>
<div class="page-title-wrapper">
<div class="container">
<h1><?php echo the_title(); ?></h1>
</div>
</div>
<div class="content-div">
    <div class="container">
        <div class="login-div">            
            <?php
            while ( have_posts() ) : the_post();

                the_content(); 

            endwhile; // End of the loop.
            ?>
        </div>
    </div>
</div>
</div>
<?php get_footer();
