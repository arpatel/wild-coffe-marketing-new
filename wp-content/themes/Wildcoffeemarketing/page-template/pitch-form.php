<?php
/**
 * Template Name: Pitch Post Form
 *
 **/
get_header(); ?>

<div class="middle-container">
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>">Home</a> </li>
            <li><?php echo the_title(); ?></li>
        </ol>
    </div>
</div>
<div class="page-title-wrapper">
<div class="container">
<h1><?php echo the_title(); ?></h1>
</div>
</div>
<div class="content-div">
    <div class="container">
        <div class="pitch-post-div">  
        <?php 
        if(is_user_logged_in() && current_user_can('author')) 
        {
            if( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['pitch_type'] == 'add_pitch' && $_POST['title'] != '' && $_POST['description'] != '' )  
            { 
              if (isset ($_POST['title'])) { $title =  $_POST['title']; } else { echo 'Please enter a title'; }
              if (isset ($_POST['description'])) { $description = $_POST['description']; } else { echo 'Please enter the content'; }
              
              $tags = trim( $_POST['post_tags'] );
              $genre =  $_POST['genre'];
              $type =  $_POST['type'];
              $count =  $_POST['count'];
              $post = array(
                  'post_title'    => $title,
                  'post_content'  => $description,
                //  'tags_input'    => $tags,
                  'post_status'   => 'publish', // Choose: publish, preview, future, etc.
                  'post_type'     => 'pitch' 
              );
             
              // wp_insert_post($post);  

              $post_id = wp_insert_post( $post ); 
              wp_set_object_terms( $post_id, $genre, 'genre' ); 
              wp_set_object_terms( $post_id, $type, 'type' ); 
              wp_set_object_terms( $post_id, $count, 'count' ); 
               if($post_id != '')
              {
                echo '<div class="pitch-message">Your "'.$_POST['title'].'" Pitch has been added successfully.</div>';
              }
            } 

            if( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['edit_pitch'] == 'update_pitch' && $_POST['post_id'] != '' )  
            {      
              $genre =  $_POST['genre'];
              $type =  $_POST['type'];
              $count =  $_POST['count'];
              $update_pitch = array(
              'ID'           => $_POST['post_id'],
              'post_title'   => $_POST['title'],
              'post_content' => $_POST['description']);

              $post_id =  wp_update_post( $update_pitch );
              wp_set_object_terms( $post_id, $genre, 'genre' ); 
              wp_set_object_terms( $post_id, $type, 'type' ); 
              wp_set_object_terms( $post_id, $count, 'count' ); 
              if($post_id != '')
              {
                echo '<div class="pitch-message">Your "'.$_POST['title'].'" Pitch has been updated.</div>';
              }
            }

            ?>          
            <div class="postbox">
              <form id="pitch_post" name="pitch_post" method="post" action="">
                <div class="field"><label>Title of Work*</label>
                  <div class="control">
                    <input type="text" value="" name="title" autocomplete="off"/>
                  </div>
                </div>

                <div class="field"><label>Genre*</label>
                  <div class="control">
                    <?php $args = array('taxonomy' => 'genre','orderby' => 'name','order'=> 'ASC','hide_empty' => false,);
                    $cats = get_categories($args);
                    $catlist = '';
                    $catlist .= '<select name="genre[]" id="genre" multiple class="gener-select">';
                    $catlist .= '<option value="">-- Select Genre --</option>';
                    foreach($cats as $cat) 
                    { 
                      $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>';
                    }
                    $catlist .= '</select>';
                    echo $catlist;
                    ?>
                    <small>For Windows: Press CTRL key + Mouse left Enter to select multiple genres.<br>For Mac: Press CMD key + Mouse right Enter to select multiple genres.</small>
                </div>
                </div>

                <div class="field"><label>Type*</label>
                <div class="control">
                  <?php $args = array('taxonomy' => 'type','orderby' => 'name','order'=> 'ASC','hide_empty' => false,);
                  $cats = get_categories($args);
                  $catlist = '';
                  $catlist .= '<select name="type" id="type">';
                  $catlist .= '<option value="">-- Select Type --</option>';
                  foreach($cats as $cat) 
                  { 
                    $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>';
                  }
                  $catlist .= '</select>';
                  echo $catlist;
                  ?>
                  </div>
                  </div>

                <div class="field"><label>Word Count*</label>
                <div class="control">
                <?php $args = array('taxonomy' => 'count','orderby' => 'id','order'=> 'ASC','hide_empty' => false,);
                $cats = get_categories($args);
                $catlist = '';
                $catlist .= '<select name="count" id="count">';
                $catlist .= '<option value="">-- Select Word Count --</option>';
                foreach($cats as $cat) 
                { 
                  $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>';
                }
                $catlist .= '</select>';
                echo $catlist;
                ?>
                </div>
                </div>
                <div class="field"><label for="description">Pitch Post (100 Words or Less)*</label>
                  <div class="control">
                    <textarea id="description" name="description" cols="50" rows="6" onkeyup="wordcounts(this.value);"></textarea>
                    <input type="text" name="words" id="word_count" size=4 readonly class="word-count" value="0">
                  </div>
                </div>

                <div class="form-submit"><input type="submit" value="Submit" id="pitch_submit" /></div>
                
                <input type="hidden" name="pitch_type" value="add_pitch" />
                <input type="hidden" name="action" value="post" />
                <?php wp_nonce_field( 'new-post' ); ?>
              </form>
            </div>
            <!-- Update pitch-->
            <?php 
            
            global $current_user;
            $args = array( 'post_type' => 'pitch', 'posts_per_page' => 100, 'author' =>  $current_user->ID, 'order' => 'DESC'/*, 'date_query' => array(array('after' => '24 hours ago')) */);
            $the_query = new WP_Query($args); ?>
            <?php if($the_query -> have_posts())
            { ?>
              <div class="edit-postbox-area">
                <h2>Edit Pitch Post</h2>
                <div class="edit-pitch-post" id="edit-pitch-post">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                      <div class="edit-postbox">
          			        <h3><span class="title"><span class="name"><?php the_title(); ?></span> <span class="edit">Edit</span> </span><?php if ($post->post_author == $current_user->ID) { ?>
                        <span class="delete"><a onclick="return confirm('Are you sure you want to delete this pitch?')" href="<?php echo get_delete_post_link( $post->ID ) ?>" title="Remove">X</a></span>
                       
                        <?php } ?></h3>
                        
                        <form id="update_post<?php echo $post->ID; ?>" name="update_post<?php echo $post->ID; ?>" method="post" action="">
                          <div class="field"><label>Title of Work*</label>
                            <div class="control">
                              <input type="text" value="<?php the_title(); ?>" name="title" autocomplete="off"/>
                            </div>
                          </div>

                          <div class="field"><label>Genre*</label>
                            <div class="control">
                              <?php 
                              $genre_terms = wp_get_post_terms( $post->ID, 'genre');
                             
                              $selected= 'selected="selected"';
                              $args = array('taxonomy' => 'genre','orderby' => 'name','order'=> 'ASC','hide_empty' => false,);
                              $cats = get_categories($args);
                              $catlist = '';
                              $catlist .= '<select name="genre[]" id="genre" multiple class="gener-select">';
                              $catlist .= '<option value="">-- Select Genre --</option>';
                              foreach($cats as $cat) 
                              { if(in_array($cat->term_id, array_column($genre_terms, 'term_id'))) {
                                  $catlist .= '<option value="'.$cat->name.'" '.$selected.'>'.$cat->name.'</option>';}else{
                                $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>'; }
                              }
                              $catlist .= '</select>';
                              echo $catlist;
                              ?>
                              <small>For Windows: Press CTRL key + Mouse left Enter to select multiple genres.<br>For Mac: Press CMD key + Mouse right Enter to select multiple genres.</small>
                          </div>
                          </div>

                          <div class="field"><label>Type*</label>
                          <div class="control">
                            <?php 
                            $type_terms = wp_get_post_terms( $post->ID, 'type');
                              $selected= 'selected';
                            $args = array('taxonomy' => 'type','orderby' => 'name','order'=> 'ASC','hide_empty' => false,);
                            $cats = get_categories($args);
                            $catlist = '';
                            $catlist .= '<select name="type" id="type">';
                            $catlist .= '<option value="">-- Select Type --</option>';
                            foreach($cats as $cat) 
                            {  
                              if($type_terms[0]->term_id == $cat->term_id){  $catlist .= '<option value="'.$cat->name.'" '.$selected.'>'.$cat->name.'</option>';}else{
                              $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>'; }
                            }
                            $catlist .= '</select>';
                            echo $catlist;
                            ?>
                            </div>
                            </div>

                          <div class="field"><label>Word Count*</label>
                          <div class="control">
                          <?php
                          $count_terms = wp_get_post_terms( $post->ID, 'count');
                              $selected= 'selected';
                           $args = array('taxonomy' => 'count','orderby' => 'id','order'=> 'ASC','hide_empty' => false,);
                          $cats = get_categories($args);
                          $catlist = '';
                          $catlist .= '<select name="count" id="count">';
                          $catlist .= '<option value="">-- Select Word Count --</option>';
                          foreach($cats as $cat) 
                          {  if($count_terms[0]->term_id == $cat->term_id){  $catlist .= '<option value="'.$cat->name.'" '.$selected.'>'.$cat->name.'</option>';}else{
                            $catlist .= '<option value="'.$cat->name.'">'.$cat->name.'</option>'; }
                          }
                          $catlist .= '</select>';
                          echo $catlist;
                          ?>
                          </div>
                          </div>
                          <div class="field"><label for="description">Pitch Post (100 Words or Less)*</label>
                            <div class="control">
                              <textarea id="description<?php echo $post->ID; ?>" name="description" cols="50" rows="6" onkeyup="wordcounts<?php echo $post->ID; ?>(this.value);"><?php $content = get_the_content();
                                echo strip_tags( $content ); ?></textarea>
                              <input type="text" name="words" id="word_count" size=4 readonly class="word-count" value="0">
                            </div>
                          </div>

                          <div class="form-submit"><input type="submit" value="Update" id="pitch_submit" /></div>
                          
                          <input type="hidden" name="edit_pitch" id="post_type" value="update_pitch" />
                          <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>" />
                          <input type="hidden" name="action" value="post" />
                          <?php wp_nonce_field( 'new-post' ); ?>
                        </form>

                        <script type="text/javascript">
                          function wordcounts<?php echo $post->ID; ?>(textarea)
                          {
                            var chars=textarea.length,
                            words=textarea.match(/\w+/g).length;
                            $('#update_post<?php echo $post->ID; ?> #word_count').val(words);
                            var counts = $('#update_post<?php echo $post->ID; ?> #word_count').val();
                            if(counts > 100)
                            {
                                $('#update_post<?php echo $post->ID; ?> #word_count').css('border', '1px solid #ff0000');
                            }
                             else
                            {
                                $('#update_post<?php echo $post->ID; ?> #word_count').css('border', '1px solid #008000');
                            }
                          }

                          $(document).ready(function () {  
                          $('#update_post<?php echo $post->ID; ?>').submit(function () {
                          
                                var $this     = $(this),
                                    $response = $('#response');
                                    $(".error").remove();
                                $response.find('p').remove();
                                if(document.update_post<?php echo $post->ID; ?>.title.value=="")
                                {       
                                     $(".error").remove();   
                                     $(document.update_post<?php echo $post->ID; ?>.title).after('<p class="error">Please enter Title</p>'); 
                                     document.update_post<?php echo $post->ID; ?>.title.focus();
                                     return false;  
                                     
                                }
                                if(document.update_post<?php echo $post->ID; ?>.genre.value=="")
                                {
                                     $(".error").remove();   
                                     $(document.update_post<?php echo $post->ID; ?>.genre).after('<p class="error">Please select Genre</p>'); 
                                     document.update_post<?php echo $post->ID; ?>.genre.focus();
                                     return false;  
                                     
                                }
                                if(document.update_post<?php echo $post->ID; ?>.type.value=="")
                                {
                                    $(".error").remove();
                                    $(document.update_post<?php echo $post->ID; ?>.type).after('<p class="error">Please select Type</p>');
                                    document.update_post<?php echo $post->ID; ?>.type.focus()
                                    return false;
                                }
                                if(document.update_post<?php echo $post->ID; ?>.count.value=="")
                                {
                                    $(".error").remove();
                                    $(document.update_post<?php echo $post->ID; ?>.count).after('<p class="error">Please select Word Count</p>');
                                    document.update_post<?php echo $post->ID; ?>.count.focus()
                                    return false;
                                }
                                if(document.update_post<?php echo $post->ID; ?>.description.value=="")
                                {
                                    $(".error").remove();
                                    $(document.update_post<?php echo $post->ID; ?>.description).after('<p class="error">Please enter pitch post</p>');
                                    document.update_post<?php echo $post->ID; ?>.description.focus()
                                    return false;
                                }
                                if(document.update_post<?php echo $post->ID; ?>.words.value > 100)
                                {
                                    $(".error").remove();
                                    $(document.update_post<?php echo $post->ID; ?>.description).after('<p class="error">You can enter pitch post up 100 words</p>');
                                    document.update_post<?php echo $post->ID; ?>.description.focus()
                                    return false;
                                }
                                return true;
                            }); 
                            });
                          </script>
                      </div>
                  <?php endwhile; ?>  
                </div>
              </div>
  <?php   }
        }
        else 
        {
          echo "<p>You don't have right to access this page.</p>";
        } ?>
        </div>
    </div>
</div>
</div>

<?php get_footer();


?>

