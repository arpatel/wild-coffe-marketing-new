<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php
/**
* Template Name: FAQ Page
*
**/
get_header(); ?>
<div class="middle-container">
<div class="breadcrumb-container">
<div class="container">
<ol class="breadcrumb">
<li><a href="<?php echo site_url(); ?>">Home</a> </li>
<li><?php echo the_title(); ?></li>
</ol>
</div>
</div>
<div class="page-title-wrapper">
<div class="container">
<h1><?php echo the_title(); ?></h1>
</div>
</div>
<div class="content-div">
<div class="container">
<div class="faq-div">            

<?php
// check if the repeater field has rows of data
if( have_rows('faq_repeater') ): ?>
<div id="accordion">
<?php  // loop through the rows of data
while ( have_rows('faq_repeater') ) : the_row(); ?>


<h3><?php the_sub_field('question'); ?></h3>
<div><?php the_sub_field('answer'); ?></div>


<?php
endwhile;
else :
// no rows found
?>
</div>
<?php
endif;
?>
</div>
</div>
</div>
</div>
<?php get_footer();
