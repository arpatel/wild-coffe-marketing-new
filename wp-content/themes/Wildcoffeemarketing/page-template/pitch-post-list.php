<?php
/**
 * Template Name: Pitch Post List
 *
 **/
get_header(); ?>

<div class="middle-container">
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>">Home</a> </li>
            <li><?php echo the_title(); ?></li>
        </ol>
    </div>
</div>
<div class="page-title-wrapper">
<div class="container">
<h1><?php echo the_title(); ?></h1>
</div>
</div>
<div class="content-div">
    <div class="container">
      <?php 
          if(is_user_logged_in()) 
          { ?>
      <form action="#" method="post" name="book-filter" id="book-filter">
        <?php
       // print_r($_POST);
         $args = array(
               'taxonomy' => 'genre',
               'orderby' => 'name',
               'order'   => 'ASC',
               'hide_empty' => false
           );

   $cats = get_categories($args);
   ?>
  <select name="genre" id="genre">
    <option value="-1">Select Genre</option>
    <?php foreach($cats as $cat) { ?>
      <option value="<?php echo $cat->term_id ?>" <?php if($_POST['genre'] == $cat->term_id ){ ?> selected<?php } ?>><?php echo $cat->name; ?></option>
    <?php } ?>
     </select>
<?php
     $args = array(
               'taxonomy' => 'type',
               'orderby' => 'name',
               'order'   => 'ASC',
               'hide_empty' => false
           );

   $cats = get_categories($args);
   ?>
  <select name="type" id="type">
    <option value="-1">Select Type</option>
    <?php foreach($cats as $cat) { ?>
      <option value="<?php echo $cat->term_id ?>" <?php if($_POST['type'] == $cat->term_id ){ ?> selected<?php } ?>><?php echo $cat->name; ?></option>
    <?php } ?>
     </select>
<?php
        
         $args = array(
               'taxonomy' => 'count',
               'orderby' => 'id',
               'order'   => 'ASC',
               'hide_empty' => false
           );

  $cats = get_categories($args);
   ?>
  <select name="count" id="count">
    <option value="-1">Select Word Counts</option>
    <?php foreach($cats as $cat) { ?>
      <option value="<?php echo $cat->term_id ?>" <?php if($_POST['count'] == $cat->term_id ){ ?> selected<?php } ?>><?php echo $cat->name; ?></option>
    <?php } ?>
     </select>

        <input type="text" name="keyword" value="<?php echo $_POST['keyword']; ?>" placeholder="Enter Keyword..." maxlength="50">
        <input type="submit" name="submit" value="Search">
        </form>
       <?php }?>
        <div class="pitch-post-list">  
          <?php 
          if(is_user_logged_in()) 
          { ?>

        <?php  global $wpdb;
            global $post;
            $args = array( 'post_type' => 'pitch', 'posts_per_page' => -1, 'order' => 'DESC');

            $genre  = isset($_REQUEST['genre']) && $_REQUEST['genre'] != '-1' ? $_REQUEST['genre'] : '';
            $type = isset($_REQUEST['type']) && $_REQUEST['type'] != '-1' ? $_REQUEST['type'] : '';
            $count = isset($_REQUEST['count']) && $_REQUEST['count'] != '-1' ? $_REQUEST['count'] : '';

            $querystr = "SELECT $wpdb->posts.* FROM $wpdb->posts WHERE ";
    
          if( !empty($_POST['keyword']) ){
                      $querystr .= "(post_title LIKE '%".$_POST['keyword']."%' OR post_content LIKE '%".$_POST['keyword']."%') AND ";
                   }
          if ( !empty( $genre ) ) {
              $querystr .= "EXISTS( SELECT 1 FROM $wpdb->term_relationships WHERE  $wpdb->posts.ID = $wpdb->term_relationships.object_id AND $wpdb->term_relationships.term_taxonomy_id IN ( '".$genre."' ) ) AND ";
          }   
          if ( !empty( $type ) ) {
             $querystr .= "EXISTS( SELECT 1 FROM $wpdb->term_relationships WHERE  $wpdb->posts.ID = $wpdb->term_relationships.object_id AND $wpdb->term_relationships.term_taxonomy_id IN ( '".$type."' ) ) AND ";
          } 
          if( !empty($count )){
              $querystr .= "EXISTS( SELECT 1 FROM $wpdb->term_relationships WHERE  $wpdb->posts.ID = $wpdb->term_relationships.object_id AND $wpdb->term_relationships.term_taxonomy_id IN ( '".$count."' ) ) AND ";
          }
              $querystr .= " $wpdb->posts.post_type = 'pitch' AND ($wpdb->posts.post_status = 'publish')
              GROUP BY $wpdb->posts.ID
              ORDER BY $wpdb->posts.post_date DESC";   
             
       $result = $wpdb->get_results($querystr);
       if ( $result ) {
       foreach ( $result as $post ) {
       // print_r($result);
                                setup_postdata($post);
                                
                           ?>
                <div class="blog-block" id="post-<?php the_ID(); ?>">
                  <div class="blog-listing">
                    <div class="post-left-area">
                      <div class="date-area">
                        <span class="date"><?php the_time('d'); ?></span>
                        <span class="month"><?php the_time('M'); ?></span>
                      </div>
                    </div>
                    <div class="post-rgt-area">
                      <div class="post-top">
                      <h2><?php the_title() ?></h2>
                      <?php 
                      $genre_terms = wp_get_post_terms( $post->ID, 'genre');
                      $type_terms = wp_get_post_terms( $post->ID, 'type');
                      $count_terms = wp_get_post_terms( $post->ID, 'count');?>
                      <div class="post-info-area">
                          <p>Genre: <?php $i= 1; foreach ($genre_terms as $genre) {
                            if($i != 1){echo ", ";} echo $genre->name;
                          $i++; }
                           ?></p>
                          <p>Type: <?php echo $type_terms[0]->name ?></p>
                          <p>Word Count: <?php echo $count_terms[0]->name ?></p>
                      </div>
                      <span class="post-content"><?php  the_content();  ?></span>
                      <div class="post-cmment-count <?php if(( get_comments_number()) || (current_user_can('agent')) ){echo 'pitch-cmt-toggle';} ?>"><?php comments_number('<span>0</span> Comment', '<span>1</span> Comment', '<span>%</span> Comments' );?></div>
                    </div>
                      <?php 
                    if ( comments_open() || get_comments_number() ) { ?>
                      <div class="comment-area"><?php comments_template(); ?></div>
                    <?php } ?>                  
                    </div>
                  </div>
                    <script type="text/javascript">
                    $(document).ready(function () {  
                    $('#post-<?php the_ID(); ?> #commentform').validate({

                    rules: {
                      author: {
                        required: true,
                        minlength: 2
                      },

                      email: {
                        required: true,
                        email: true
                      },
                      agency: {
                        required: true,
                      },
                      comment: {
                        required: true,
                        minlength: 20
                      },
                      
                    },

                    messages: {
                      author: "Please fill the required field",
                      email: "Please enter a valid email address.",
                      agency: "Please enter a agency name.",
                      comment: "Comments required minimum 20 characters.",
                    },

                    errorElement: "div",
                    errorPlacement: function(error, element) {
                      element.after(error);
                    }

                    });
                    });
                  </script>  
                </div>
              <?php 
            }
          } else { echo "<p>Sorry, No Pitch found in your criteria.</p>";}
          }

          else 
          {
            echo "<p>You don't have right to access this page.</p>";
          } ?>
        </div>

        <div class="pitch-authors-container">
        <div class="pitch-author-div side-block">
        <h2 class="sidebar-title">Newest Joined Authors</h2>
        <ul class="pitch-author-slider owl-carousel">
        <?php
        $authorusers = get_users(['orderby' => 'registered','order' => 'DESC', 'role__in' => [ 'author' ] ] );

        foreach ( $authorusers as $user ) { ?>    
        <li>
            <div class="auth-img">
                <span>
                    <?php echo get_avatar($user->ID, 124); ?>
                </span>
            </div>
            <?php echo '<div class="name">' . esc_html( $user->display_name ) . '</div>'; ?>
            </li>
        <?php }
        ?>
        </ul>
        </div>
        <!--<div class="pitch-agent-div side-block">
        <h2 class="sidebar-title">Latest Joined Agents</h2>
        <ul class="pitch-agent-slider owl-carousel">
        <?php
        $agentusers = get_users( [ 'orderby' => 'registered','order' => 'DESC', 'role__in' => [ 'agent' ] ] );
        foreach ( $agentusers as $user ) { ?>
        <li>
        <div class="auth-img">
                <span>
                    <?php echo get_avatar($user->ID, 124); ?>
                </span>
            </div>
        <?php echo '<div class="name">' . esc_html( $user->display_name ) . '</div>'; ?>
        </li>
        <?php }
        ?>
        </ul>
        </div>-->
        </div>
    </div>
  </div>
</div>

<?php get_footer();
