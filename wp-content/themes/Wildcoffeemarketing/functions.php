<?php
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'blisstheme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function my_site_default_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on blisstheme, use a find and replace
	 * to change 'blisstheme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'blisstheme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Middle Menu', 'blisstheme' ),
		'social'  => __( 'Top Menu', 'blisstheme' ),
		'footer'  => __( 'Footer Links Menu', 'blisstheme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */

}
endif; // blisstheme_setup
add_action( 'after_setup_theme', 'my_site_default_setup' );



//Widget sidebar
if (function_exists('register_sidebar')) {
    
    register_sidebar(array(
    'name' => 'Search',
    'id' => 'search',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => ''
    ));
}
	
//Theme Options
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'     => 'Theme General Settings',
        'menu_title'    => 'Theme General Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
    acf_add_options_sub_page(array(
        'page_title'     => 'Theme Header Settings',
        'menu_title'    => 'Header',
        'parent_slug'    => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title'     => 'Theme Footer Settings',
        'menu_title'    => 'Footer',
        'parent_slug'    => 'theme-general-settings',
    ));
}


add_filter('the_content', 'do_shortcode');
function homeurl_shortcode($atts, $content = null)
{
	$siteurl = get_template_directory_uri();
	return $siteurl;
}
add_shortcode('theme_url', 'homeurl_shortcode');
add_filter('widget_text', 'do_shortcode');
function baseurl_shortcode($atts, $content = null)
{
	$siteurl = get_site_url();
	return $siteurl;
}
add_shortcode('site_url', 'baseurl_shortcode');
add_filter('widget_text', 'do_shortcode');

function my_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

//Login Page Logo
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
        	background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg);
			margin-bottom: 0px;
			background-size: 100%;
			width: 100%;
			height: 100px;
        }
		body.login.login-action-login.wp-core-ui.locale-en-us{background: #F8F9FB}
		body.login #login {padding-top: 7%;}
		li#toplevel_page_functions .wp-menu-image.dashicons-before.dashicons-admin-generic{
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg);
		}
		body, html{height: auto;}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url_title() {
     return get_option('blogname'); 
} 
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

remove_filter('the_excerpt', 'wpautop');

function wpse_excerpt_length( $length ) {
    return 65;
}
add_filter( 'excerpt_length', 'wpse_excerpt_length', 999 );

function new_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


//Comment Form
function comment_form_new( $args = array(), $post_id = null ) {
    if ( null === $post_id )
        $post_id = get_the_ID();

    $commenter = wp_get_current_commenter();
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    $args = wp_parse_args( $args );
    if ( ! isset( $args['format'] ) )
        $args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

    $req      = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $html5    = 'html5' === $args['format'];
    $fields   =  array(
            'author' => '<div class="comment-form-top-lft"><div class="blog-row"><label><p>Name </p><span>*</span></label>' .
            '<input id="author" placeholder="Name*" class="name-field" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
            'email'  => '<div class="comment-form-email"><div class="blog-row"><label><p>Email </p><span>*</span></label>' .
            '<input id="email" placeholder="Email*" class="email-field" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
            'url' => '<div class="blog-row"><label>Website</label>' .
            '<input id="url" placeholder="Website" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .'"  /></div></div></div>',
    );

    $required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );

    /**
     * Filter the default comment form fields.
     *
     * @since 3.0.0
     *
     * @param array $fields The default comment fields.
    */
    $fields = apply_filters( 'comment_form_default_fields', $fields );
    $defaults = array(
            'fields'               => $fields,
            'comment_field'        => '<div class="comment-form-top-rgt"><div class="blog-row"><label><p>Your Comments </p><span>*</span></label><textarea id="comment" placeholder="Your Comments" name="comment" class="form-control" rows="3"  aria-required="true" required="required"></textarea></div></div>',
            /** This filter is documented in wp-includes/link-template.php */
            'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
            /** This filter is documented in wp-includes/link-template.php */
            'logged_in_as'         => '<div class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</div>',
            'comment_notes_before' => '<div class="comment-notes">' . __( 'Your email address will not be published.' ) . '</div>',
            'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
            'id_form'              => 'commentform',
            'id_submit'            => 'submit',
            'name_submit'          => 'submit',
            'title_reply'          => __( 'Add a comment' ),
            'title_reply_to'       => __( 'Leave a Reply to %s' ),
            'cancel_reply_link'    => __( 'Cancel reply' ),
            'label_submit'         => __( 'Post' ),
            'format'               => 'xhtml',
    );

    /**
     * Filter the comment form default arguments.
     *
     * Use 'comment_form_default_fields' to filter the comment fields.
     *
     * @since 3.0.0
     *
     * @param array $defaults The default comment form arguments.
    */
    $args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

    ?>
        <?php if ( comments_open( $post_id ) ) : ?>
            <?php
            /**
             * Fires before the comment form.
             *
             * @since 3.0.0
             */
            do_action( 'comment_form_before' );
            ?>
            <div id="respond"></div>
            <div id="respond2" class="comment-respond">
                <h3 id="reply-title" class="comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'], false ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3>
                <?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
                    <?php echo $args['must_log_in']; ?>
                    <?php
                    /**
                     * Fires after the HTML-formatted 'must log in after' message in the comment form.
                     *
                     * @since 3.0.0
                     */
                    do_action( 'comment_form_must_log_in_after' );
                    ?>
                <?php else : ?>
                <div class="add-comment-form">
                    <form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form"<?php echo $html5 ? ' novalidate' : ''; ?>>
                        <div class="comment-form-top"><?php
                        /**
                         * Fires at the top of the comment form, inside the <form> tag.
                         *
                         * @since 3.0.0
                         */
                        do_action( 'comment_form_top' );
                        ?>
                        <?php if ( is_user_logged_in() ) : ?>
                            <?php
                            /**
                             * Filter the 'logged in' message for the comment form for display.
                             *
                             * @since 3.0.0
                             *
                             * @param string $args_logged_in The logged-in-as HTML-formatted message.
                             * @param array  $commenter      An array containing the comment author's
                             *                               username, email, and URL.
                             * @param string $user_identity  If the commenter is a registered user,
                             *                               the display name, blank otherwise.
                             */
                            echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
                            ?>
                            <?php
                            /**
                             * Fires after the is_user_logged_in() check in the comment form.
                             *
                             * @since 3.0.0
                             *
                             * @param array  $commenter     An array containing the comment author's
                             *                              username, email, and URL.
                             * @param string $user_identity If the commenter is a registered user,
                             *                              the display name, blank otherwise.
                             */
                            do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
                            ?>
                        <?php else : ?>
                            <?php //echo $args['comment_notes_before']; ?>
                            <?php
                            /**
                             * Fires before the comment fields in the comment form.
                             *
                             * @since 3.0.0
                             */
                            do_action( 'comment_form_before_fields' );
                            foreach ( (array) $args['fields'] as $name => $field ) {
                                /**
                                 * Filter a comment form field for display.
                                 *
                                 * The dynamic portion of the filter hook, $name, refers to the name
                                 * of the comment form field. Such as 'author', 'email', or 'url'.
                                 *
                                 * @since 3.0.0
                                 *
                                 * @param string $field The HTML-formatted output of the comment form field.
                                 */
                                echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
                            }
                            /**
                             * Fires after the comment fields in the comment form.
                             *
                             * @since 3.0.0
                             */
                            do_action( 'comment_form_after_fields' );
                            ?>
                        <?php endif; ?>
                        <?php if ( is_user_logged_in() ) {?><div class="user-loged-commnt"><?php }
                        /**
                         * Filter the content of the comment textarea field for display.
                         *
                         * @since 3.0.0
                         *
                         * @param string $args_comment_field The content of the comment textarea field.
                         */
                        echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );
                        ?>
                        <?php if ( is_user_logged_in() ) {?></div><?php }?>
                        </div><?php //echo $args['comment_notes_after']; ?>
                        <div class="comment-form-btm">
                            <input class="btn btn-sm  btn-primary submit-button" name="<?php echo esc_attr( $args['name_submit'] ); ?>" type="submit" id="<?php echo esc_attr( $args['id_submit'] ); ?>" value="Add Comment" />
                            <?php comment_id_fields( $post_id ); ?>
                        </div>
                        <?php
                        /**
                         * Fires at the bottom of the comment form, inside the closing </form> tag.
                         *
                         * @since 1.5.0
                         *
                         * @param int $post_id The post ID.
                         */
                        do_action( 'comment_form', $post_id );
                        ?>
                    </form>
                    </div>
                <?php endif; ?>
            </div>
            <?php
            /**
             * Fires after the comment form.
             *
             * @since 3.0.0
             */
            do_action( 'comment_form_after' ); 
        else :
            /**
             * Fires after the comment form if comments are closed.
             *
             * @since 3.0.0
             */
            do_action( 'comment_form_comments_closed' ); 
        endif;
        
}
//Comment List display
function proui_comments($comment, $args, $depth) {
 $GLOBALS['comment'] = $comment; ?>
<li class="commnt">
    <div class="commnt-img">
        <a href="#" class="pull-left">
            <?php echo get_avatar($comment,$size='64',$default='' ); ?>
        </a>
    </div>    
    <div class="commnt-body">
        <?php if ($comment->comment_approved == '0') : ?>
            <p><em><?php _e('Your comment is awaiting moderation.') ?></em></p>
        <?php endif; ?>
        <span class="posted-time"><small><em><?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?></em></small></span>
        <span class="commnt-author-name"><strong><?php comment_author( $comment_ID ); ?></strong><?php edit_comment_link(__('Edit'),'&nbsp; ','') ?></span>
                <?php comment_text() ?>
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
</li>
<?php
 }
 

// Slider 
add_action('init', 'register_my_custom_post_type_slider');
function register_my_custom_post_type_slider()
{
    register_post_type('slider', array(
    'label' => 'Slider',
    'description' => 'This will allow you to add slider.',
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'map_meta_cap' => true,
    'hierarchical' => false,
    'rewrite' => false,
    'query_var' => true,
    'supports' => array(
    'title',
    'editor',
    'thumbnail',
    ),
    'labels' => array(
    'name' => 'Slider',
    'singular_name' => 'Slider',
    'menu_name' => 'Slider',
    'add_new' => 'Add Slider',
    'add_new_item' => 'Add New Slider',
    'edit' => 'Edit',
    'edit_item' => 'Edit Slider',
    'new_item' => 'New Slider',
    'view' => 'View Slider',
    'view_item' => 'View Slider',
    'search_items' => 'Search Slider',
    'not_found' => 'No Slider Found',
    'not_found_in_trash' => 'No Slider Found in Trash',
    'parent' => 'Parent Slider'
            )
    ));
}
function slider() {
$args = array( 'post_type' => 'slider', 'posts_per_page' => 100, 'orderby' => 'ID', 'order' => 'DESC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ ?>
<div class="home_banner">
    <ul id="banner" class="owl-carousel">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <li>
             <?php $img = get_the_post_thumbnail($recent['ID'], 'full'); if($img != ''){echo $img;  } else{?> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/banner.jpg"> <?php } ?>
        <?php the_content(); ?>
        <a href="<?php the_field( 'button_link' ); ?>"><?php the_field( 'link_text' ); ?></a>
        </li>
    <?php  endwhile;?>
    </ul>
</div>
<?php }
wp_reset_query();
return;
}
add_shortcode( 'slider', 'slider' );

add_action('init', 'register_my_custom_post_type_testimonials');
function register_my_custom_post_type_testimonials()
{
    register_post_type('testimonials', array(
    'label' => 'Testimonials',
    'description' => 'This will allow you to add testimonials.',
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'map_meta_cap' => true,
    'hierarchical' => false,
    'rewrite' => false,
    'query_var' => true,
    'supports' => array(
    'title',
    'editor',
    'thumbnail',
    ),
    'labels' => array(
    'name' => 'Testimonials',
    'singular_name' => 'Testimonials',
    'menu_name' => 'Testimonials',
    'add_new' => 'Add Testimonials',
    'add_new_item' => 'Add New Testimonials',
    'edit' => 'Edit',
    'edit_item' => 'Edit Testimonials',
    'new_item' => 'New Testimonials',
    'view' => 'View Testimonials',
    'view_item' => 'View Testimonials',
    'search_items' => 'Search Testimonials',
    'not_found' => 'No Testimonials Found',
    'not_found_in_trash' => 'No Testimonials Found in Trash',
    'parent' => 'Parent Testimonials'
            )
    ));
}