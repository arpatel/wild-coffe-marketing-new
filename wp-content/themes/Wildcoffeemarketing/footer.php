<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php the_field('footer_top_title', 'option'); ?>
<a href="<?php the_field('button_link', 'option'); ?>"><?php the_field('button_text', 'option'); ?></a>

<a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png" alt="Wildcoffeemarketing" title="Wildcoffeemarketing"></a>
<?php the_field('description', 'option'); ?>
<?php the_field('phone_label', 'option'); ?> <?php the_field('phone_number', 'option'); ?>
<?php the_field('email_label', 'option'); ?> <a href="mailto: <?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a> 


<?php the_field('address_title', 'option'); ?>
<?php the_field('footer_address', 'option'); ?>
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.12.4.min.js"></script> 
</body>
</html>
