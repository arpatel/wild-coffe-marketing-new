<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
wc_print_notices();
 ?>
<div class="checkout-breadcrumbs-wrap">
	<div  class="container">
	<nav class="checkout-breadcrumb">
		<a href="<?php echo site_url() ?>/cart">Cart</a>
		<a href="#">Customer information</a>
		<a href="#">Payment Method</a>		
	</nav>
	</div>
	
	
</div>

 <div class="row">
 	<div class="col-sm-6">
		 		<div class="col-sm-12 billing-fill-info1">
			 		<div class="checkout-step-title">
			 			<div class="step-num col-lg-2 col-md-2 col-sm-4 col-xs-12"><span class="badge badge-info">1</span></div>
			 			<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12"><h3><?php _e( 'CONTACT INFORMATION', 'woocommerce' ); ?></h3></div>
			 			<?php if(is_user_logged_in()) { ?>
			 				<div class="col-lg-5 col-sm-12">
								<div class="step-change"><a href="javascript:;" id="step1-change">Change</a></div>
							</div>
			 			<?php } ?>
						<?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>
						<div class="col-sm-12 step1-email"></div>
					</div>
				</div>
		<div class="col-sm-12 billing-fill-info2">

			<?php
			// If checkout registration is disabled and not logged in, the user cannot checkout
			if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
				echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
				return;
			}

			// filter hook for include new pages inside the payment method
			$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', wc_get_checkout_url() ); ?>

				<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

					

							<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

								<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

								<div class="row" id="customer_details">
									
									<div class="col-sm-12">
										<?php do_action( 'woocommerce_checkout_billing' ); ?>
									</div>
									
									<div class="col-sm-12" style="display: none;">
										<?php do_action( 'woocommerce_checkout_shipping' ); ?>
									</div>
								</div>

								<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>


							<?php endif; ?>

							<div class="checkout-step-title"><div class="step-num col-lg-2 col-md-2 col-sm-4 col-xs-12"><span class="badge badge-info">3</span></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-12"><h3><?php _e( 'PAYMENT METHOD', 'woocommerce' ); ?></h3></div></div>
							<div class="clear"></div>
							<div class="step3-wrap">
							<div class="row">
							<div class="col-sm-12">
							<?php if ( ! is_ajax() ) {
				do_action( 'woocommerce_review_order_before_payment' );
			}
			?>
			<div id="payment" class="woocommerce-checkout-payment">
				<?php if ( WC()->cart->needs_payment() ) : ?>
					<ul class="wc_payment_methods payment_methods methods">
						<?php
						if ( ! empty( $available_gateways ) ) {
							foreach ( $available_gateways as $gateway ) {
								wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
							}
						} else {
							echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
						}
						?>
					</ul>
				<?php endif; ?>
				<div class="form-row place-order">
					<noscript>
						<?php
						/* translators: $1 and $2 opening and closing emphasis tags respectively */
						printf( esc_html__( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate Totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ), '<em>', '</em>' );
						?>
						<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
					</noscript>

					<?php wc_get_template( 'checkout/terms.php' ); ?>

					<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

					<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

					<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

					<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
				</div>
			</div>
			<?php
			if ( ! is_ajax() ) {
				do_action( 'woocommerce_review_order_after_payment' );
			}
			 ?>
			 </div>
			 </div>
			 </div>
		</div>
	</div>

		<div class="col-sm-6">
			<div class="checkout-order-review">	
				<?php /* <h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3> */ ?>

				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php //do_action( 'woocommerce_checkout_order_review' ); ?>

						<table class="shop_table woocommerce-checkout-review-order-table">
							
							<tbody>
								<?php
									do_action( 'woocommerce_review_order_before_cart_contents' );

									foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
										$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

										if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
											?>
											<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
												<td class="product-thumb">
												<span class="checout-product-quantity badge badge-info"><?php echo $cart_item['quantity'];  ?></span>
													<?php $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
															echo wp_kses_post( $thumbnail );  ?>
															
												</td>
												<td class="product-name">
													<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
													<?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
												</td>
												<td class="product-total">
													<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
												</td>
											</tr>
											<?php
										}
									}

									do_action( 'woocommerce_review_order_after_cart_contents' );
								?>
							</tbody>
							<tfoot>

								<tr class="cart-subtotal">
									<th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
									<td><?php wc_cart_totals_subtotal_html(); ?></td>
								</tr>

								<?php /* foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
									<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
										<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
										<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
									</tr>
								<?php endforeach; */ ?>

								<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

									<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

									<?php wc_cart_totals_shipping_html(); ?>

									<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

								<?php endif; ?>

								<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
									<tr class="fee">
										<th><?php echo esc_html( $fee->name ); ?></th>
										<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
									</tr>
								<?php endforeach; ?>

								<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
									<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
										<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
											<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
												<th><?php echo esc_html( $tax->label ); ?></th>
												<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
											</tr>
										<?php endforeach; ?>
									<?php else : ?>
										<tr class="tax-total">
											<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
											<td><?php wc_cart_totals_taxes_total_html(); ?></td>
										</tr>
									<?php endif; ?>
								<?php endif; ?>

								<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
								<tr class="coupon-form">
								
								</tr>
								<?php /* <tr class="order-total">
									<th><?php _e( 'Total', 'woocommerce' ); ?></th>
									<td><?php wc_cart_totals_order_total_html(); ?></td>
								</tr> */ ?>

								<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

							</tfoot>
						</table>
						<?php  ?>
					</div>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

					<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
				</div>
								
								
							</form>

								<form class="checkout_coupon woocommerce-form-coupon" method="post" style="display:block;">
								<div class="checkout_coupon_inner">

							<p><?php esc_html_e( 'If you have a coupon code, please apply it below.', 'woocommerce' ); ?></p>

							<p class="form-row form-row-first">
								<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Gift card or discount code', 'woocommerce' ); ?>" id="coupon_code" value="" />
							</p>

							<p class="form-row form-row-last">
								<button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply', 'woocommerce' ); ?></button>
							</p>

							<div class="clear"></div>
							</div>
						</form>
						<div class="checkout-finaltotal">
						<table class="shop_table woocommerce-checkout-review-order-table ">
						<tr class="order-total"><td><?php wc_cart_totals_order_total_html(); ?></td></tr>
						</table>
						</div>
		</div>
</div>

