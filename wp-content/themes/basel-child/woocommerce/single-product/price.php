<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<?php 
if ( ! $product->is_purchasable() ) { $form_code = get_field( 'form_shortcode' );  ?>
		<div class="basel-button-wrapper text-center"><a href="#quoteform" title="" class="btn button btn-color-default btn-style-default btn-size-default basel-popup-with-content ">Get A Quote</a></div>
	<div id="quoteform" class="mfp-with-anim basel-content-popup mfp-hide" style="max-width:500px;"><div class="basel-popup-inner">
	<div class="wpb_text_column wpb_content_element" id="quoteform" style="margin-bottom: 0px">
		<div class="wpb_wrapper"><?php echo do_shortcode($form_code); ?></div></div></div></div>

<?php }
?>
<p class="price"><?php echo $product->get_price_html(); ?></p>
