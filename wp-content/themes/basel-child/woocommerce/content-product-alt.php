<?php 
	global $product;
?>
<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
<div class="product-list-top">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked basel_template_loop_product_thumbnail - 10
			 */
			//do_action( 'woocommerce_before_shop_loop_item_title' );
			/**
			 * woocommerce_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );
			/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
			?><div class="product-list-price">Starting from:<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?></div>
		
</div>
<?php //basel_product_brands_links(); ?>
<div class="product-list-text">
	<?php the_content(); ?>
</div>
<div class="product-list-bottom">
	<a href="<?php the_permalink(); ?>">BUY NOW</a>
</div>
<?php /* <div class="wrap-price">
	<div class="wrapp-swap">
		<div class="swap-elements1">
			<?php
				
			?>
			<div class="btn-add">
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>
		</div>
	</div>
	<?php 
		basel_swatches_list();
	?>
</div> */ ?>

<?php if ( basel_loop_prop( 'timer' ) ): ?>
	<?php basel_product_sale_countdown(); ?>
<?php endif ?>