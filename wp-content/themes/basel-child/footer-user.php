<?php
/**
 * The template for displaying the footer
 *
 */
?>  
      <div class="clearfix"></div>
          <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        Copyright &copy; <?php echo date('Y'); ?> | Wild Coffee Content | All Rights Reserved | Build By: iGreen Markering
                    </div>
                    
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->
         </div>
    <!-- /#right-panel -->
<?php wp_footer(); ?>
    <!-- Scripts -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/popper.min-user.js"></script> 
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.matchHeight.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/main.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-ui.min.js"></script>
    <script>
      jQuery( function() {
        jQuery( "#order-date" ).datepicker();
      } );
    </script>
     
</body>
</html>

