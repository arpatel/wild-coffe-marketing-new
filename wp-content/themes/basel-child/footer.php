<?php
/**
 * The template for displaying the footer
 *
 */
?>
<?php if (basel_needs_footer()): ?>
	<?php basel_page_bottom_part(); ?>

	<?php if ( basel_get_opt( 'prefooter_area' ) != '' ): ?>
		<div class="basel-prefooter">
			<div class="container">
				<?php echo do_shortcode( basel_get_opt( 'prefooter_area' ) ); ?>
			</div>
		</div>
	<?php endif ?>

	<!-- FOOTER -->
	<footer class="footer-container color-scheme-<?php echo esc_attr( basel_get_opt( 'footer-style' ) ); ?>">
		<!--<div class="footer-top">
			<div class="footer-logo"><?php //dynamic_sidebar( 'footer-1' ) ?></div>
			<div class="footer-txt"><?php //dynamic_sidebar( 'footer-2' ) ?></div>
			<div class="footer-contact"><?php //dynamic_sidebar( 'footer-3' ) ?></div>
		</div>-->
		<div class="footer-bottom"><div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<?php wp_nav_menu( array( 'menu' => 'Footer Menu', 'container_class' => 'footer_menu' ) ); ?>
				</div>
			
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
				<?php /*	<div class="support-menu ft-social"><?php dynamic_sidebar( 'footer-social' ) ?></div> */ ?>
					<div class="footer-address"><?php dynamic_sidebar( 'footer-address' ) ?></div>
				</div>
			</div></div>
			
		</div>
		<?php /*
		if ( basel_get_opt( 'disable_footer' ) ) {
			get_sidebar( 'footer' ); 
		}*/
		?>

		<?php if ( basel_get_opt( 'disable_copyrights' ) ): ?>
			<div class="copyrights-wrapper copyrights-<?php echo esc_attr( basel_get_opt( 'copyrights-layout' ) ); ?>">
				<div class="container">
					<div class="min-footer">
						<div class="col-left">
							<?php if ( basel_get_opt( 'copyrights' ) != ''): ?>
								<?php echo do_shortcode( basel_get_opt( 'copyrights' ) ); ?>
							<?php else: ?>
								<p>Copyright &copy; <?php echo date( 'Y' ); ?> <a href="<?php echo esc_url( home_url('/') ); ?>"><?php bloginfo( 'name' ); ?></a>. <?php _e( 'Powered by iGreen Marketing', 'basel' ) ?></p>
							<?php endif ?>
						</div>
						<?php if ( basel_get_opt( 'copyrights2' ) != ''): ?>
							<div class="col-right">
								<?php echo do_shortcode( basel_get_opt( 'copyrights2' ) ); ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		<?php endif ?>
		
	</footer>
<?php endif ?>
</div> <!-- end wrapper -->

<div class="basel-close-side"></div>

<?php wp_footer(); ?>

<?php if (basel_needs_footer()) do_action( 'basel_after_footer' ); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/popper.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="text/javascript">
	 jQuery(document).ready(function () { 
	 	jQuery("#createaccount").prop( "checked", true );
	 	jQuery('#write-for-us-button button').on('click', function () {
    		jQuery('.write-for-us-form').slideToggle( "slow" );
		});
		jQuery("#account_password").on("propertychange change keyup paste input", function(){
		   // alert(jQuery(this).val());
		    if(jQuery(this).val() != ''){
		    	$( "#createaccount" ).prop( "checked", true );
		    }
		    else{
		    	$( "#createaccount" ).prop( "checked", false );
		    }
		});
		jQuery('.save-continue').on('click', function () {
    		//jQuery('.write-for-us-form').slideToggle( "slow" );
    		//alert('test');
    		var email = jQuery('#billing_email').val();
    		var password = jQuery('#account_password').val();
    		var first_name = jQuery('#billing_first_name').val();
    		var last_name = jQuery('#billing_last_name').val();
    		var address = jQuery('#billing_address_1').val();
    		var city = jQuery('#billing_city').val();
    		var postcode = jQuery('#billing_postcode').val();
    		var state = jQuery('#billing_state').val();
    		if((email == '') || (password == '') ||  (password == '') ||  (first_name == '') ||  (last_name == '') ||  (address == '') ||  (city == '') || (postcode == '') || (state == '') ){
    			jQuery('#place_order').click();
    		}else {
    			jQuery(this).hide();
    			jQuery('.step1-wrap, .step2-wrap, .woocommerce-form-login-toggle').fadeOut();
    			jQuery('.step1-email').text(email);
    			jQuery('.step2-address').text(address + ' ' + city + ' ' + state +' '+ postcode);
    			jQuery('.step-change').show();
    			jQuery('.billing-fill-info1, .billing-fill-info2').addClass('bor-btm');
    		}
		});
		jQuery('#step1-change').on('click', function () {
			jQuery('.step1-wrap, .woocommerce-form-login-toggle').fadeIn();
			jQuery('.step1-email').text('');
			jQuery('.save-continue').show();
			jQuery('.billing-fill-info1').removeClass('bor-btm');
		});
		jQuery('#step2-change').on('click', function () {
			jQuery('.step2-wrap').fadeIn();
			jQuery('.step2-address').text('');
			jQuery('.save-continue').show();
			jQuery('.billing-fill-info2').removeClass('bor-btm');
		});

	});
</script>

</body>
</html>