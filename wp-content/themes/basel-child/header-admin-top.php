<?php
/**
 * The User top Header template for our theme
 */
?>
<?php
global $wpdb;
?>
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a> 
                </div>
				<div class="logo-section visible-xs">
                        <a class="navbar-brand" href="./"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/wild-coffee-logo.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo2.png" alt="Logo"></a>  
                        </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
				<div class="search-top visible-lg visible-md visible-sm">					
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                <button class="search-trigger"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
					</div>
					<div class="logo-section visible-lg visible-md visible-sm">
                        <a class="navbar-brand" href="./"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/wild-coffee-logo.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo2.png" alt="Logo"></a>  
                        </div>
						<div class="header-left">
						<div class="user-area dropdown float-left">
	                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
	                           <i class="fa fa-user"></i>
	                        </a>

	                        <div class="user-menu dropdown-menu">
	                            <a class="nav-link" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
	                        </div>
                   		</div>

                        <div class="dropdown for-notification">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                 <?php $notification_count = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}notification WHERE sent_to = 'admin' AND status = '0'");
                                        if($notification_count){ ?>
                                <span class="count bg-danger">
                                    <?php echo sizeof($notification_count); ?>
                                </span>
                            <?php } ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="notification" id="view-notification">
                                <p class="noti-message">Loading...</p>
                            </div>
                        </div>

                       <?php /* <div class="dropdown for-message">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-envelope"></i>
                                <span class="count bg-primary">4</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="message">
                                <p class="red">You have 4 Mails</p>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/avatar/1.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Jonathan Smith</span>
                                        <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/avatar/2.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Jack Sanders</span>
                                        <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/avatar/3.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Cheryl Wheeler</span>
                                        <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left">
                                        <img alt="avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/avatar/4.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Rachel Santos</span>
                                        <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </div>
                                </a>
                            </div>
                        </div> */?>
                    </div>

        

                </div>
            </div>
        </header>
        <!-- /#header -->

<script type="text/javascript">
jQuery(document).ready(function ($) {    

 function load_unseen_notification(view)
 {  
 
  $.ajax({
   url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
   method:"POST",
   data:"action=admin_notification&view=" +view,
   dataType:"json",
   success:function(data)
   { //alert(data.unseen_notification);
    $('#view-notification').html(data.notification);
    

    if(data.unseen_notification > 0)
    {
     $('.bg-danger').html(data.unseen_notification);
    }
   }
  });
 }
  $(document).on('click', '#notification', function(){
  $('.bg-danger').hide();
  load_unseen_notification('yes');
 });
 /*load_unseen_notification();

  setInterval(function(){ 
  load_unseen_notification();; 
 }, 5000);
*/
}); 

</script>        