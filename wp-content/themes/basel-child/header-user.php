<?php
/**
 * The User Header template for our theme
 */
if(!is_user_logged_in()){
	$siteurl = get_site_url();
	header("Location: ". $siteurl);
}
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php the_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri(); ?>/assets/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style-child.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jquery-ui.css">

<?php wp_head(); ?>
</head>
<?php
   $query = $wp_query->query_vars;
   $current_user = wp_get_current_user();
?>
<body <?php body_class(); ?>>