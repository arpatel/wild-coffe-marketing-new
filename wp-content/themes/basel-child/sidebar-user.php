<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 */
?>
<?php
   $current_user = wp_get_current_user();
?>
<!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
    	<div class="user-profile">
        	<div class="user-image">
               <a href="<?php echo site_url(); ?>/profile-image/" title="Change Photo">
               <?php  $args = array( 'size' => 65); 
                $avatar = get_avatar_data(  $current_user->ID, $args );  $img_id = get_user_meta( $current_user->ID, 'wc_user_avatar', true );  $src = wp_get_attachment_image_src( $img_id , 'thumbnail', false );
             if($src[0] != '') { ?>
               <img class="user-avatar rounded-circle avatar-65" src="<?php echo $src[0]; ?>" alt="<?php echo $current_user->display_name; ?>" width="65">
            <?php }else{ ?>
                <img class="user-avatar rounded-circle avatar-65" src="<?php echo $avatar['url']; ?>" alt="<?php echo $current_user->display_name; ?>" width="65">
            <?php } ?>
        </a>
        	</div>
	            <div class="user-content">
	            <h5 class="text-sm-center mt-2 mb-1">Welcome <?php echo get_user_meta( $current_user->ID, 'first_name', true ); ?> <?php echo get_user_meta( $current_user->ID, 'last_name', true ); ?></h5>	
	            <div class="location text-sm-center"><i class="fa fa-map-marker"></i> <?php echo get_user_meta( $current_user->ID, 'billing_address_1', true ); ?>, <?php echo get_user_meta( $current_user->ID, 'billing_country', true ); ?></div>
	            </div>
            </div>
        <nav class="navbar navbar-expand-sm navbar-default">
        	
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?php if(is_page('1890')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/user-dashboard/"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="<?php if(is_page('1983')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/user-personal-information/"><i class="menu-icon fa fa-user"></i>Personal Information</a>
                    </li>
                    <li class="menu-item-has-children dropdown <?php if(is_page('1878') || is_page('2152') || is_page('2154')) echo "active"; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-usd"></i>Order / Status</a>
                        <ul class="sub-menu children dropdown-menu">
                                                     
                        	<li><i class="fa fa-th"></i><a href="<?php echo site_url(); ?>/order-forms/">Order Forms</a></li>
                            <li><i class="fa fa-bars"></i><a href="<?php echo site_url(); ?>/user-all-order-status/">All Orders</a></li>
                            <li><i class="fa fa-tasks"></i><a href="<?php echo site_url(); ?>/user-complete-order-review/">Completed Order</a></li>
                        </ul>
                    </li>
                    <li class="<?php if(is_page('2124')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/purchase-new-content/" > <i class="menu-icon fa fa-star"></i>Purchase New Content</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
        <div class="left-panel-bottom">
            <?php dynamic_sidebar('userside-about'); ?>
        </div>
    </aside>
    <!-- /#left-panel -->