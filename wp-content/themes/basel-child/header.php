<?php
/**
 * The Header template for our theme
 */
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,800,900" rel="stylesheet">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.min.js"></script> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php if ( basel_needs_header() ): ?>
	<?php do_action( 'basel_after_body_open' ); ?>
	<?php 
		basel_header_block_mobile_nav(); 
		$cart_position = basel_get_opt( 'cart_position' );
		if( $cart_position == 'side' || ! basel_woocommerce_installed() ) {
			?>
				<div class="cart-widget-side">
					<div class="widget-heading">
						<h3 class="widget-title"><?php esc_html_e( 'Shopping cart', 'basel' ); ?></h3>
						<a href="#" class="widget-close"><?php esc_html_e( 'close', 'basel' ); ?></a>
					</div>
					<div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>
				</div>
			<?php
		}
	?>
<?php endif ?>
<div class="website-wrapper">
<?php if ( basel_needs_header() ): ?>
	<?php if( basel_get_opt( 'top-bar' ) ): ?>
		<div class="topbar-wrapp ar color-scheme-<?php echo esc_attr( basel_get_opt( 'top-bar-color' ) ); ?>">
			<div class="container">
				<div class="topbar-content">
					<div class="top-bar-left">
						
						<?php if( basel_get_opt( 'header_text' ) != '' ): ?>
							<?php echo do_shortcode( basel_get_opt( 'header_text' ) ); ?>
						<?php endif;  ?>						
						
					</div>
					<div class="top-bar-right">
						<?php if(is_user_logged_in() && (current_user_can('customer') ) ) 
      					{ ?>	
      						<div class="login-link dashboard-link-desk"><a href="<?php echo site_url(); ?>/user-dashboard">Dashboard</a></div>
      						<div class="login-link dashboard-link-mobile"><a href="<?php echo site_url(); ?>/user-dashboard"><i class="fa fa-desktop" aria-hidden="true"></i></div>
      						<?php }  ?>
						<?php if(is_user_logged_in()) { ?>
							<div class="login-link"><a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></div>
						<?php } else { ?>
						<div class="login-link"><a href="<?php echo site_url(); ?>/my-account?redirect=yes">Login</a></div>
					<?php } ?>
						 <div class="topbar-menu">
							<?php 
								if( has_nav_menu( 'top-bar-menu' ) ) {
									wp_nav_menu(
										array(
											'theme_location' => 'top-bar-menu',
											'walker' => new BASEL_Mega_Menu_Walker()
										)
									);
								}
							 ?>
						</div>  
						<div class="top-cart">
							

							<a class="cart-customlocation"  href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
							<?php echo WC()->cart->get_cart_contents_count();  ?>
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								</a></div>
						<?php echo do_shortcode( '[social_buttons type="follow"]' ); ?>
					</div>
				</div>
			</div>
		</div> <!--END TOP HEADER-->
	<?php endif; ?>

	<?php 
		$header_class = 'main-header';
		$header = apply_filters( 'basel_header_design', basel_get_opt( 'header' ) );
		$header_bg = basel_get_opt( 'header_background' );
		$header_has_bg = ( ! empty($header_bg['background-color']) || ! empty($header_bg['background-image']));

		$header_class .= ( $header_has_bg ) ? ' header-has-bg' : ' header-has-no-bg';
		$header_class .= ' header-' . $header;
		$header_class .= ' icons-design-' . basel_get_opt( 'icons_design' );
		$header_class .= ' color-scheme-' . basel_get_opt( 'header_color_scheme' );
	?>

	<!-- HEADER -->
	<header class="<?php echo esc_attr( $header_class ); ?>">
		
		<?php basel_generate_header( $header ); // location: inc/template-tags.php ?>
		
	</header><!--END MAIN HEADER-->
	<?php if ( is_product() ){?>
	<div class="product_single_image">
		<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
				the_post();
				
				$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
				if(!empty($featured_img_url)){				
					echo '<img src="'.$featured_img_url.'" class="inner-banner-image">';
				}else{
					echo '<img src="'.get_template_directory_uri().'/images/Single-Product-banner.png" class="inner-banner-image">';
				}
				
				$title = get_the_title();
				echo '<div class="static-inner-title"> <h1>'.$title.'</h1> </div>';
				
				
			}
			}			
		?>
	</div>
	<?php }?>
	
	<?php if ( is_product_category() ){?>
	<div class="product_single_image">
		<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
				the_post();
				
				
					echo '<img src="'.get_template_directory_uri().'/images/Single-Product-banner.png" class="inner-banner-image">';
				
				
				$title = $wp_query->get_queried_object()->name;
				echo '<div class="static-inner-title"> <h1>'.$title.'</h1> </div>';
				
				
			}
			}			
		?>
	</div>
	<?php }?>
	<div class="clear"></div>
	
	<?php basel_page_top_part(); ?>
<?php endif ?>