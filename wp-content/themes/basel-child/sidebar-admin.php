<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 */
?>
<?php
   $current_user = wp_get_current_user();
?>
<!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
    	<div class="user-profile">
        	<div class="user-image">
               <?php  $args = array( 'size' => 65); 
                $avatar = get_avatar_data(  $current_user->ID, $args );  $img_id = get_user_meta( $current_user->ID, 'wc_user_avatar', true );  $src = wp_get_attachment_image_src( $img_id , 'thumbnail', false );
             if($src[0] != '') { ?>
               <img class="user-avatar rounded-circle avatar-65" src="<?php echo $src[0]; ?>" alt="<?php echo $current_user->display_name; ?>" width="65">
            <?php }else{ ?>
                <img class="user-avatar rounded-circle avatar-65" src="<?php echo $avatar['url']; ?>" alt="<?php echo $current_user->display_name; ?>" width="65">
            <?php } ?>
        
        	</div>
	            <div class="user-content">
	            <h5 class="text-sm-center mt-2 mb-1">Welcome Admin</h5>	
	            <div class="location text-sm-center"><i class="fa fa-map-marker"></i> <?php echo get_user_meta( $current_user->ID, 'billing_address_1', true ); ?>, <?php echo get_user_meta( $current_user->ID, 'billing_country', true ); ?></div>
	            </div>
            </div>
        <nav class="navbar navbar-expand-sm navbar-default">
        	
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?php if(is_page('2232')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/admin-dashboard/"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="<?php if(is_page('2234')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/admin-information/"><i class="menu-icon fa fa-user"></i>Admin Information</a>
                    </li>
                    <li class="<?php if(is_page('2241')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/admin-new-orders-status/"><i class="menu-icon fa fa-usd"></i>New Orders / Status</a>
                    </li>
                    <li class="<?php if(is_page('2243')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/admin-history-completed-orders/"><i class="menu-icon fa fa-star"></i>History / Completed Orders</a>
                    </li>
                    <li class="<?php if(is_page('2245')) echo "active"; ?>">
                        <a href="<?php echo site_url(); ?>/client-feedback/" > <i class="menu-icon fa fa-pencil-square-o"></i>Client Feedback</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
        <div class="left-panel-bottom">
            <?php dynamic_sidebar('userside-about'); ?>
        </div>
    </aside>
    <!-- /#left-panel -->