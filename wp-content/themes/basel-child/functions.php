<?php

add_action( 'wp_enqueue_scripts', 'basel_child_enqueue_styles', 1000 );

function basel_child_enqueue_styles() {
  $version = basel_get_theme_info( 'Version' );
  
  if( basel_get_opt( 'minified_css' ) ) {
    wp_enqueue_style( 'basel-style', get_template_directory_uri() . '/style.min.css', array('bootstrap'), $version );
  } else {
    wp_enqueue_style( 'basel-style', get_template_directory_uri() . '/style.css', array('bootstrap'), $version );
  }
  
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('bootstrap'), $version );

    wp_enqueue_script( 'child-custom', get_stylesheet_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), $version );
    //wp_enqueue_script( 'child-validate', get_stylesheet_directory_uri() . '/assets/js/jquery.validate.min.js', array( 'jquery' ), $version, true );
}

if (function_exists('register_sidebar')) {
   register_sidebar(array(
    'name' => 'Footer Address',
    'id' => 'footer-address',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
    ));

    register_sidebar(array(
    'name' => 'About WildCoffee User Side',
    'id' => 'userside-about',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
    ));

    register_sidebar(array(
    'name' => 'User Order Form Text',
    'id' => 'user-order-form',
    'before_widget' => '<div class="ar">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
    ));
}


if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'     => 'Email setting',
        'menu_title'    => 'Order Email Setting',
        'menu_slug'     => 'order-email-settings',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
    
    
    
    
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;

  ob_start();

  ?>
  <a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
  <span><?php echo WC()->cart->get_cart_contents_count();  ?></span>
  <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
  <?php
  $fragments['a.cart-customlocation'] = ob_get_clean();
  return $fragments;
}

add_action('init', 'register_my_custom_post_type_testimonials');
function register_my_custom_post_type_testimonials()
{
    register_post_type('testimonials', array(
    'label' => 'Testimonials',
    'description' => 'This will allow you to add testimonials.',
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'map_meta_cap' => true,
    'hierarchical' => false,
    'rewrite' => false,
    'query_var' => true,
    'supports' => array(
    'title',
    'editor',
    'thumbnail',
    ),
    'labels' => array(
    'name' => 'Testimonials',
    'singular_name' => 'Testimonials',
    'menu_name' => 'Testimonials',
    'add_new' => 'Add Testimonials',
    'add_new_item' => 'Add New Testimonials',
    'edit' => 'Edit',
    'edit_item' => 'Edit Testimonials',
    'new_item' => 'New Testimonials',
    'view' => 'View Testimonials',
    'view_item' => 'View Testimonials',
    'search_items' => 'Search Testimonials',
    'not_found' => 'No Testimonials Found',
    'not_found_in_trash' => 'No Testimonials Found in Trash',
    'parent' => 'Parent Testimonials'
            )
    ));
}

function testimonail() {
$html = '';    
$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 4, 'orderby' => 'ID', 'order' => 'DESC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ 
$html = '<div class="testimonials_slider info-box-per-view-4" data-owl-carousel="" data-wrap="yes" data-autoplay="no" data-hide_prev_next_buttons="yes" data-desktop="4" data-desktop_small="2" data-tablet="1" data-mobile="1"">
    <ul id="testimonials_slider" class="owl-carousel">';
    while ( $the_query->have_posts() ) : $the_query->the_post();
        $html .= '<li class="owl-li">';  
        $html .= '<div class="row">';
          $img = get_the_post_thumbnail($recent['ID'], 'full'); 
            if($img != ''){
             $html .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 quote-img text-center">'.$img.'</div>';  
            } else{ 
                $html .='<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 quote-img text-center"><img src="'.esc_url( get_template_directory_uri() ).'/images/banner.jpg"></div>'; 
             } 
             $html .='<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 quote-content">';
          $html .= '<span class="name-title">'.get_the_title().'</span>';

        $html .='<a class="link-text" href="'.get_field( 'website_link' ).'">'.str_replace(array('http://', 'https://'), ' ', get_field( 'website_link' )).'</a>';
        $html .= get_the_content();
        $html .='</div></div></li>';  
     endwhile;
    $html .='</ul></div>';
 }
wp_reset_query();
return $html; 
}
add_shortcode( 'testimonail', 'testimonail' );

function coffee_woo_reg_form_fields() {
    ?>
    <p class="form-row form-row-wide">
        <label for="billing_first_name"><?php _e('First name', 'text_domain'); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_first_name" id="billing_first_name" value="<?php if (!empty($_POST['billing_first_name'])) esc_attr_e($_POST['billing_first_name']); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="billing_last_name"><?php _e('Last name', 'text_domain'); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_last_name" id="billing_last_name" value="<?php if (!empty($_POST['billing_last_name'])) esc_attr_e($_POST['billing_last_name']); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="billing_company"><?php _e('Business Name', 'text_domain'); ?></label>
        <input type="text" class="input-text" name="billing_company" id="billing_company" value="<?php if (!empty($_POST['billing_company'])) esc_attr_e($_POST['billing_company']); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="billing_phone"><?php _e('Phone', 'text_domain'); ?></label>
        <input type="text" class="input-text" name="billing_phone" id="billing_phone" value="<?php if (!empty($_POST['billing_phone'])) esc_attr_e($_POST['billing_phone']); ?>" />
    </p>
    <div class="clear"></div>
    <?php
}
add_action('woocommerce_register_form_start', 'coffee_woo_reg_form_fields');

function coffee_woo_validate_reg_form_fields($username, $email, $validation_errors) {
    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
        $validation_errors->add('billing_first_name_error', __('<strong>Error</strong>: First name is required!', 'text_domain'));
    }

    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
        $validation_errors->add('billing_last_name_error', __('<strong>Error</strong>: Last name is required!.', 'text_domain'));
    }
   
    return $validation_errors;
}
add_action('woocommerce_register_post', 'coffee_woo_validate_reg_form_fields', 10, 3);

function coffee_woo_save_reg_form_fields($customer_id) {
    //First name field
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    //Last name field
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
    }
    if (isset($_POST['billing_company'])) {
        update_user_meta($customer_id, 'billing_company', sanitize_text_field($_POST['billing_company']));
    }
    if (isset($_POST['billing_phone'])) {
        update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
    }
}

add_action('woocommerce_created_customer', 'coffee_woo_save_reg_form_fields');

add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);
function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
    global $woocommerce;
    extract( $_POST );
    if ( strcmp( $password, $password2 ) !== 0 ) {
        return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
    }
    return $reg_errors;
}
add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );
function wc_register_form_password_repeat() {
    ?>
    
    <?php
}

/* Remove product meta */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );        // Remove the description tab
    unset( $tabs['reviews'] );      // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab

    return $tabs;
}

//add_action( 'woocommerce_before_checkout_billing_form', 'woocommerce_checkout_login_form' );

/*add quntity label*/
add_action( 'woocommerce_before_add_to_cart_quantity', 'bbloomer_echo_qty_front_add_cart' );
 
function bbloomer_echo_qty_front_add_cart() {
 echo '<div class="quantity_text">Quantity </div>'; 
}

/*ACF fields*/
add_action( 'woocommerce_product_tabs', 'product_extra_fields', 98 );
function product_extra_fields() {
  if( get_field('benefits') != '' ):
  echo get_field('benefits');
  endif;
}

add_action( 'woocommerce_after_single_product', 'bbloomer_custom_action', 5 );
 
function bbloomer_custom_action() {
  if( have_rows('faq') ): ?>
  <div class="faq_section">
    
    <div class="container">
    <h2><?php the_field( 'faq_title' ); ?></h2>
      <?php 
      $i=1;
      while( have_rows('faq') ): the_row(); 

      // vars
      $question = get_sub_field('question');
      $answer = get_sub_field('answer');
      ?>
      <div id="accordion">
        <div class="card">
          <div class="card-header">
            <a class="card-link" data-toggle="collapse"  href="#menu<?php echo $i;?>" aria-expanded="false" aria-controls="menu<?php echo $i;?>">Q: <?php echo $question; ?><span class="collapsed"><p><b>+</b></p></span>
            <span class="expanded"><p><b>x</b></p></span></a>
          </div>
          <div id="menu<?php echo $i;?>" class="collapse">
            <div class="card-body">
              A: <?php echo $answer; ?>
            </div>
          </div>
        </div>
      </div>
      <?php 
        $i++;
        endwhile; 
      ?>
    </div>
  </div>  
  <?php endif; 
}



add_filter( 'woocommerce_checkout_fields' , 'wildcoffee_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function wildcoffee_checkout_fields( $fields ) {
     unset($fields['billing']['billing_phone']);
     unset($fields['order']['order_comments']);
     $fields['billing']['billing_email']['priority'] = 4;
     //$fields['billing']['billing_state']['priority'] = 110;
     //$fields['billing']['billing_country']['priority'] = 100;
     $fields['billing']['billing_email']['placeholder'] = 'Email';
     $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
     $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
     $fields['billing']['billing_company']['placeholder'] = 'Company';
     $fields['billing']['billing_address_1']['placeholder'] = 'Street Address';
     $fields['billing']['billing_city']['placeholder'] = 'City';
     $fields['billing']['billing_postcode']['placeholder'] = 'ZIP/Postal Code';
     $fields['account']['account_password']['label'] = 'Password';
     $fields['billing']['billing_email']['label'] = 'Email';
     $fields['billing']['billing_company']['label'] = 'Company';
     $fields['billing']['billing_city']['label'] = 'City';
     $fields['billing']['billing_postcode']['label'] = 'ZIP/Postal Code';
     $fields['billing']['billing_state']['placeholder'] = 'State/Province';
   //  $fields['billing']['billing_state']['placeholder'] = 'Country';
     return $fields;
}

function hackies( $field, $key, $args, $value ) {
  // Remove form-row from p to avoid collision with js.
  $field = str_replace( 'p class="form-row ', 'p class="', $field );
if(is_page( 2124 )){
  // Wrap all fields except first and last name.
  if ( $key === 'billing_first_name') {
    $field = '<div class="checkout-step-title"><div class="step-num col-lg-1 col-sm-12"><span class="badge badge-info">3</span></div><div class="col-lg-5 col-sm-12"><h3>Billing Address</h3><div class="step-change"><a href="javascript:;" id="step2-change">Edit</a></div></div><div class="col-sm-12 step2-address"></div></div><div class="step2-wrap">' . $field . '';
  }
}else{
 if ( $key === 'billing_first_name') {
    $field = '<div class="checkout-step-title"><div class="step-num col-lg-2 col-md-2 col-sm-4 col-xs-12"><span class="badge badge-info">2</span></div><div class="step-num col-lg-10 col-md-10 col-sm-8 col-xs-12"><h3>Billing Address</h3></div><div class="col-lg-5 col-sm-12"><div class="step-change"><a href="javascript:;" id="step2-change">Change</a></div></div><div class="col-sm-12 step2-address"></div></div><div class="step2-wrap">' . $field . '';

}
}

  // Wrap first and last name in double wrap/unwrap.
  /*if( $key === 'billing_first_name' ) {
    $field = '<div class="first-and-second-field-wrapper form-row"><span class="checkout-step-title"><span class="step-num">2</span><span>Billing Address</span></span><div class="single-field-wrapper">' . $field . '</div>';
        } elseif ( $key === 'billing_last_name' ) {
    $field = '<div class="single-field-wrapper">' . $field . '</div></div>';
        }*/

  return $field;
}
add_filter( 'woocommerce_form_field_text', 'hackies', 10, 4);


function email_step1( $field, $key, $args, $value ) {
  // Remove form-row from p to avoid collision with js.
  $field = str_replace( 'p class="form-row ', 'p class="', $field );
  // Wrap all fields except first and last name.
  $register_text = '';
  if ( ! is_user_logged_in() ) {
      $register_text = '<span class="register-text">Creating an account is required to have access to your custom content created by our skilled writers.</span>';
     }
  if ( $key === 'billing_email' ) {
    $field = '<div class="step1-wrap">' . $register_text . $field. '';
     if (  is_user_logged_in() ) {
      $field .= '</div>';
     }
  }
  return $field;
}
add_filter( 'woocommerce_form_field_email', 'email_step1', 10, 4);

function account_step1( $field, $key, $args, $value ) {
  // Remove form-row from p to avoid collision with js.
  $field = str_replace( 'p class="form-row ', 'p class="', $field );
  // Wrap all fields except first and last name.
  if ( $key === 'account_password' ) {
    $field = '' . $field . '</div>';
  }
  return $field;
}
add_filter( 'woocommerce_form_field_password', 'account_step1', 10, 4);


add_filter('woocommerce_login_redirect', 'wc_login_redirect');
function wc_login_redirect( $redirect_to ) {
  $redirect = $_GET['redirect'];
   if ( $redirect == 'yes' ) {
      $redirect_to = site_url() .'/user-dashboard';
  }  
     return $redirect_to;
}

function custom_registration_redirect($registration_redirect ) {
     $registration_redirect = site_url() .'/user-dashboard';
     return $registration_redirect;
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);

function wc_empty_cart_redirect_url() {
  return get_site_url(). '/order/';
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

function user_order_info_data_func(){
$postdata = isset($_POST) ? $_POST : '';
global $wpdb;
if($_POST['submit_type'] == 'save-continue'){
  $status = '1';
}else{
  $status = '0';
}
 $relevant_link = serialize($_POST['relevant-link']); 
 //$wpdb->insert('wc_order_form_data', $postdata );
   $results = $wpdb->get_row( 'SELECT * FROM wc_order_form_data WHERE order_id = '.$postdata['order-id'].' AND item_name = "'.$postdata['item-name'].'"');
if($results == null)
{   
$wpdb->insert('wc_order_form_data',
              array(
              'order_id' => $postdata['order-id'],
              'item_name' => $postdata['item-name'],
              'title' => $postdata['title-of-blog'],
              'topic' => $postdata['topic'],
              'street' => $postdata['street'],
              'relevant_link' => $relevant_link,
              'call_to_action' => $postdata['call-to-action'],
              'description' => $postdata['targeted-audience-desc'],
              'status' => $status,
              'created' => current_time('mysql', 1)
              ),
              array(
              '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s'
              )
            );
 $status = $wpdb->insert_id;
            echo $status ? 'ok' : var_dump($wpdb);
}
else{
  echo 'update';
  $wpdb->update('wc_order_form_data',
              array(
              'title' => $postdata['title-of-blog'],
              'topic' => $postdata['topic'],
              'street' => $postdata['street'],
              'relevant_link' => $relevant_link,
              'call_to_action' => $postdata['call-to-action'],
              'description' => $postdata['targeted-audience-desc'],
              'status' => $status,
              ), array('order_id' => $postdata['order-id'], 'item_name' => $postdata['item-name'])
            );
 $status = $wpdb->insert_id;
            echo $status ? 'ok' : var_dump($wpdb);
}            
die();
}
//add action to call ajax
add_action( 'wp_ajax_user_order_info_data', 'user_order_info_data_func');
add_action( 'wp_ajax_nopriv_user_order_info_data', 'user_order_info_data_func');

function action_woocommerce_save_account_details( $user_id ) { 
       wp_safe_redirect(get_permalink( '1983' )); 
       exit;
}; 
add_action( 'woocommerce_save_account_details', 'action_woocommerce_save_account_details', 99, 2 );

function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
 if(is_page( 2124 )){
if ( is_null( $product ) ) {
   $product = $GLOBALS['product'];
}
 
$defaults = array(
   'input_id'    => uniqid( 'quantity_' ),
   'input_name'  => 'quantity',
   'input_value' => '1',
   'max_value'   => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
   'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
   'step'        => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),
   'pattern'     => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),
   'inputmode'   => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),
);
 
$args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product );
 
// Apply sanity to min/max args - min cannot be lower than 0.
$args['min_value'] = max( $args['min_value'], 0 );
$args['max_value'] = 0 < $args['max_value'] ? $args['max_value'] : '';
 
// Max cannot be lower than min if defined.
if ( '' !== $args['max_value'] && $args['max_value'] < $args['min_value'] ) {
    $args['max_value'] = $args['min_value'];
}
 
$options = '';
   
for ( $count = 1; $count <= 10; $count = $count + $args['step'] ) {
     
    // Cart item quantity defined?
    if ( '' !== $args['input_value'] && $args['input_value'] > 1 && $count == $args['input_value'] ) {
        $selected = 'selected';     
    } else $selected = '';
     
    $options .= '<option value="' . $count . '"' . $selected . '>' . $count . '</option>';
     
}
     
$string = '<div class="quantity"><select name="' . $args['input_name'] . '">' . $options . '</select></div>';
     
if ( $echo ) {
    echo $string;
} else {
    return $string;
}
 }else{
      if ( is_null( $product ) ) { 
        $product = $GLOBALS['product']; 
    } 
 
    $defaults = array( 
        'input_name' => 'quantity',  
        'input_value' => '1',  
        'max_value' => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),  
        'min_value' => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),  
        'step' => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),  
        'pattern' => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),  
        'inputmode' => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),  
 ); 
 
    $args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product ); 
 
    // Apply sanity to min/max args - min cannot be lower than 0. 
    $args['min_value'] = max( $args['min_value'], 0 ); 
    $args['max_value'] = 0 < $args['max_value'] ? $args['max_value'] : ''; 
 
    // Max cannot be lower than min if defined. 
    if ( '' !== $args['max_value'] && $args['max_value'] < $args['min_value'] ) { 
        $args['max_value'] = $args['min_value']; 
    } 
 
    ob_start(); 
 
    wc_get_template( 'global/quantity-input.php', $args ); 
 
    if ( $echo ) { 
        echo ob_get_clean(); 
    } else { 
        return ob_get_clean(); 
    } 
 }
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +
function woo_custom_single_add_to_cart_text() {
  if(is_page( 2124 )){
    return __( '+', 'woocommerce' );
  }else{
    return __( 'Add to cart', 'woocommerce' );
  }
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'add_product_link' );
function add_product_link( $link ) {
    global $product;
    if ( ! $product->is_purchasable() ) {
    $form_code = get_field( 'form_shortcode' );  ?>
    <div class="basel-button-wrapper"><a href="#quoteform" title="" class="btn button btn-color-default btn-style-default btn-size-default basel-popup-with-content ">+</a></div>
  <div id="quoteform" class="mfp-with-anim basel-content-popup mfp-hide" style="max-width:500px;"><div class="basel-popup-inner">
  <div class="wpb_text_column wpb_content_element" id="quoteform" style="margin-bottom: 0px">
    <div class="wpb_wrapper"><?php echo do_shortcode($form_code); ?></div></div></div></div>

<?php 
  }
}

function wildcoffee_wc_get_formatted_cart_item_data( $cart_item, $flat = false ) {
  $item_data = array();
 
  // Variation values are shown only if they are not found in the title as of 3.0.
  // This is because variation titles display the attributes.
  if ( $cart_item['data']->is_type( 'variation' ) && is_array( $cart_item['variation'] ) ) {

    foreach ( $cart_item['variation'] as $name => $value ) {
      $taxonomy = wc_attribute_taxonomy_name( str_replace( 'attribute_pa_', '', urldecode( $name ) ) );
 
      if ( taxonomy_exists( $taxonomy ) ) {
        // If this is a term slug, get the term's nice name.
        $term = get_term_by( 'slug', $value, $taxonomy );
        if ( ! is_wp_error( $term ) && $term && $term->name ) {
          $value = $term->name;
        }
        $label = wc_attribute_label( $taxonomy );
      } else {
        // If this is a custom option slug, get the options name.
        $value = apply_filters( 'woocommerce_variation_option_name', $value );
        $label = wc_attribute_label( str_replace( 'attribute_', '', $name ), $cart_item['data'] );
      }

      // Check the nicename against the title.
      if ( '' === $value || wc_is_attribute_in_product_name( $value, $cart_item['data']->get_name() ) ) {
        continue;
      }

      $item_data[] = array(
        'key'   => $label,
        'value' => $value,
      );
    }
  }

  // Filter item data to allow 3rd parties to add more to the array.
  $item_data = apply_filters( 'woocommerce_get_item_data', $item_data, $cart_item );

  // Format item data ready to display.
  foreach ( $item_data as $key => $data ) {
    // Set hidden to true to not display meta on cart.
    if ( ! empty( $data['hidden'] ) ) {
      unset( $item_data[ $key ] );
      continue;
    }
    $item_data[ $key ]['key']     = ! empty( $data['key'] ) ? $data['key'] : $data['name'];
    $item_data[ $key ]['display'] = ! empty( $data['display'] ) ? $data['display'] : $data['value'];
  }

  // Output flat or in list format.
  if ( count( $item_data ) > 0 ) {
    ob_start();

    if ( $flat ) {
      foreach ( $item_data as $data ) {
        echo esc_html( $data['key'] ) . ': ' . wp_kses_post( $data['display'] ) . "\n";
      }
    } else {
      //wc_get_template( 'cart/cart-item-data.php', array( 'item_data' => $item_data ) );
      foreach ( $item_data as $data ) : 

        if($data['key'] == 'Recurring Service For')
        {  ?>
          <div class="product-service-value"><span><?php echo wp_kses_post( ( $data['display'] ) );?></span></div>
        <?php }
      endforeach; 
    }

    return ob_get_clean();
  }

  return '';
}

add_filter( 'woocommerce_order_button_text', 'misha_custom_button_text_for_product' );
function misha_custom_button_text_for_product( $button_text ) {
  if(is_page( 2124 )){
    $button_text = 'Complete Order';
  }
  return $button_text;
 
}

function current_order_lists_item(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $per_page = 4;
$offset = ($paged-1)*$per_page; 
  $customer_user_id = get_current_user_id(); 
    $order_data = $wpdb->get_results("SELECT
    p.ID as order_id,
    p.post_date,
    oi.order_item_id as order_items_id,
    oi.order_item_name as order_items
    from
        {$wpdb->prefix}posts p 
        join {$wpdb->prefix}postmeta pm on p.ID = pm.post_id
        join {$wpdb->prefix}woocommerce_order_items oi on p.ID = oi.order_id
        join {$wpdb->prefix}woocommerce_order_itemmeta oim on oi.order_item_id = oim.order_item_id
    where
        post_type = 'shop_order'  AND
        post_status = 'wc-processing' AND 
        oi.order_item_type = 'line_item' AND 
        pm.meta_key LIKE '_customer_user' AND pm.meta_value = '".$customer_user_id."' 
    group by oi.order_item_id 
    ORDER BY p.post_date DESC LIMIT ".$offset.", ".$per_page."");

if ($order_data)
{

    foreach ($order_data as $order) 
    {
       
        $orderkey =  get_post_meta( $order->order_id, '_order_key', true );
        $product_id = wc_get_order_item_meta( $order->order_items_id, '_product_id', true ); 
        $item_word =  wc_get_order_item_meta( $order->order_items_id, 'pa_word-count', true );
        $item_post_count =  wc_get_order_item_meta( $order->order_items_id, 'pa_number-of-posts', true );
        $item_post = wc_get_order_item_meta( $order->order_items_id, 'pa_social-media', true );
        $item_qty =  wc_get_order_item_meta( $order->order_items_id, '_qty', true );

        $any_order_item_qty = 'No';
         $complete_date = '';
        for($j=1;$j<=$item_qty;$j++)
        {  
            $order_data = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}order_contents 
            WHERE order_id = '".$order->order_id."' AND product_name LIKE '".$order->order_items."%' AND item_number = '".$j."' AND status in('sent','updated')"); 
            if($order_data)
            {
                $any_order_item_qty = 'Yes';
                $complete_date = $order_data->completed_date;
            }
        } 
       ?>
       <tr>
            <td>
                <?php if(get_field( 'product_icon', $product_id )) { ?>
                <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                <?php } ?>
            </td>
            <td>
                <div class="stat-icon dib flat-color-1">
                    <div class="order-content">
                        <h3><?php echo $order->order_items; ?></h3>
                        <?php if($item_word){ ?>
                        <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                    <?php } ?>
                     <?php if($item_post_count){ ?>
                        <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                    <?php } ?>
                   
                    </div>
                </div>
            </td>
            <td>  
                <span class="name">
                <?php if($any_order_item_qty == 'Yes') {  
                     echo 'Completed: '. date('m/d/Y \A\t H:i A', strtotime($complete_date));
                } else { echo 'Processing'; } ?>     
                </span>
            </td>
            <td style="width: 200px;">
                <div class="progress mb-3" style="height: 8px">
                       <?php if($any_order_item_qty == 'Yes') {  ?>
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="20"></div>
                    <?php } else { ?>    
                        <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="20"></div>
                    <?php } ?>  
                </div>
            </td>
            <td>
                <?php if($any_order_item_qty == 'Yes') {  ?>
                    <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
                <?php } ?>
            </td>
        </tr> 
       <?php
    }
}
 die();        
}
add_action('wp_ajax_current_order_lists_item', 'current_order_lists_item', 10 );
add_action('wp_ajax_nopriv_current_order_lists_item', 'current_order_lists_item', 10 );


function past_order_lists(){
     global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
   if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $per_page = 4;
  $offset = ($paged-1)*$per_page; 
  $customer_user_id = get_current_user_id(); 
  $order_data = $wpdb->get_results("SELECT 
                  p.ID as order_id,
                  p.post_date,
                  oi.order_item_id as order_items_id,
                  oi.order_item_name as order_items
                  FROM {$wpdb->prefix}posts p
                  INNER JOIN {$wpdb->prefix}postmeta m1
                   ON ( p.ID = m1.post_id )
                  INNER JOIN {$wpdb->prefix}postmeta m2
                   ON ( p.ID = m2.post_id )
                  INNER JOIN {$wpdb->prefix}woocommerce_order_items oi 
                   ON ( p.ID = oi.order_id)
                   INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta oim 
                   ON ( oi.order_item_id = oim.order_item_id)
                  WHERE
                  p.post_type = 'shop_order'
                  AND post_status = 'wc-completed'
                  AND oi.order_item_type = 'line_item' 
                  AND ( m1.meta_key = '_customer_user' AND CAST(m1.meta_value AS DECIMAL) = '".$customer_user_id."' )
                  AND ( m2.meta_key = '_completed_date' AND m2.meta_value LIKE '".$odate."%' )
                  AND oi.order_item_name like '".$_POST['order-name']."%'
                  GROUP BY oi.order_item_id 
                  ORDER BY p.post_date
                  DESC LIMIT ".$offset.", ".$per_page."");

if ($order_data)
{

    foreach ($order_data as $order) 
    {
       
        $orderkey =  get_post_meta( $order->order_id, '_order_key', true );
        $product_id = wc_get_order_item_meta( $order->order_items_id, '_product_id', true ); 
        $item_word =  wc_get_order_item_meta( $order->order_items_id, 'pa_word-count', true );
        $item_post_count =  wc_get_order_item_meta( $order->order_items_id, 'pa_number-of-posts', true );
        $item_post = wc_get_order_item_meta( $order->order_items_id, 'pa_social-media', true );
        $completed_date = get_post_meta( $order->order_id, '_completed_date', true );
       ?>
       <tr>
            <td>
                <?php if(get_field( 'product_icon', $product_id )) { ?>
                <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                <?php } ?>
            </td>
            <td>
                <div class="stat-icon dib flat-color-1">
                    <div class="order-content">
                        <h3><?php echo $order->order_items; ?></h3>
                        <?php if($item_word){ ?>
                        <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                    <?php } ?>
                     <?php if($item_post_count){ ?>
                        <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                    <?php } ?>
                   
                    </div>
                </div>
            </td>
            <td>  
                <?php if($completed_date) { ?>
                    <span class="name">
                    <?php echo 'Completed: '. date('m/d/Y \A\t H:i A', strtotime($completed_date)); ?>
                    </span>
                <?php } ?>
            </td>
            <td style="width: 200px;">
                <div class="progress mb-3" style="height: 8px">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="20"></div>
                </div>
            </td>
            <td>
                <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
            </td>
        </tr> 
       <?php
    }
}
 die();        
}
add_action('wp_ajax_past_order_lists', 'past_order_lists', 10 );
add_action('wp_ajax_nopriv_past_order_lists', 'past_order_lists', 10 );



// plugins\contact-form-7\includes/submission.php change private $skip_mail = false; to public $skip_mail = false;
add_filter('wpcf7_before_send_mail', 'wpcf7_custom_form_action_url');
function wpcf7_custom_form_action_url( $form)
{
  $submission = WPCF7_Submission::get_instance();
  $posted_data = $submission->get_posted_data();
  //print_r($_POST);
  if($_POST['_wpcf7'] == '1996' || $_POST['_wpcf7'] == '2042' || $_POST['_wpcf7'] == '2029' || $_POST['_wpcf7'] == '2030' || $_POST['_wpcf7'] == '2031' || $_POST['_wpcf7'] == '2025' || $_POST['_wpcf7'] == '2043' || $_POST['_wpcf7'] == '2028' || $_POST['_wpcf7'] == '2050')
  {
    $submit_type = $_POST['submit_type'];
    if($submit_type == 'Save & Finish later')
    {
      $submission->skip_mail = true;
    }
    else
    { 
      global $wpdb;
      $form_id = $_POST['_wpcf7'];
      $customer_user_id = $_POST['user_id'];
      $order_id = $_POST['order_id'];
      $item_name = $_POST['ac_form_title'];
      $product_id = $_POST['product_id'];
      $saved_form_data = $wpdb->get_results("SELECT 
                           p.id as data_id
                            FROM {$wpdb->prefix}cf7_vdata p
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                               ON ( p.id = ve.data_id )
                              INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve1
                               ON ( p.id = ve1.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve2
                               ON ( p.id = ve2.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve3
                               ON ( p.id = ve3.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve4
                               ON ( p.id = ve4.data_id )
                            WHERE
                            ve.cf7_id = '".$form_id."'
                              AND ( ve.name = 'user_id' AND CAST(ve.value AS DECIMAL) = '".$customer_user_id."' )
                              AND ( ve1.name = 'order_id' AND CAST(ve1.value AS DECIMAL) = '".$order_id."' )
                              AND ( ve2.name = 'ac_form_title' AND ve2.value LIKE '".$item_name."' )
                              AND ( ve3.name = 'product_id' AND ve3.value = '".$product_id."' )
                              AND ( ve4.name = 'submit_type' AND ve4.value = 'Save &amp; Finish later' )  
                              GROUP BY p.id 
                              ORDER BY p.id
                              DESC"); 
      echo $wpdb->last_error;
      echo  $wpdb->print_error();
      if($saved_form_data)
      {
        $id = $saved_form_data[0]->data_id;
        $wpdb->query($wpdb->prepare( "DELETE FROM {$wpdb->prefix}cf7_vdata_entry WHERE data_id = %d", $id));
        $wpdb->query($wpdb->prepare( "DELETE FROM {$wpdb->prefix}cf7_vdata WHERE id = %d", $id));
        echo $wpdb->last_error;
        echo  $wpdb->print_error();
      }
      if( ! get_post_meta( $order_id, 'total_forms_submitted', true ) ) {
        $total_data = get_post_meta( $order_id, 'total_forms_submitted', true );
        $update_saved_data = $total_data + 1;
        update_post_meta( $order_id, 'total_forms_submitted', $update_saved_data );
      }  
    }
  }
  if($_POST['_wpcf7'] == '2030' || $_POST['_wpcf7'] == '2025' || $_POST['_wpcf7'] == '2029' || $_POST['_wpcf7'] == '2031' || $_POST['_wpcf7'] == '2043')
  {
    if(isset($_FILES['order-file']['name']) && $_FILES['order-file']['name'] =='')
    {
      if($_POST['saved-order-file'] != '') 
      {
        $upload_dir = wp_upload_dir();
        $target_dir = $upload_dir['basedir'].'/advanced-cf7-upload/';
        $filename = end(explode('/', $_POST['saved-order-file']));
        $filepath = $target_dir . $filename;
        $submission = WPCF7_Submission::get_instance();
        $submission->add_uploaded_file('order-file', $filepath);
      }
    }
    if(isset($_FILES['press-document']['name']) && $_FILES['press-document']['name'] =='')
    {
      if($_POST['saved-order-file'] != '') 
      {
        $upload_dir = wp_upload_dir();
        $target_dir = $upload_dir['basedir'].'/advanced-cf7-upload/';
        $filename = end(explode('/', $_POST['saved-order-document']));
        $filepath = $target_dir . $filename;
        $submission = WPCF7_Submission::get_instance();
        $submission->add_uploaded_file('press-document', $filepath);
      }
    }
  }
 
}


add_action("vsz_cf7_after_insert_db","vsz_cf7_after_insert_db_callback",10,3);
function vsz_cf7_after_insert_db_callback($contact_form, $cf7_id, $data_id)
{ 
  global $wpdb;
  $prefix =  $wpdb->prefix;
 if($_POST['_wpcf7'] == '1996' || $_POST['_wpcf7'] == '2042' || $_POST['_wpcf7'] == '2029' || $_POST['_wpcf7'] == '2030' || $_POST['_wpcf7'] == '2031' || $_POST['_wpcf7'] == '2025' || $_POST['_wpcf7'] == '2043' || $_POST['_wpcf7'] == '2028' || $_POST['_wpcf7'] == '2050')
  {
    $submit_type = $_POST['submit_type'];
    $order_id = $_POST['order_id'];
    $order = wc_get_order( $order_id );
    $uname = $order->get_billing_first_name() .' '. $order->get_billing_last_name();
    $customer_user_id = $_POST['user_id'];
    $item_number = $_POST['ac_form_title'];
    $item_name = substr(strstr($item_number," "), 1);
    $recurring = $_POST['recurring_form'];
    if($submit_type == 'Save & Continue')
    {
        $message = $uname. " Submitted Form for '" .$item_name . "' Order #".$order_id; 
    
    $wpdb->insert($prefix.'notification',
              array(
              'sent_to' => 'admin',
              'user_id' => $customer_user_id,
              'user_name' => $uname,
              'message' => $message,
              'status' => '0',
              'type' => 'form submit',
              'created' => current_time('mysql', 1),
              ),
              array(
              '%s', '%d', '%s', '%s', '%d', '%s', '%s'
              )
          ); 
    }
 $status = $wpdb->insert_id;
            //echo $status ? 'ok' : var_dump($wpdb);                    
}
}

add_action( 'woocommerce_checkout_update_order_meta', 'saving_checkout_cf_data');
function saving_checkout_cf_data( $order_id ) {
        $pageid = get_the_ID();
        update_post_meta( $order_id, 'order_page_id', $pageid );
}

add_action( 'woocommerce_thankyou', 'wc_thankyou_redirect');
function wc_thankyou_redirect( $order_id ){
    $order = new WC_Order( $order_id );
    $order_page_id = get_post_meta( $order_id, 'order_page_id', true );
    $url = get_site_url() . '/order-forms/';
    if ( $order->status != 'failed' && $order_page_id == '') {
      /*if( ! get_post_meta( $order_id, 'total_order_quantity', true ) ) {
      $order = wc_get_order( $order_id );
        update_post_meta( $order_id, 'total_order_quantity', $order->get_item_count() );
    }  
    if( ! get_post_meta( $order_id, 'total_forms_submitted', true ) ) {
      
        update_post_meta( $order_id, 'total_forms_submitted', '0' );
    }  */
        wp_redirect($url);
        exit;
    }
}


add_action('woocommerce_payment_complete', 'custom_process_order', 10, 1);
function custom_process_order($order_id) {
   global $wpdb;
  $prefix =  $wpdb->prefix;
    $order = new WC_Order( $order_id);
   
    if ( ! $order_id )
        return;
    if( ! get_post_meta( $order_id, 'total_order_quantity', true ) ) {
      $order = wc_get_order( $order_id );
        update_post_meta( $order_id, 'total_order_quantity', $order->get_item_count() );
    }  
    if( ! get_post_meta( $order_id, 'total_forms_submitted', true ) ) {
      
        update_post_meta( $order_id, 'total_forms_submitted', '0' );
    } 
    $uname = $order->get_billing_first_name() .' '. $order->get_billing_last_name();
    if($order->created_via == 'subscription'){
      $message = $uname ." Recurring order placed #" .$order_id;
       foreach($order->get_items() as $item_id => $item)
       {
                  $itemname =  $item['name'];
        }
        $noti_message = "Recurring ".$itemname." Order Item has been Enabled"; 
          $wpdb->insert($prefix.'notification',
                    array(
                    'sent_to' => 'user',
                    'user_id' => $order->get_customer_id(),
                    'user_name' => '',
                    'message' => $noti_message,
                    'status' => '0',
                    'type' => 'recurring item',
                    'created' => current_time('mysql', 1),
                    ),
                    array(
                    '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                    )
                );

    }
    else{
      $message = $uname ." Placed New order #" .$order_id;
    }
    $wpdb->insert($prefix.'notification',
              array(
              'sent_to' => 'admin',
              'user_id' => $order->get_customer_id(),
              'user_name' => $uname,
              'message' => $message,
              'status' => '0',
              'type' => 'new order',
              'created' => current_time('mysql', 1),
              ),
              array(
              '%s', '%d', '%s', '%s', '%d', '%s', '%s'
              )
          ); 
    return $order_id;
}

add_action( 'my_hookname', 'my_function' );
function my_function() {
  $to = 'arpatel@sigmasolve.net';
  $headers = 'From: Wlid Coffee <'.get_field('order_email_from', 'option').'>' . "\r\n";
    wp_mail( $to, 'WP Crontrol',  'WP Crontrol rocks!', $headers );
}

add_action( 'send_notification_to_user', 'send_notification_to_user_complete_form' );
function send_notification_to_user_complete_form(){
global $wpdb;
$siteurl = get_site_url() .'/order-forms/';       
$subject = 'Wild Coffee Marketing';

$mail_form_data = $wpdb->get_results("SELECT p.id as data_id FROM {$wpdb->prefix}cf7_vdata p
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                               ON ( p.id = ve.data_id )
                            WHERE (ve.name = 'submit_type' AND ve.value = 'Save &amp; Finish later') GROUP BY p.id ORDER BY p.id DESC"); 
                          echo $wpdb->last_error;
                          echo  $wpdb->print_error();       
if($mail_form_data)
{
  foreach ($mail_form_data as $form_data) 
  {
   
    $form_fields = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cf7_vdata_entry WHERE data_id = ".$form_data->data_id.""); 
    foreach ($form_fields as $field) 
    { 
      //echo $field->name .'- '. $field->value .'<br>';
      if($field->name == 'ac_form_title'){
      
        $form_title = $field->value;
      }
      if($field->name == 'user_id'){
        $user_id = $field->value;
      }

    }
    $user_info = get_userdata($user_id);
    
    $to = get_field('order_email', 'option');
    //$to = $user_info->user_email;
    $message =  "";
    if($user_info->first_name == ''){
    $message .= 'Hello,<br>';
    }
    else
    {
    $message .= 'Hello ' .  $user_info->first_name . ",<br>";   
    }
    $message .= "Please full fill the remaining information of '".$form_title."' order form <a href='". $siteurl ."' target='_blank' >Click here</a>.";
    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
    wp_mail( $to, $subject, $message);
  }
} 
}

function set_html_content_type() 
{
  return 'text/html';
}
remove_filter ( 'wp_mail_content_type', 'set_html_content_type' );

function total_orders_counts($status)
{ global $wpdb;
  if($status == 'all'){
    $customer_orders = wc_get_orders( array(
            'post_status' => array_keys( wc_get_order_statuses() ),
            'numberposts' => -1
    ) ); 
  }
  elseif($status == 'need-content')
  {

    $customer_orders = $wpdb->get_results("SELECT {$wpdb->prefix}posts.ID FROM {$wpdb->prefix}posts  INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )  INNER JOIN {$wpdb->prefix}postmeta AS mt1 ON ( {$wpdb->prefix}posts.ID = mt1.post_id ) WHERE 1=1  AND ( 
          ( {$wpdb->prefix}postmeta.meta_key = 'total_order_quantity' AND CAST({$wpdb->prefix}postmeta.meta_value AS SIGNED) != mt1.meta_value ) 
          AND 
          mt1.meta_key = 'total_forms_submitted'
        ) AND {$wpdb->prefix}posts.post_type = 'shop_order' AND {$wpdb->prefix}posts.post_status = 'wc-processing' GROUP BY {$wpdb->prefix}posts.ID ORDER BY {$wpdb->prefix}posts.post_date DESC");
  }
  else
  {
    $customer_orders = wc_get_orders( array(
            'post_status' => array($status),
            'numberposts' => -1
    ) );
  }
  echo count($customer_orders);
}
add_action( 'get_total_orders_counts', 'total_orders_counts', 10, 1 );

function wc_get_total_sales() {
global $wpdb;
$order_totals = apply_filters( 'woocommerce_reports_sales_overview_order_totals', $wpdb->get_row( "
SELECT SUM(meta.meta_value) AS total_sales, COUNT(posts.ID) AS total_orders FROM {$wpdb->posts} AS posts
LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
WHERE meta.meta_key = '_order_total'
AND posts.post_type = 'shop_order'
AND posts.post_status IN ( '" . implode( "','", array( 'wc-completed', 'wc-processing') ) . "' )
" ) );

echo number_format($order_totals->total_sales, 2);
}
add_action( 'get_total_sales', 'wc_get_total_sales', 10, 1 );

function wc_average_order_value() {
global $wpdb;
$order_totals =  $wpdb->get_row( "
SELECT SUM(meta.meta_value) AS total_sales, COUNT(posts.ID) AS total_orders FROM {$wpdb->posts} AS posts
LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
WHERE meta.meta_key = '_order_total'
AND posts.post_type = 'shop_order'
AND posts.post_status IN ( '" . implode( "','", array( 'wc-completed', 'wc-processing') ) . "' )
" ) ;
$total_revenue = $order_totals->total_sales;
//echo number_format($total_revenue);

    $customer_orders = wc_get_orders( array(
            'post_status' => array_keys( wc_get_order_statuses() ),
            'numberposts' => -1
    ) ); 
  $total_order = count($customer_orders);
echo number_format(round($total_revenue / $total_order),2);	
}
add_action( 'get_average_order', 'wc_average_order_value', 10, 1 );

function wc_average_monthly_service_length() {
 global $wpdb;
        $order_items = absint( $wpdb->get_var( "SELECT sum(om.meta_value) FROM {$wpdb->prefix}woocommerce_order_items as oi 
left join {$wpdb->prefix}woocommerce_order_itemmeta as om on om.order_item_id=oi.order_item_id
left join {$wpdb->prefix}posts as p on oi.order_id=p.ID
WHERE order_item_type='line_item' AND meta_key='_qty' AND post_type = 'shop_order' AND post_status IN ('wc-processing', 'wc-completed')" )  );
        echo round($order_items/12);  
}
add_action( 'get_average_monthly_service_length', 'wc_average_monthly_service_length', 10, 1 );

function all_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $all_orders = wc_get_orders( array(
            'post_status' => array_keys( wc_get_order_statuses() ),
            'numberposts' => 10,
            'date_created' => $odate,
            'paged' => $paged
        ) ); 

  if($all_orders)
  {  
      foreach($all_orders as $order )
      { ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status">
                  <?php if($order->get_status() == 'processing'){ ?>
                      <span class="new-order">New Order</span>
                  <?php } 
                  elseif($order->get_status() == 'completed'){ ?>
                      <span class="completed-order"><?php echo $order->get_status(); ?></span>
                  <?php } 
                  elseif($order->get_status() == 'cancelled'){ ?>
                      <span class="completed-order"><?php echo $order->get_status(); ?></span>
                  <?php } 
                  else { ?>
                      <span class="other-order"><?php echo $order->get_status(); ?></span>
                  <?php } ?>
              </td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date">
                  <?php $completed = $order->date_created;
                  foreach ($completed as $key => $value) {
                      if($key == 'date'){
                          //echo $value;
                          echo date('m/d/Y', strtotime($value));
                      } 
                  } ?>
              </td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="edit-order"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> 
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_all_order_lists', 'all_order_lists', 10 );
add_action('wp_ajax_nopriv_all_order_lists', 'all_order_lists', 10 );

function completed_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $all_orders = wc_get_orders( array(
            'post_status' => array('completed'),
            'numberposts' => 10,
            'date_modified' => $odate,
            'paged' => $paged
        ) ); 

  if($all_orders)
  {  
      foreach($all_orders as $order )
      { ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status"><span class="completed-order">Delivered</span></td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date">
                  <?php $completed = $order->date_modified;
                  foreach ($completed as $key => $value) {
                      if($key == 'date'){
                          echo date('m/d/Y', strtotime($value));
                      } 
                  } ?>
              </td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_completed_order_lists', 'completed_order_lists', 10 );
add_action('wp_ajax_nopriv_completed_order_lists', 'completed_order_lists', 10 );

function new_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $all_orders = wc_get_orders( array(
            'post_status' => array('wc-processing'),
            'numberposts' => 10,
            'date_created' => $odate,
            'paged' => $paged
        ) ); 

  if($all_orders)
  {  
      foreach($all_orders as $order )
      { ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status"><span class="new-order">New Order</span></td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date">
                  <?php $completed = $order->date_created;
                  foreach ($completed as $key => $value) {
                      if($key == 'date'){
                          //echo $value;
                          echo date('m/d/Y', strtotime($value));
                      } 
                  } ?>
              </td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                       <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="edit-order"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> 
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_new_order_lists', 'new_order_lists', 10 );
add_action('wp_ajax_nopriv_new_order_lists', 'new_order_lists', 10 );

function cancelled_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $all_orders = wc_get_orders( array(
            'post_status' => array('cancelled'),
            'numberposts' => 10,
            'date_modified' => $odate,
            'paged' => $paged
        ) ); 

  if($all_orders)
  {  
      foreach($all_orders as $order )
      { ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status"><span class="new-order"><?php echo $order->get_status(); ?></span></td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date">
                  <?php $completed = $order->date_modified;
                  foreach ($completed as $key => $value) {
                      if($key == 'date'){
                          //echo $value;
                          echo date('m/d/Y', strtotime($value));
                      } 
                  } ?>
              </td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_cancelled_order_lists', 'cancelled_order_lists', 10 );
add_action('wp_ajax_nopriv_cancelled_order_lists', 'cancelled_order_lists', 10 );

function need_content_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $per_page = 10;
  $offset = ($paged-1)*$per_page; 
  $all_orders = $wpdb->get_results("SELECT {$wpdb->prefix}posts.ID FROM {$wpdb->prefix}posts  INNER JOIN {$wpdb->prefix}postmeta ON ( {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id )  INNER JOIN {$wpdb->prefix}postmeta AS mt1 ON ( {$wpdb->prefix}posts.ID = mt1.post_id ) WHERE 1=1  AND ( 
          ( {$wpdb->prefix}postmeta.meta_key = 'total_order_quantity' AND CAST({$wpdb->prefix}postmeta.meta_value AS SIGNED) != mt1.meta_value ) 
          AND 
          mt1.meta_key = 'total_forms_submitted'
        ) AND {$wpdb->prefix}posts.post_type = 'shop_order' AND {$wpdb->prefix}posts.post_status = 'wc-processing' AND {$wpdb->prefix}posts.post_date LIKE '".$odate."%' GROUP BY {$wpdb->prefix}posts.ID ORDER BY {$wpdb->prefix}posts.post_date DESC LIMIT ".$offset.", ".$per_page."");

  if($all_orders)
  {  
      foreach($all_orders as $order )
      { $order = wc_get_order( $order->ID ); ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status"><span class="new-order"><?php echo $order->get_status(); ?></span></td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date">
                  <?php $completed = $order->date_created;
                  foreach ($completed as $key => $value) {
                      if($key == 'date'){
                          //echo $value;
                          echo date('m/d/Y', strtotime($value));
                      } 
                  } ?>
              </td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_need_content_order_lists', 'need_content_order_lists', 10 );
add_action('wp_ajax_nopriv_need_content_order_lists', 'need_content_order_lists', 10 );

function feedback_order_lists(){
  global $wpdb, $paged;
  $paged = $_REQUEST['page'];
  $odate = '';
  if($_POST['order-date']){
      $odate = date("Y-m-d", strtotime($_POST['order-date']));
  }
  $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
  $per_page = 10;
  $offset = ($paged-1)*$per_page; 
  $all_orders = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
            WHERE status = 'feedback' AND completed_date LIKE '".$odate."%' ORDER BY completed_date DESC LIMIT ".$offset.", ".$per_page."");

  if($all_orders)
  {  
      foreach($all_orders as $orders )
      { $order = wc_get_order( $orders->order_id ); ?>
          <tr>
              <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
              <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
              <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
              <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
              <td class="order-status"><span class="feedback-order-status">Feedback</span></td>
              <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
              <td class="order-date"><?php echo date('m/d/Y', strtotime($orders->completed_date)); ?></td>
              <td class="order-action">
                  <div class="order-action-inner">
                      <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                 </div>
              </td>
          </tr>
      <?php } 
  } 
 die();        
}
add_action('wp_ajax_feedback_order_lists', 'feedback_order_lists', 10 );
add_action('wp_ajax_nopriv_feedback_order_lists', 'feedback_order_lists', 10 );

function admin_new_content_upload(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
       $valid_extensions = array('pdf' , 'doc' , 'docx' );
        if (!function_exists('wp_handle_upload')) {
           require_once(ABSPATH . 'wp-admin/includes/file.php');
       }
      $img = $_FILES["file"]["name"];
       $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
      
           /*if($_FILES["file"]['size'] > 500000){
       die('<div class="alert alert-danger" role="alert"> File is too big </div>');
    }*/
if(in_array($ext, $valid_extensions)) 
{ 

      $uploadedfile = $_FILES['file'];
      $upload_overrides = array('test_form' => false);
      $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
      if ($movefile && !isset($movefile['error'])) {
         //echo $movefile['url'];
        $siteurl = get_site_url() .'/user-order-detail/';       
        $subject = 'Wild Coffee Marketing';
        $user_info = get_userdata($_POST['user_id']);   
        $to = get_field('order_email', 'option');
        //$to = $_POST['user_email'];
        $message =  "";
        if($user_info->first_name == ''){
        $message .= 'Hello,<br>';
        }
        else
        {
        $message .= 'Hello ' .  $user_info->first_name . ",<br>";   
        }
        $message .= 'Service Name: ' .$_POST['product_name'].'<br>';
        $message .= "Your New content has been created, please review it from the attached file. Review it approve the content from site <a href='". $siteurl ."' target='_blank' >Click here</a>.";
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        $attachments = $movefile['file'];
        $headers = 'From: Wlid Coffee <'.get_field('order_email_from', 'option').'>' . "\r\n";
        wp_mail($to, $subject, $message, $headers, $attachments);
        
        $wpdb->insert($prefix.'order_contents',
              array(
              'order_id' => $_POST['order_id'],
              'product_id' => $_POST['product_id'],
              'user_id' => $_POST['user_id'],
              'item_number' => $_POST['item_number'],
              'product_name' => $_POST['product_name'],
              'status' => 'sent',
              'content_file' => $movefile['url'],
              'completed_date' => current_time('mysql', 1)
              ),
              array(
              '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%s'
              )
          );
          $status = $wpdb->insert_id;
          //echo $status ? 'ok' : var_dump($wpdb);  
          
          $message = "New Content Placed on '".$_POST['product_name'] ."'"; 
          
          $wpdb->insert($prefix.'notification',
                    array(
                    'sent_to' => 'user',
                    'user_id' => $_POST['user_id'],
                    'user_name' => '',
                    'message' => $message,
                    'status' => '0',
                    'type' => 'new content',
                    'created' => current_time('mysql', 1),
                    ),
                    array(
                    '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                    )
                ); 
          //echo $status ? 'ok' : var_dump($wpdb);  
          echo "success";
    } else {
        /**
         * Error generated by _wp_handle_upload()
         * @see _wp_handle_upload() in wp-admin/includes/file.php
         */
        echo "<p class='error'>".$movefile['error']."</p>";
    }
  }else{
    echo "<p class='error'>You are not allowed to upload files of this type.</p>";
  }
    die();
  
 }
 add_action( 'wp_ajax_admin_new_content_upload','admin_new_content_upload' );
 add_action( 'wp_ajax_nopriv_admin_new_content_upload','admin_new_content_upload' );

function admin_updated_content_upload(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
       $valid_extensions = array('pdf' , 'doc' , 'docx' );
        if (!function_exists('wp_handle_upload')) {
           require_once(ABSPATH . 'wp-admin/includes/file.php');
       }
      $img = $_FILES["file"]["name"];
       $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
      
           /*if($_FILES["file"]['size'] > 500000){
       die('<div class="alert alert-danger" role="alert"> File is too big </div>');
    }*/
if(in_array($ext, $valid_extensions)) 
{ 

      $uploadedfile = $_FILES['file'];
      $upload_overrides = array('test_form' => false);
      $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
      if ($movefile && !isset($movefile['error'])) {
         //echo $movefile['url'];
        $siteurl = get_site_url() .'/user-order-detail/';       
        $subject = 'Wild Coffee Marketing';
        $user_info = get_userdata($_POST['user_id']);   
        $to = get_field('order_email', 'option'); 
        //$to = $_POST['user_email'];
        $message =  "";
        if($user_info->first_name == ''){
        $message .= 'Hello,<br>';
        }
        else
        {
        $message .= 'Hello ' .  $user_info->first_name . ",<br>";   
        }
         $message .= 'Service Name: ' .$_POST['product_name'].'<br>';
        $message .= "Your content has been Updated, please review it from the attached file. Review it and approv the content from site <a href='". $siteurl ."' target='_blank' >Click here</a>.";
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        $attachments = $movefile['file'];
        $headers = 'From: Wlid Coffee <'.get_field('order_email_from', 'option').'>' . "\r\n";
        wp_mail($to, $subject, $message, $headers, $attachments);

        $result =  $wpdb->update( $prefix.'order_contents',array(
                'content_file'=>$movefile['url'], 'status'=>'updated'), array('id'=>$_POST['item_row_id']));
        $message = "Updated Content Placed on '".$_POST['product_name'] ."'"; 
        $wpdb->insert($prefix.'notification',
                  array(
                  'sent_to' => 'user',
                  'user_id' => $_POST['user_id'],
                  'user_name' => '',
                  'message' => $message,
                  'status' => '0',
                  'type' => 'updated content',
                  'created' => current_time('mysql', 1),
                  ),
                  array(
                  '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                  )
              ); 
        //echo $wpdb->last_query;
        //echo $wpdb->query_error;
          echo "success";
    } else {
        /**
         * Error generated by _wp_handle_upload()
         * @see _wp_handle_upload() in wp-admin/includes/file.php
         */
        echo "<p class='error'>".$movefile['error']."</p>";
    }
  }else{
    echo "<p class='error'>You are not allowed to upload files of this type.</p>";
  }
    die();
  
 }
 add_action( 'wp_ajax_admin_updated_content_upload','admin_updated_content_upload' );
 add_action( 'wp_ajax_nopriv_admin_updated_content_upload','admin_updated_content_upload' );


function user_feedback_data(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
  $id = $_POST['item_row_id'];
$siteurl = get_site_url() .'/admin-order-detail/?id=' .$_POST['order_id'];       
$subject = 'Wild Coffee Marketing Feedback #' .$_POST['order_id'];
$user_info = get_userdata($_POST['user_id']);   
$to = get_field('order_email', 'option');
$message =  "";
$message .= 'Hello Admin<br>';   
$message .= "User has submitted feedback on '".$_POST['product_name']."'.<br>";
if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
{
       $valid_extensions = array('pdf' , 'doc' , 'docx' );
        if (!function_exists('wp_handle_upload')) {
           require_once(ABSPATH . 'wp-admin/includes/file.php');
       }
      $img = $_FILES["file"]["name"];
      $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
      /*if($_FILES["file"]['size'] > 500000){
       die('<div class="alert alert-danger" role="alert"> File is too big </div>');
      }*/
      if(in_array($ext, $valid_extensions)) 
      { 

            $uploadedfile = $_FILES['file'];
            $upload_overrides = array('test_form' => false);
            $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
            if ($movefile && !isset($movefile['error'])) {
               //echo $movefile['url'];
              $feedback = '';
              $message .= "Find Feedback from the attachment or you can view on site from <a href='". $siteurl ."' target='_blank' >here</a>.<br>";
              if($_POST['feedback'] != ''){
                $message .= '<strong>Feedback:</strong><br>';
                $message .= stripslashes($_POST['feedback']);
                $feedback = $_POST['feedback'];
              }
              add_filter( 'wp_mail_content_type', 'set_html_content_type' );
              $attachments = $movefile['file'];
              $headers = 'From: '.$user_info->first_name.' <'.get_field('order_email_from', 'option').'>' . "\r\n";
              wp_mail($to, $subject, $message, $headers, $attachments);
              $result =  $wpdb->update($prefix.'order_contents',array(
                'user_feedback'=>$feedback, 'user_feedback_file'=>$movefile['url'], 'status'=>'feedback'), array('id'=>$_POST['item_row_id']));
              //$wpdb->query($wpdb->prepare("UPDATE $wpdb->order_contents SET user_feedback='".$feedback."', user_feedback_file = '".$movefile['url']."' WHERE id=".$id));
              $uname = $user_info->first_name .' '. $user_info->last_name;
              $message = $uname ." Submitted Feedback on '".$_POST['product_name'] ."' Order #" . $_POST['order_id']; 
              $wpdb->insert($prefix.'notification',
                        array(
                        'sent_to' => 'admin',
                        'user_id' => $_POST['user_id'],
                        'user_name' => $uname,
                        'message' => $message,
                        'status' => '0',
                        'type' => 'feedback',
                        'created' => current_time('mysql', 1),
                        ),
                        array(
                        '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                        )
                    ); 
                echo "success";
          } else {
              /**
               * Error generated by _wp_handle_upload()
               * @see _wp_handle_upload() in wp-admin/includes/file.php
               */
              echo "<p class='error'>".$movefile['error']."</p>";
          }
      }
      else{
        echo "<p class='error'>You are not allowed to upload files of this type.</p>";
      }
}else{
  if($_POST['feedback'] != ''){
              $message .= "Review below feedback or you can view on site from <a href='". $siteurl ."' target='_blank' >here</a>.<br>";
                $message .= '<strong>Feedback:</strong><br>';
                $message .= stripslashes($_POST['feedback']);
                 add_filter( 'wp_mail_content_type', 'set_html_content_type' );
              $headers = 'From: '.$user_info->first_name.' <'.get_field('order_email_from', 'option').'>' . "\r\n";
              wp_mail($to, $subject, $message, $headers);

              $result =  $wpdb->update( $prefix.'order_contents',array(
                'user_feedback'=>$_POST['feedback'], 'status'=>'feedback'), array('id'=>$_POST['item_row_id']));
              //$wpdb->query($wpdb->prepare("UPDATE $wpdb->order_contents SET user_feedback='".$_POST['feedback']."' WHERE id=".$id));
              $uname = $user_info->first_name .' '. $user_info->last_name;
              $message = $uname ." Submitted Feedback on '".$_POST['product_name'] ."' Order #" . $_POST['order_id']; 
              $wpdb->insert($prefix.'notification',
                        array(
                        'sent_to' => 'admin',
                        'user_id' => $_POST['user_id'],
                        'user_name' => $uname,
                        'message' => $message,
                        'status' => '0',
                        'type' => 'feedback',
                        'created' => current_time('mysql', 1),
                        ),
                        array(
                        '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                        )
                    ); 
              echo "success";
              }
}
    die();
 }
 add_action( 'wp_ajax_user_feedback_data','user_feedback_data' );
 add_action( 'wp_ajax_nopriv_user_feedback_data','user_feedback_data' );

function user_view_content_entry(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
  $result =  $wpdb->update($prefix.'order_contents',array(
                'downloaded_content'=>'1'), array('id'=>$_POST['item_row_id']));
  if($result >= 0)
  {
    echo "succes";
  }
  die();
 }
 add_action( 'wp_ajax_user_view_content_entry','user_view_content_entry' );
 add_action( 'wp_ajax_nopriv_user_view_content_entry','user_view_content_entry' );

function user_approved_content(){
  
  global $wpdb;
  $prefix =  $wpdb->prefix;
  $customer_user_id = get_current_user_id(); 
  $order_id = $_POST['order_id'];
  $order = wc_get_order( $order_id );
  $result =  $wpdb->update($prefix.'order_contents',array('status'=>'approved', 'approve'=>'1', 'completed_date' => current_time('mysql', 1)), array('id'=>$_POST['item_row_id']));
  
  $uname = $order->get_billing_first_name() .' '. $order->get_billing_last_name();
  $message = $uname . " Approved Content of '".$_POST['itemname']."' Order #". $_POST['order_id']; 
  $wpdb->insert($prefix.'notification',
            array(
            'sent_to' => 'admin',
            'user_id' => $customer_user_id,
            'user_name' => $uname,
            'message' => $message,
            'status' => '0',
            'type' => 'approved content',
            'created' => current_time('mysql', 1),
            ),
            array(
            '%s', '%d', '%s', '%s', '%d', '%s', '%s'
            )
        ); 

        $item_count = $order->get_item_count();
        //echo $item_count;
        $total_approved = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
                                              WHERE order_id = '".$order_id."' and approve = '1' ORDER BY completed_date DESC");
      //echo sizeof($total_approved);
      if($item_count == sizeof($total_approved))
      {
        $order = new WC_Order($order_id);
         $order->update_status( 'completed' );
         $message = "Order #". $_POST['order_id']. " has been Completed of ".$uname; 
         $wpdb->insert($prefix.'notification',
                  array(
                  'sent_to' => 'admin',
                  'user_id' => $customer_user_id,
                  'user_name' => $uname,
                  'message' => $message,
                  'status' => '0',
                  'type' => 'completed',
                  'created' => current_time('mysql', 1),
                  ),
                  array(
                  '%s', '%d', '%s', '%s', '%d', '%s', '%s'
                  )
          ); 

      }
 
//echo $wpdb->num_rows;
  if($result >= 0)
  {
    echo "succes";
  }
  die();
 }
 add_action( 'wp_ajax_user_approved_content','user_approved_content' );
 add_action( 'wp_ajax_nopriv_user_approved_content','user_approved_content' );


function user_all_order_lists(){
global $wpdb, $paged;
$paged = $_REQUEST['page'];
$odate = '';
 if($_POST['order-date']){
    $odate = date("Y-m-d", strtotime($_POST['order-date']));
}
$paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
$per_page = 4;
$offset = ($paged-1)*$per_page; 
$customer_user_id = get_current_user_id(); 
 $order_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
      WHERE user_id = '".$customer_user_id."' AND product_name LIKE '".$_POST['order-name']."%' AND completed_date LIKE '".$odate."%' ORDER BY completed_date DESC LIMIT ".$offset.", ".$per_page."");
// print_r($order_data);
if ($order_data)
{ 
    foreach ($order_data as $order) 
    {
        $product_id = $order->product_id; 
     $orders = wc_get_order( $order->order_id );
  foreach ( $orders->get_items() as $item_id => $item ) {
 $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
   $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
   $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false );
  }
        $completed_date = get_post_meta( $order->order_id, '_completed_date', true );
        
       ?>
       <div class="user-order-detail-list order-<?php echo $order->id; ?>">
            <div class="status-icon">
                 <?php if($order->status == 'feedback')
                {  ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-waiting-icon.png">
                <?php } if($order->status == 'approved') { ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-downloded.png">
                <?php } ?>
            </div>
            <div class="product-icon">
                <?php if(get_field( 'product_icon', $product_id )) { ?>
                <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                <?php } ?>
            </div>
            <div class="order-title-area">
                <div class="stat-icon dib flat-color-1">
                    <div class="order-content">
                        <h3><?php echo $order->product_name; ?></h3>
                    </div>
                </div>
                <?php if($item_word){ ?>
                        <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                    <?php } ?>
                     <?php if($item_post_count){ ?>
                        <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                    <?php } ?>
            </div>
            <div class="order-complete-date">  
                <?php if($order->completed_date) { ?>
                <span class="name">
                <?php echo 'Completed: '. date('m/d/y \a\t H:i A', strtotime($order->completed_date)); ?>
                </span>
            <?php } ?>
            </div>
            <div class="order-review-btn download-content-btn">
                <span class="badge badge-complete">
                    <span class="download-content button1" data-id="<?php echo $order->id; ?>">View Content</span>
                    <span id="download-content-click" data-url="<?php echo $order->content_file; ?>" onclick="window.open('<?php echo $order->content_file; ?>')">
                    </span>
                </span>
            </div>
            <div class="order-review-btn show-feeback-btn">
                <span class="show-feeback <?php if($order->status == 'approved' ) { echo 'disabled'; } ?>">Send Feedback</span>
            </div>
            <?php  if($order->downloaded_content == 1) { ?>
            <div class="order-review-btn approve-btn">
                 <?php  if($order->status == 'approved') { ?>
                    <span class="approve-selector approved-content">Approved!</span>   
                <?php } else{ ?>
                    <span class="approve-selector approve-content" data-id="<?php echo $order->id; ?>" data-orderid="<?php echo $order->order_id; ?>">Approve?</span>    
                <?php } ?>
            </div>
            <?php } ?>
            <div class="user-feedback-box">
                <div class="user-feedback-box-inner">
                    <p>Feedback on the content we wrote. This is used to let us know if there is anything that needs to be corrected. Please be descriptive with your feedback, and once submitted, and once submitted we will review and reach out to you.</p>
                    <form action="#" method="post" enctype="multipart/form-data" id="user-feedback-form">
                        <textarea class="send-feeback-field" id="description" cols="50" rows="7" name="feedback" placeholder="Feedback. ( Max 500 word count )" onkeyup="wordcounts<?php echo $order->id; ?>(this.value);"></textarea>
                         <input type="hidden" name="words" id="word_count" size=4 readonly class="word-count" value="0">
                         <input type="hidden" name="item_row_id" id="item_row_id" value="<?php echo $order->id; ?>">
                         <input type="hidden" name="user_id" id="user_id" value="<?php echo $order->user_id; ?>">
                         <input type="hidden" name="order_id" id="order_id" value="<?php echo $order->order_id; ?>">
                        <input type="hidden" name="product_name" id="product_name" value="<?php echo $order->product_name; ?>">
                        <div class="upload-feedback-input">
                        <div class="upload-content-btn">
                            <input type="button" name="button" class="button send-feedback" value="SUBMIT FEEDBACK" id="send-feedback">
                            <div class="feedback-msg"></div>
                        </div>
                        <div class="filebutton button">
                           Upload Feedback <input type="file" id="feedback_file" name="feedback_file">
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
        <script type="text/javascript">
          function wordcounts<?php echo $order->id; ?>(textarea)
          {
            var chars=textarea.length,
            words=textarea.match(/\w+/g).length;
            jQuery('.order-<?php echo $order->id; ?> #word_count').val(words);
            var counts = jQuery('.order-<?php echo $order->id; ?> #word_count').val();
            if(counts > 500)
            {
            jQuery('.order-<?php echo $order->id; ?> #description').css('border', '1px solid #ff0000');
            }
            else
            {    
             jQuery('.order-<?php echo $order->id; ?> #description').css('border', '1px solid #008000');
            }
          }
          jQuery(document).ready(function () {  
            jQuery('.order-<?php echo $order->id; ?> .show-feeback').click(function () {
                jQuery('.order-<?php echo $order->id; ?> .user-feedback-box').slideToggle( "slow" );
            });
            
        });    
        </script>
       <?php
    }
}
 die();        
}
add_action('wp_ajax_user_all_order_lists', 'user_all_order_lists', 10 );
add_action('wp_ajax_nopriv_user_all_order_lists', 'user_all_order_lists', 10 );


function admin_notification(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
  if($_POST["view"] != '')
  {
      $result =  $wpdb->update( $prefix.'notification',array(
                'status'=>'1'), array('sent_to'=>'admin', 'status'=>'0'));
  }
  $notification_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}notification WHERE sent_to LIKE 'admin' AND status = '1' ORDER BY id DESC LIMIT 5 ");
  if ($notification_data)
  { 
      foreach ($notification_data as $notification) 
      {
          $output .= '<p class="noti-message">'. $notification->message.'</p>';
       } 
  }else{
    $output .= '<p class="noti-message">No Notification Found</p>';
  }
   $notification_count = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}notification WHERE sent_to = 'admin' AND status = '0'");
   $total = sizeof($notification_count);
   $data = array(
    'notification' => $output,
    'unseen_notification'  => $total
  );
  echo json_encode($data);
 die();        
} 
add_action('wp_ajax_admin_notification', 'admin_notification', 10 );
add_action('wp_ajax_nopriv_admin_notification', 'admin_notification', 10 );


function user_notification(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
  $current_user_id = get_current_user_id();
  if($_POST["view"] != '')
  {
      $result =  $wpdb->update( $prefix.'notification',array('status'=>'1'), array('user_id'=>$current_user_id, 'sent_to'=>'user', 'status'=>'0'));
      //$wpdb->query($wpdb->prepare("UPDATE ".$prefix."notification SET status='1' WHERE user_id = '".$current_user_id."' and sent_to LIKE 'user' and status = '0'"));
      
  }
  $notification_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}notification WHERE sent_to LIKE 'user' AND user_id = '".$current_user_id."' AND status = '1' ORDER BY id DESC LIMIT 5");
  if ($notification_data)
  { 
      foreach ($notification_data as $notification) 
      {
          $output .= '<p class="noti-message">'. $notification->message.'</p>';
       } 
  }else{
    $output .= '<p class="noti-message">No Notification Found</p>';
  }
   $notification_count = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}notification WHERE sent_to LIKE 'user' AND user_id = '".$current_user_id."' AND status = '0'");
   $total = sizeof($notification_count);
   $data = array(
    'notification' => $output,
    'unseen_notification'  => $total
  );
  echo json_encode($data);
 die();        
} 
add_action('wp_ajax_user_notification', 'user_notification', 10 );
add_action('wp_ajax_nopriv_user_notification', 'user_notification', 10 );




add_action( 'admin_bar_menu', 'wc_custom_admin_bar_link', 100 );
function wc_custom_admin_bar_link( $admin_bar ) {
    $admin_bar->add_menu( array(
  'id'    => 'wp-custom-link',
  'title' => '<span class="ab-icon dashicons-welcome-view-site"></span>Admin Dashboard',
  'href'  => site_url() . '/admin-dashboard/',
  'meta'  => array(
             'title' => __('Admin Dashboard'),
             'target' => '_blank',
  ),
    ));
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'cinchws_filter_dropdown_args', 10 );

function cinchws_filter_dropdown_args( $args ) {
  if(is_page( 2124 )){
    $var_tax = wc_attribute_label( $args['attribute'] );
    $args['show_option_none'] = apply_filters( '', $var_tax );}
    return $args;
}

add_action('woocommerce_single_product_summary', 'wc_woocommerce_product_total_price', 10);
function wc_woocommerce_product_total_price() {
    global $woocommerce, $product;
     if ($product->is_type( 'simple' )) { 
    // Setup our Html to show the totoal price
    echo '<div id="wc-single-product-price" style="display:none"></div>';
    ?>
    <script>
        jQuery(function ($) {
          $('[name=quantity]').val('1');
            var product_price = <?php echo $product->get_price(); ?>,
                    currency = '<?php echo get_woocommerce_currency_symbol(); ?>';
 
            $('[name=quantity]').change(function () {
                if (!(this.value < 1)) {
                    var product_total = parseFloat(product_price * this.value);
                    $('#wc-single-product-price').show();
                    $('#wc-single-product-price').html(currency + product_total.toFixed(2));
                    $('p.price').hide();
                }
               // $('#pt-product-total-price,#pt-cart-total-price').toggle(!(this.value <= 1));
 
            });
        });
    </script>
    <?php
  }

}

add_action( 'woocommerce_before_add_to_cart_button', 'custom_wc_template_single_price', 20);
function custom_wc_template_single_price(){
    global $product;
// Variable product only
if($product->is_type('variable') && is_product()):
 echo '<div id="pt-product-total-price"></div>';
 ?>
    <script>
    jQuery(document).ready(function($) {
      $('[name=quantity]').val('1');
      $('#pa_word-count, #pa_reoccurring-service-for, #pa_social-media, #pa_number-of-posts, #pa_reoccurring-service-for').val('');
      $(".quantity").addClass('wc_qty');
      currency = '<?php echo get_woocommerce_currency_symbol(); ?>';
      $('select').blur( function(){
        $('[name=quantity]').val('1');
        $('#pt-product-total-price').hide();
          if( '' != $('input.variation_id').val() ){
            $(".quantity").removeClass('wc_qty');
          } else {
              $(".quantity").addClass('wc_qty');
          }
      });
      $('[name=quantity]').change(function () {
        if (!(this.value < 1)) {  
          $('.woocommerce-variation.single_variation').hide();
          var price =  $('div.woocommerce-variation-price > span.price .woocommerce-Price-amount').eq(0).text();
          var month = $('div.woocommerce-variation-price > span.price .subscription-details').text();
          price = price.replace('$','');
          var product_total = parseFloat(price * this.value);
          $('#pt-product-total-price').show();  
          $('#pt-product-total-price').html(currency + product_total.toFixed(2) + month);
        }
      });
    });
    </script>
    <?php
endif;
}


function subscription_product_price(){
  global $wpdb;
  $prefix =  $wpdb->prefix;
  $id = $_POST['product_id'];
  
  if($id != ''){
  $product_price = $wpdb->get_results("SELECT min_price FROM {$wpdb->prefix}wc_product_meta_lookup WHERE product_id = '{$id}'");
 
          echo $product_price[0]->min_price;
      }

    die();
 }
 add_action( 'wp_ajax_subscription_product_price','subscription_product_price' );
 add_action( 'wp_ajax_nopriv_subscription_product_price','subscription_product_price' );

 function filter_plugin_updates( $value ) {
    unset( $value->response['woocommerce-gateway-stripe/woocommerce-gateway-stripe.php'] );
    unset( $value->response['woocommerce-product-table/woocommerce-product-table.php'] );
    unset( $value->response['advanced-custom-fields-pro/acf.php'] );
    unset( $value->response['woocommerce-auto-added-coupons/woocommerce-jos-autocoupon.php'] );
    unset( $value->response['woocommerce/woocommerce.php'] );
    unset( $value->response['cf7-conditional-fields/contact-form-7-conditional-fields.php'] );
    unset( $value->response['cf7-repeatable-fields/cf7-repeatable-fields.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );