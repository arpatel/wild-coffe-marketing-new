<?php 

/* Template name: User Order Detail*/


get_header('user');
$per_page = 4;
global $wpdb, $paged; ?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
 <?php  $customer_user_id = get_current_user_id();  ?> 
       <!-- Content -->
        <div class="content"> <div class="wc-loader"></div>
            <!-- Animated -->
            <div class="animated fadeIn user-all-order-page">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Current Completed Content Orders,</strong>
                                <p>Checkout your completed order(s) content to review, download or leave feedback.</p>
                            </div>
                            <div class="order-search">
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
							<div class="user-order-list-top">
                                <ul>
                                    <li><span class="order-status-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-downloded.png"></span>Downloaded</li>
                                    <li><span class="order-status-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-waiting-icon.png"></span>Submitted feedback & waiting for response</li>
                                </ul>
                            </div>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <form method="post" action="" name="order-search">
                                    <input type="text" name="order-date" value="<?php if($_POST['order-date']) echo $_POST['order-date'] ?>" placeholder="Date" id="order-date" >
                                    <input type="text" name="order-name" value="<?php if($_POST['order-name']) echo $_POST['order-name'] ?>" placeholder="Order Name" id="order-name">
                                    <input type="submit" name="submit" value="Search with Filter(s)...">
                                </form>
								</div>
                            </div>
   

                            <div class="user-order-list-wrapper">
                                <div id="review-order-list">
                                    <?php 
                                    $odate = '';
                                    $order_id = '%';
                                    if($_GET['id']){
                                        $order_id = $_GET['id'];
                                    }
                                    
                                     if($_POST['order-date']){
                                        $odate = date("Y-m-d", strtotime($_POST['order-date']));
                                    }
                                    if($_POST['submit'] == 'Search with Filter(s)...'){
                                        $order_id = '%';
                                    }
                                    $total_order_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
                                        WHERE id LIKE '".$order_id."' AND user_id = '".$customer_user_id."' AND product_name LIKE '".$_POST['order-name']."%' AND completed_date LIKE '".$odate."%' ORDER BY completed_date DESC");

                                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                    $offset = ($paged-1)*$per_page;

                                        $order_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
                                        WHERE id LIKE '".$order_id."' AND user_id = '".$customer_user_id."' AND product_name LIKE '".$_POST['order-name']."%' AND completed_date LIKE '".$odate."%' ORDER BY completed_date DESC LIMIT ".$offset.", ".$per_page."");
                                        
                                       // print_r($order_data);
                                    if ($order_data)
                                    { 
                                        foreach ($order_data as $order) 
                                        {   
                                            $product_id = $order->product_id; 
                                            $orders = wc_get_order( $order->order_id );
                                            foreach ( $orders->get_items() as $item_id => $item ) {
                                           $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
                                             $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
                                             $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false );
                                            }
                                             $completed_date = get_post_meta( $order->order_id, '_completed_date', true );
                                           ?>
                                           <div class="user-order-detail-list order-<?php echo $order->id; ?>">
                                                <div class="status-icon">
                                                     <?php if($order->status == 'feedback')
                                                    {  ?>
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-waiting-icon.png">
                                                    <?php } if($order->status == 'approved') { ?>
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-downloded.png">
                                                    <?php } ?>
                                                </div>
                                                <div class="product-icon">
                                                    <?php if(get_field( 'product_icon', $product_id )) { ?>
                                                    <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                                                    <?php } ?>
                                                </div>
                                                <div class="order-title-area">
                                                    <div class="stat-icon dib flat-color-1">
                                                        <div class="order-content">
                                                            <h3><?php echo $order->product_name; ?></h3>                    
                                                        </div>
                                                    </div>
													<?php if($item_word){ ?>
                                                            <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                        <?php } ?>
                                                         <?php if($item_post_count){ ?>
                                                            <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                        <?php } ?>
                                                </div>
                                                <div class="order-complete-date">  
                                                    <?php if($order->completed_date) { ?>
                                                    <span class="name">
                                                    <?php echo 'Completed: '. date('m/d/y \a\t g:i A', strtotime($order->completed_date)); ?>
                                                    </span>
                                                <?php } ?>
                                                </div>
                                                <div class="order-review-btn download-content-btn">
                                                    <span class="badge badge-complete">
                                                        <span class="download-content button1" data-id="<?php echo $order->id; ?>">View Content</span>
                                                        <span id="download-content-click" data-url="<?php echo $order->content_file; ?>" onclick="window.open('<?php echo $order->content_file; ?>')">
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="order-review-btn show-feeback-btn">
                                                    <span class="show-feeback <?php if($order->status == 'approved' ) { echo 'disabled'; } ?>" 
                                                    >Send Feedback</span>
                                                </div>
                                                <?php  if($order->downloaded_content == 1) { ?>
                                                <div class="order-review-btn approve-btn">
                                                     <?php  if($order->status == 'approved') { ?>
                                                        <span class="approve-selector approved-content">Approved!</span>   
                                                    <?php } else{ ?>
                                                        <span class="approve-selector approve-content" data-id="<?php echo $order->id; ?>" data-orderid="<?php echo $order->order_id; ?>" data-itemname="<?php echo $order->product_name; ?>" >Approve?</span>    
                                                    <?php } ?>
                                                </div>
                                                 <?php } ?>
                                                <div class="user-feedback-box">
                                                    <div class="user-feedback-box-inner">
                                                        <p>Feedback on the content we wrote. This is used to let us know if there is anything that needs to be corrected. Please be descriptive with your feedback, and once submitted, and once submitted we will review and reach out to you.</p>
                                                        <form action="#" method="post" enctype="multipart/form-data" id="user-feedback-form">
                                                            <textarea class="send-feeback-field" id="description" cols="50" rows="7" name="feedback" placeholder="Feedback. ( Max 500 word count )" onkeyup="wordcounts<?php echo $order->id; ?>(this.value);"></textarea>
                                                             <input type="hidden" name="words" id="word_count" size=4 readonly class="word-count" value="0">
                                                             <input type="hidden" name="item_row_id" id="item_row_id" value="<?php echo $order->id; ?>">
                                                             <input type="hidden" name="user_id" id="user_id" value="<?php echo $order->user_id; ?>">
                                                             <input type="hidden" name="order_id" id="order_id" value="<?php echo $order->order_id; ?>">
                                                            <input type="hidden" name="product_name" id="product_name" value="<?php echo $order->product_name; ?>">
                                                            <div class="upload-feedback-input">
															<div class="upload-content-btn">
                                                                <input type="button" name="button" class="button send-feedback" value="SUBMIT FEEDBACK" id="send-feedback">
                                                                <div class="feedback-msg"></div>
                                                            </div>
                                                            <div class="filebutton button">
                                                               Upload Feedback <input type="file" id="feedback_file" name="feedback_file">
                                                            </div>
                                                            <div class="uploaded-file button"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> 
                                            <script type="text/javascript">
                                              function wordcounts<?php echo $order->id; ?>(textarea)
                                              {
                                                var chars=textarea.length,
                                                words=textarea.match(/\w+/g).length;
                                                jQuery('.order-<?php echo $order->id; ?> #word_count').val(words);
                                                var counts = jQuery('.order-<?php echo $order->id; ?> #word_count').val();
                                                if(counts > 500)
                                                {
                                                jQuery('.order-<?php echo $order->id; ?> #description').css('border', '1px solid #ff0000');
                                                }
                                                else
                                                {    
                                                 jQuery('.order-<?php echo $order->id; ?> #description').css('border', '1px solid #008000');
                                                }
                                              }
                                              jQuery(document).ready(function () {  
                                                jQuery('.order-<?php echo $order->id; ?> .show-feeback').click(function () {
                                                    jQuery('.order-<?php echo $order->id; ?> .user-feedback-box').slideToggle( "slow" );
                                                });
                                                
                                            });    
                                            </script>
                                           <?php
                                        }
                                    }
                                    else
                                    { echo "<div class='order-data-blank'><p>There is no orders found</p></div>"; }
                                    ?>
                                </div>
                            </div>
                            <?php if(sizeof($total_order_data) > $per_page){ ?>
                                <div class="card-footer">
                                   <div id="order-loadmore" class="loading-banner" data-total="<?php echo sizeof($total_order_data); ?>"><a class="btn" href="javascript:;">Load more Orders...</a></div>
                                </div> 
                            <?php } ?>
                        </div>
                    </div>
                </div>        
                
                
            <!-- /#add-category -->
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
<script type="text/javascript">

jQuery(document).ready(function () {
    jQuery('.user-feedback-box #description').val('');
    jQuery('.user-feedback-box #word_count').val('');
    jQuery('.user-feedback-box #feedback_file').val('');
     jQuery('.user-feedback-box  #feedback_file').change(function(e) {          
     var fileName = e.target.files[0].name;
     jQuery(this).parent().next().html(fileName);
    });
    jQuery(document).on('click', '.send-feedback', function (e) {
        jQuery('.wc-loader').fadeIn();   
        var selector = jQuery(this).closest('.user-feedback-box');
        //selector.find(".feedback-msg .error").remove();
        selector.find(".feedback-msg .success").remove();
        selector.find(".error").remove();   
        var feedback = selector.find('#description').val();
        var item_row_id = selector.find('#item_row_id').val();
        var user_id = selector.find('#user_id').val();
        var order_id = selector.find('#order_id').val();
        var product_name = selector.find('#product_name').val();
        var word_count = selector.find('#word_count').val();
        var fileval = selector.find('#feedback_file').val();
        var file_data = selector.find('#feedback_file').prop('files')[0];
        if((feedback == "") && (fileval == ""))
        {       
             jQuery('.wc-loader').fadeOut();
             selector.find('.feedback-msg').append('<p class="error">Please Enter Feedback or Upload File.</p>');
             return false;  
        }
        if(word_count > 500)
        {       jQuery('.wc-loader').fadeOut();
             selector.find(".error").remove();   
            selector.find('#description').after('<p class="error">You can enter Feedback up to 500 words</p>'); 
             return false;  
        }
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('item_row_id', item_row_id);
        form_data.append('user_id', user_id);
        form_data.append('order_id', order_id);
        form_data.append('product_name', product_name);
        form_data.append('feedback', feedback);
        form_data.append('action', 'user_feedback_data');

        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'post',
            contentType: false,
            processData: false,
            data: form_data,
            success:function(response) {
                if(response == 'success')
                {
                    selector.find('.feedback-msg').append("<p class='success'>Feedback Sent Successfully</p>");
                    selector.find("#user-feedback-form")[0].reset();
                    jQuery('.wc-loader').fadeOut();
                }
                else
                {
                     selector.find('.feedback-msg').append(response);
                     jQuery('.wc-loader').fadeOut();
                }
            }  
        });
    });
    jQuery(document).on('click', '.download-content', function (e) {
        jQuery('.wc-loader').fadeIn();
        jQuery(this).next().click();
       //var selector = jQuery(this).closest('.download-content-btn');
        //alert(selector.html());
       // selector.find('#download-content-click').click();
    //alert(jQuery(this).data("id") );
    var item_row_id = jQuery(this).data("id");
    if(item_row_id != ''){

        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=user_view_content_entry&item_row_id=" +item_row_id,
           success:function(data)
           {   
            //window.open('http://180.211.98.106:8083/Wildcoffeemarketing/wp-content/uploads/2019/05/demo-doc-2-5.docx');
                if(data)
                {   //selector.find('#download-content-click').click();
                    setTimeout(function(){
                       location.reload(); 
                    }, 1000); 
                   jQuery('.wc-loader').fadeOut();
                 }
                
               
            }
        });
    }   
    }); 
    jQuery(document).on('click', '.approve-content', function (e) {
        jQuery('.wc-loader').fadeIn();
        var selector = jQuery(this).closest('.approve-btn');
        var item_row_id = jQuery(this).data("id");
        var order_id = jQuery(this).data("orderid");
        var itemname = jQuery(this).data("itemname");
        if(item_row_id != '')
        {
            jQuery.ajax({
               type:"post",
               url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
               data:"action=user_approved_content&item_row_id=" +item_row_id + "&order_id="+order_id + "&itemname="+itemname,
               success:function(data)
               {   
                    if(data)
                    {   
                        selector.find('.approve-selector').removeClass("approve-content");
                        selector.find('.approve-selector').addClass("approved-content");
                        selector.find('.approve-selector').text("Approved!");
                        selector.find('.approve-selector').off("click");
                        selector.prev().find('.show-feeback').addClass("disabled");
                        jQuery('.wc-loader').fadeOut();

                     }
                    
                   
                }
            });
        }   
    }); 
    let pull_item = 1; let item_jsonFlag = true; let pagecount = <?php echo $per_page; ?>;
    jQuery("#order-loadmore").click(function(){
      
    if(item_jsonFlag)
    {  
        jQuery('.wc-loader').fadeIn();
        var odate = jQuery("#order-date").val();
        var oname = jQuery("#order-name").val();
        pull_item++; item_jsonFlag = false; 
        pagecount += <?php echo $per_page; ?>;
        var totalpage =  jQuery(this).data('total');
        //console.log(pagecount);
        //console.log(totalpage);
        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=user_all_order_lists&page=" +pull_item + "&order-date=" +odate + "&order-name=" +oname,
           success:function(data)
           {
                if(data)
                {
                   jQuery("#review-order-list").append(data);
                    item_jsonFlag = true;
                    jQuery('.wc-loader').fadeOut();
                 }
                else{
                    jQuery('#order-loadmore').hide(); 
                    jQuery('.wc-loader').fadeOut();
                }
                if ( pagecount >= totalpage ) {
                    jQuery('#order-loadmore').hide();
                } 
            }
        });
    }
    });

});
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>

  
<?php get_footer('user'); ?>