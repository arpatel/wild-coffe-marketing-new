<?php 

/* Template name: Uesr Purchase Content */

get_header('user'); 
?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
<div class="content"><div class="wc-loader"></div>
<div class="row">
<div class="col-lg-12">
<div class="purchase-contnet">
    <div class="product-table-section">
        <div class="purchase-step-title">
            <div class="step-num"><span class="badge badge-info">1</span></div>
            <div class="col-lg-5 col-sm-12"><h3>Order New Or Additional Content</h3></div>
        </div>
        <?php echo do_shortcode('[product_table]'); ?>
    </div>    
<?php if(sizeof( WC()->cart->get_cart() ) > 0) { ?>
    <div class="cart-section">
        <div class="purchase-step-title">
            <div class="step-num"><span class="badge badge-info">2</span></div>
            <div class="col-lg-5 col-sm-12"><h3>Cart</h3><span class="step-text">As you add items from the above select will appear in your cart.</span></div>
        </div>
         <?php do_action( 'woocommerce_before_cart' ); ?>

        <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
            <?php do_action( 'woocommerce_before_cart_table' ); ?>

            <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                <thead>
                    <tr>
                        <th class="product-remove">&nbsp;</th>
                        <th class="product-name"><?php esc_html_e( 'Content', 'woocommerce' ); ?></th>
                        <th class="product-quantity"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
                        <th class="product-service"><?php esc_html_e( 'Monthly Recurring Service', 'woocommerce' ); ?></th>
                        <th class="product-price"><?php esc_html_e( 'Price Per Content', 'woocommerce' ); ?></th>
                        <th class="product-subtotal"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                    <?php
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                            ?>
                            <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                                <td class="product-remove">
                                    <?php
                                        // @codingStandardsIgnoreLine
                                        echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                            '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">X</a>',
                                            esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                            __( 'Remove this item', 'woocommerce' ),
                                            esc_attr( $product_id ),
                                            esc_attr( $_product->get_sku() )
                                        ), $cart_item_key );
                                    ?>
                                </td>

                              
                                <td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
                                <?php
                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<span class="prod-name">%s</a>',  $_product->get_name() ), $cart_item, $cart_item_key ) . '&nbsp;' );
                               

                                do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

                                // Meta data.
                            //    echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

                                // Backorder notification.
                                if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
                                }
                                ?>
                                </td>

                                 <td class="product-quantity cart-qty" data-qty="<?php echo $cart_item['quantity'];  ?>" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
                                <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {
                                    $product_quantity = woocommerce_quantity_input( array(
                                        'input_name'   => "cart[{$cart_item_key}][qty]",
                                        'input_value'  => $cart_item['quantity'],
                                        'max_value'    => $_product->get_max_purchase_quantity(),
                                        'min_value'    => '0',
                                        'product_name' => $_product->get_name(),
                                    ), $_product, false );
                                }

                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                                ?>
                                </td>
                                <td class="product-service" data-title="<?php esc_attr_e( 'Monthly Recurring Service', 'woocommerce' ); ?>">
                                <?php
                                echo wildcoffee_wc_get_formatted_cart_item_data( $cart_item );
                                ?>
                                </td>
                                <td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                    ?>
                                </td>

                               
                                <td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    <?php do_action( 'woocommerce_cart_contents' ); ?>

                    <tr class="cart-total-row">
                        <td colspan="3" class="actions cart-total-left">
                            <button type="submit" class="button btn" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
                            <?php do_action( 'woocommerce_cart_actions' ); ?>
                            <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
                        </td>
                        <td colspan="3" class="actions cart-total-rgt">
                          <span class="total-txt"><?php esc_attr_e( 'Total ', 'woocommerce' ); ?></span>
                          <?php wc_cart_totals_order_total_html(); ?>
                        </td>
                    </tr>
                    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
                </tbody>
            </table>
            <?php do_action( 'woocommerce_after_cart_table' ); ?>
        </form>
        <?php do_action( 'woocommerce_after_cart' ); ?>
    </div>

    <div class="payment-section">
         <?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>

        <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
            <div class="payment-blilling-section">
                <?php //if ( $checkout->get_checkout_fields() ) : ?>

                    <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                    <div class="col2-set" id="customer_details">
                        <div class="col-12">
                            <?php do_action( 'woocommerce_checkout_billing' ); ?>
                        </div>

                       
                    </div>

                    <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

                <?php //endif; ?>
            </div>
            <div class="payment-pay-section">
                <div class="purchase-step-title">
            <div class="step-num"><span class="badge badge-info">4</span></div>
            <div class="col-lg-5 col-sm-12"><h3>Payment Method</h3></div>
        </div>
                <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                <div id="order_review" class="woocommerce-checkout-review-order">
                    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                </div>

                <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
            </div>
        </form>

        <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
    </div>
    <?php } ?><!-- cart empty -->
</div>
</div>
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function () {
    jQuery('.step2-wrap').hide();
    var first_name = jQuery('#billing_first_name').val();
    var last_name = jQuery('#billing_last_name').val();
    var company = jQuery('#billing_company').val();
    var address = jQuery('#billing_address_1').val();
    var city = jQuery('#billing_city').val();
    var postcode = jQuery('#billing_postcode').val();
    var state = jQuery('#billing_state').val();
    jQuery('.step2-address').html('<span class="fname">' + first_name + ' ' + last_name + '</span><br><span class="bill_address">'+ company + '<br>' + address + '<br>' + city + ', ' + state +'<br>'+ postcode+'</span>');

   
    jQuery('.save-continue, #place_order').on('click', function () { 
        var first_name = jQuery('#billing_first_name').val();
        var last_name = jQuery('#billing_last_name').val();
        var company = jQuery('#billing_company').val();
        var address = jQuery('#billing_address_1').val();
        var city = jQuery('#billing_city').val();
        var postcode = jQuery('#billing_postcode').val();
        var state = jQuery('#billing_state').val();

        jQuery(".error").remove();           
        if((first_name == '')) {
            jQuery('#billing_first_name').after('<div class="error">Please Enter First Name</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_first_name').focus();
            return false;  
        }
        if((last_name == '')) {
            jQuery('#billing_last_name').after('<div class="error">Please Enter Last Name</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_last_name').focus();
            return false;  
        }
        if((address == '')) {
            jQuery('#billing_address_1').after('<div class="error">Please Enter Street Address</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_address_1').focus();
            return false;  
        }
        if((city == '')) {
            jQuery('#billing_city').after('<div class="error">Please Enter City</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_city').focus();
            return false;  
        }
        if((postcode == '')) {
            jQuery('#billing_postcode').after('<div class="error">Please Enter ZIP/Postal Code</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_postcode').focus();
            return false;  
        }
        if((state == '')) {
            jQuery('#billing_state').after('<div class="error">Please Enter State / County</div>'); 
            jQuery('.step2-wrap').fadeIn();
            jQuery('#billing_state').focus();
            return false;  
        }
        else {
            jQuery(this).hide();
            jQuery('.step1-wrap, .step2-wrap, .woocommerce-form-login-toggle').fadeOut();
            jQuery('.step2-address').html('<span class="fname">' + first_name + ' ' + last_name + '</span><br><span class="bill_address">'+ company + '<br>' + address + '<br>' + city + ', ' + state +'<br>'+ postcode+'</span>');
            jQuery('.step-change').show();
        }
    });

    jQuery('#step2-change').on('click', function () { 
        jQuery('.step2-wrap').fadeIn();
        jQuery('.step2-address').text('');
        jQuery('.save-continue').show();
        jQuery('.step-change').hide();
    });
    
    jQuery('.woocommerce-cart-form').trigger("reset");
    jQuery("[name='update_cart']").prop("disabled", true);
    jQuery('.cart-qty .quantity select').change(function() {
        var oldval = '';
        var val = '';
        jQuery('.cart-qty .quantity select').each(function() {
           // alert(jQuery(this).val());
          oldval += jQuery(this).parents('.cart-qty').data("qty"); 
          val += jQuery(this).val(); 
        
        });
        //alert(oldval +' '+ val);
        if(oldval != val)
        {
           jQuery("[name='update_cart']").prop("disabled", false);
        }else{
            jQuery("[name='update_cart']").prop("disabled", true);
        }
    });


    
});     
jQuery(document).ready(function($) {
    $('#product-row-1785 .variation_id, #product-row-1632 .variation_id').change( function(){
        $('[name=quantity]').val('1');
        var selector = jQuery(this).closest('.variations_form');
        var product_id = $(this).val();
        if(product_id != '')
        { 
            //jQuery('.wc-loader').fadeIn();  
            
            selector.append('<div class="blockUI blockOverlay" style="z-index: 1000; border: medium none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.6; cursor: wait; position: absolute;"></div>');
            selector.find('.subscription_price').remove();
            jQuery.ajax({
               type:"post",
               url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
               data:"action=subscription_product_price&product_id=" +product_id,
               success:function(data)
               {  
                    if(data)
                    {    
                        selector.find('.subscription_price').remove();
                        selector.append('<div class="subscription_price"><span>$' +  data + '</span></div>');
                        //jQuery('.wc-loader').fadeOut();
                         selector.find(".quantity").removeClass('wc_qty');
                        selector.find('.blockOverlay').remove();

                     }
                }
            });
        }else {
            selector.find('.subscription_price').remove();
            selector.find(".quantity").addClass('wc_qty');
            selector.find('.subscription_total_price').remove();
         }
        });
    $('.product-row [name=quantity]').val('1');
    $('#pa_word-count, #pa_reoccurring-service-for, #pa_social-media, #pa_number-of-posts, #pa_reoccurring-service-for').val('');
    $(".product-type-variable .quantity").addClass('wc_qty');
    $('.product-type-variable .variation_id').change( function(){
         var selector = jQuery(this).closest('.variations_form');
        selector.find('[name=quantity]').val('1');
        selector.find('.subscription_total_price').remove();
        var product_id = $(this).val();
           if(product_id != '')
        { 
            selector.find(".quantity").removeClass('wc_qty');
          } else {
              selector.find(".quantity").addClass('wc_qty');
          }
    });

     $(".product-type-variable-subscription .quantity").addClass('wc_qty');
    $('#product-row-1785 [name=quantity], #product-row-1632 [name=quantity]').change( function(){ 
        if (!(this.value < 1)) {  
            var selector = jQuery(this).closest('.variations_form');
            selector.find('.subscription_total_price').remove();
            var price = selector.find('.subscription_price span').text();
            price = price.replace('$','');
            selector.find('.subscription_price').hide();
            var product_total = parseFloat(price * this.value);
            selector.append('<div class="subscription_total_price"><span>$' +  product_total.toFixed(2) + '</span></div>');
        }else{
            $(".product-type-variable-subscription .quantity").removeClass('wc_qty');
        }
    });

    $('.product-type-variable [name=quantity], .product-type-simple [name=quantity]').change( function(){ 
        if (!(this.value < 1)) {  
            var selector = jQuery(this).closest('form.cart');
            selector.find('.woocommerce-variation.single_variation').hide();
            selector.find('.subscription_total_price').remove();
            var price = selector.find('.woocommerce-Price-amount').text();
            price = price.replace('$','');
            selector.find('.subscription_price').hide();
            var product_total = parseFloat(price * this.value);
            selector.append('<div class="subscription_total_price"><span>$' +  product_total.toFixed(2) + '</span></div>');
        }
    });  
});   
</script>
 
<?php get_footer('user'); ?>