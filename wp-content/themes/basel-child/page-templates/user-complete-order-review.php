<?php 

/* Template name: User Complete Order Review */


get_header('user');
$customer_user_id = get_current_user_id(); ?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
<div class="content user-completed-orders">
    <div class="animated fadeIn">
        <?php
            $customer_orders = wc_get_orders( array(
            'meta_key' => '_customer_user',
            'meta_value' => $customer_user_id,
            'post_status' => array('wc-completed'),
            'numberposts' => -1
        ) );
        if($customer_orders)
        {  ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header completed-orders-top">
                            <strong class="card-title">Completed Orders</strong>
                        </div>
                        <div class="table-stats order-table ov-h">
						<div class="table-responsive">
                            <table class="table ">
                                <tbody>    
                                    <?php foreach($customer_orders as $order )
                                    {
                                        $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id; 
                                        foreach($order->get_items() as $item_id => $item)
                                        { 
                                             $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
                                             $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
                                             $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false );
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php if(get_field( 'product_icon', $item['product_id'] )) { ?>
                                                    <img src="<?php the_field( 'product_icon', $item['product_id'] ); ?>" alt="">
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <div class="stat-icon dib flat-color-1">
                                                        <div class="order-content">
                                                            <h3><?php echo $item['name']; ?></h3>
                                                            <?php if($item_word){ ?>
                                                            <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                            <?php } ?>
                                                            <?php if($item_post_count){ ?>
                                                            <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="name_processing">  
                                                    <span class="name"><?php $status = $order->status;  ?>
                                                        <?php if($order->status == 'completed')
                                                        {
                                                            $completed = $order->date_modified;
                                                            foreach ($completed as $key => $value) {
                                                                if($key == 'date'){
                                                                    //echo $value;
                                                                    echo $status .': '. date('m/d/Y \A\t H:i A', strtotime($value));
                                                                }
                                                            }
                                                         ?>
                                                        <?php  } else { echo $status; } ?>     
                                                    </span>
                                                </td>
                                                <td style="width: 200px;">
                                                    <div class="progress mb-3" style="height: 8px">
                                                        <?php if($order->status == 'completed'){ ?> 
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                        <?php }
                                                        elseif($order->status == 'processing'){ ?>
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="20"></div>
                                                       <?php } ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
                                                </td>
                                            </tr>
                                                <?php
                                        }
                                    }  ?>
                                 </tbody>
                            </table>
							</div>
                        </div> <!-- /.table-stats -->
                        <div class="card-footer">
                        <strong class="card-title">Need to Order More or New Content?</strong>
                        <p><a href="<?php echo site_url() ?>/purchase-new-content/" class="button">Click Here</a></p>
                        </div>
                    </div>
                </div>
            </div><?php             
        } else{
            echo "<div class='order-data-blank'><p>There is no orders found</p></div>";
        }  ?>
        <div class="clearfix"></div>  
    </div>
</div>
<div class="clearfix"></div>
<?php get_footer('user'); ?>