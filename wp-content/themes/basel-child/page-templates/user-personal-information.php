<?php 

/* Template name: User Personal Information */

global $woocommerce;
get_header('user'); 
?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
<div class="content">
<div class="row Personal-information-main-col">
    <div class="col-lg-12">
        <div class="card">
            <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry-content">
                    <?php the_content(); ?>
                    <?php $notices = wc_get_notices(); wc_set_notices( $notices );
                        wc_print_notices();  wc_get_template( 'myaccount/form-edit-account.php', array( 'user' => get_user_by( 'id', get_current_user_id() ) ) ); ?>
                   </div>
                    <?php basel_entry_meta(); ?>
                </article>
            <?php endwhile; ?>
        </div>
    </div>
</div>
</div>
<?php get_footer('user'); ?>