<?php 

/* Template name: User Dashboard */


get_header('user');
global $wpdb;
$customer_user_id = get_current_user_id(); ?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
<div class="content">
    <div class="animated fadeIn">
        <!--<div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="Alert-box">
                <div class="sufee-alert alert with-close blue-bg alert-dismissible fade show">
                    <h4 class="text-white card-title mb-3">Dashboard Main Alert </h4>
                    <p class="card-text text-white">Curabitur imperdiet velit ex, sit amet iaculis nisi fringilla a. Sed vulputate tempus ante, 
					sed convallis nisl cursus vitae. Mauris sit amet iaculis nisl. Phasellus at ligula pharetra, ultricies felis in, 
					accumsan tellus.Curabitur imperdiet velit ex. </p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo site_url(); ?>/user-all-order-status/">
                            <div class="mx-auto d-block order-summary">
                                <h3 class="text-sm-center">Your Total Orders</h3>
                                <div class="count text-sm-center mt-3 mb-3"><?php
                                    $customer_orders = get_posts( array(
                                        'numberposts' => -1,
                                        'meta_key'    => '_customer_user',
                                        'meta_value'  => $customer_user_id,
                                        'post_type'   => wc_get_order_types(),
                                        'post_status' => array_keys( wc_get_order_statuses() ),  //'post_status' => array('wc-completed', 'wc-processing'),
                                    ) );
                                    echo count( $customer_orders ); ?>
                                </div>
                                <div class="location text-sm-center mb-3">View Orders</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <div class="mx-auto d-block order-summary">
                            <h3 class="text-sm-center">Orders left to process</h3>
                            <div class="count text-sm-center mt-3 mb-3"><?php
                                $customer_orders = get_posts( array(
                                    'numberposts' => -1,
                                    'meta_key'    => '_customer_user',
                                    'meta_value'  => $customer_user_id,
                                    'post_type'   => wc_get_order_types(),
                                    'post_status' => array('wc-processing'),
                                ) );
                                echo count( $customer_orders ); ?>
                            </div>
                            <div class="location text-sm-center mb-3">View Progress</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo site_url(); ?>/user-complete-order-review/">
                            <div class="mx-auto d-block order-summary">
                                <h3 class="text-sm-center">Order Completed</h3>
                                <div class="count text-sm-center mt-3 mb-3"><?php
                                    $customer_orders = get_posts( array(
                                        'numberposts' => -1,
                                        'meta_key'    => '_customer_user',
                                        'meta_value'  => $customer_user_id,
                                        'post_type'   => wc_get_order_types(),
                                        'post_status' => array('wc-completed'),
                                    ) );
                                    echo count( $customer_orders ); ?>
                                </div>
                                <div class="location text-sm-center mb-3">Review your new content</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
 

        <?php
            $customer_orders = wc_get_orders( array(
            'meta_key' => '_customer_user',
            'meta_value' => $customer_user_id,
            'post_status' => array('wc-processing'),
            'numberposts' => -1
        ) );
        if($customer_orders)
        {  ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="admin-card">
                        <div class="card-header admin-card-header">
                            <strong class="card-title">Current Order Progress,</strong>
                            <p>Checkout your order(s) and their progress.</p>
                        </div>
                        <div class="table-stats order-table">
						<div class="table-responsive">
                            <table class="table">
                                <tbody>    
                                    <?php foreach($customer_orders as $order )
                                    {
                                        $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id; 
                                        foreach($order->get_items() as $item_id => $item)
                                        {   
                                            $any_order_item_qty = 'No';
                                             $complete_date = '';
                                            for($j=1;$j<=$item['quantity'];$j++)
                                            {  
                                                $order_data = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}order_contents 
                                                WHERE order_id = '".$order_id."' AND product_name LIKE '".$item['name']."%' AND status in('sent','updated') order by completed_date DESC"); 
                                                if($order_data)
                                                {
                                                    $any_order_item_qty = 'Yes';
                                                    $complete_date = $order_data->completed_date;
                                                }
                                            } 
                                             $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
                                             $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
                                             $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false );
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php if(get_field( 'product_icon', $item['product_id'] )) { ?>
                                                    <img src="<?php the_field( 'product_icon', $item['product_id'] ); ?>" alt="">
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <div class="stat-icon dib flat-color-1">
                                                        <div class="order-content">
                                                            <h3><?php echo $item['name']; ?></h3>
                                                            <?php if($item_word){ ?>
                                                            <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                            <?php } ?>
                                                            <?php if($item_post_count){ ?>
                                                            <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>  
                                                    <span class="name">
                                                        <?php if($any_order_item_qty == 'Yes') {  
                                                                    echo 'Completed: '. date('m/d/Y \A\t g:i A', strtotime($complete_date));
                                                         ?>
                                                        <?php  } else { echo $status; } ?>     
                                                    </span>
                                                </td>
                                                <td style="width: 200px;">
                                                    <div class="progress mb-3" style="height: 8px">
                                                        <?php if($order->status == 'completed' || $any_order_item_qty == 'Yes'){ ?> 
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                        <?php }
                                                        elseif($order->status == 'processing'){ ?>
                                                            <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="20"></div>
                                                       <?php } ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php if($any_order_item_qty == 'Yes') {  ?>
                                                    <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
                                                <?php } ?>
                                                </td>
                                            </tr>
                                                <?php
                                               
                                        }
                                    }  ?>
                                 </tbody>
                            </table>
							</div>
                        </div> <!-- /.table-stats -->
                        <div class="card-footer">
                        <strong class="card-title">Need to Order More or New Content?</strong>
                        <p><a href="<?php echo site_url() ?>/purchase-new-content/" class="button">Click Here</a></p>
                        </div>
                    </div>
                </div>
            </div><?php             
        } else{ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="admin-card">
                    <div class="card-footer">
                        <strong class="card-title">Need to Order More or New Content?</strong>
                        <p><a href="<?php echo site_url() ?>/purchase-new-content/" class="button">Click Here</a></p>
                    </div>
                    </div>
                </div>
            </div>        
        <?php }  ?>
        <div class="clearfix"></div>  
    </div>
</div>
<div class="clearfix"></div>
<?php get_footer('user'); ?>