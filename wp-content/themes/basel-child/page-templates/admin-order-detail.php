<?php 

/* Template name: Admin Order Detail */


get_header('admin');
$per_page = 10;
global $wpdb, $paged;
?>
<?php get_sidebar('admin'); ?>
<?php get_header('admin-top'); ?>
<div class="content admin-order-detail">
    <div class="wc-loader"></div>
    <div class="animated fadeIn">
        <?php
        	$order_id = $_GET['id'];
        	$order = wc_get_order( $order_id );

            $all_items_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
                                        WHERE order_id = '".$order_id."' ORDER BY completed_date DESC"); 
           
            if($order->created_via == 'subscription'){$recurring_orders = "Yes";}else{$recurring_orders = "No";}
            $feedback_count = 0;
            $approved_count = 0;
            $waiting_count = 0;
            foreach ($all_items_data as $all_items) {
                if($all_items->status == 'feedback')
                {
                   $feedback_count++; 
                }
                if($all_items->status == 'approved')
                {
                   $approved_count++; 
                }
                if($all_items->status == 'sent' || $all_items->status == 'updated')
                {
                   $waiting_count++; 
                }
            }
        ?>
        
        <div class="row">
            <div class="col-lg-12 single-orders-num">
                <div class="card">
                    <div class="card-header"><h4>Order: #<?php echo $order_id; ?><span class="feedback-count"><?php if($feedback_count > 0) {echo ' - Feedback Needed'; }?></span></h4></div>
                </div>
            </div>
            
            <div class="col-lg-12 order-detail-top">
                <div class="order-summary-col col-lg-4 col-md-4 col-sm-4">
                    <p>Order Status</p>
                    <div class="order-summary-content">
						<div class="order-middle-list order-status-one">
						<ul class="order-summary-list">							
							<li>Need Feedback <?php if($feedback_count > 0) {echo '('.$feedback_count.') :' . '<span> Yes </span>';}else{echo '(0):  <span> No </span>';}?></li>
							<li>Approved<?php if($approved_count > 0) {echo '('.$approved_count.') :' . '<span> Yes </span>';}else{echo '(0): <span> No </span>';}?></li>
							<li>Waiting for Download<?php if($waiting_count > 0) {echo '('.$waiting_count.') : ' . '<span> Yes </span>';}else{echo '(0): <span> No </span>';}?> </li>
						</ul>
						</div>                        
                    </div>
                </div>
                
                <div class="order-summary-col col-lg-4 col-md-4 col-sm-4">
                    <p>General</p>
                    <div class="order-summary-content">
					<div class="order-middle-list">
					<ul class="order-summary-list">
							<li> Date Purchased: <?php $completed = $order->date_created;
                                                foreach ($completed as $key => $value) {
                                                    if($key == 'date'){
                                                        echo '<span>'.date('m/d/Y', strtotime($value)).'</span>';
                                                    } 
                                                } ?> </li>
						
                        <li>Time Purchased:<?php $completed = $order->date_created;
                                                foreach ($completed as $key => $value) {
                                                   // print_r($completed);
                                                    if($key == 'date'){
                                                        echo '<span>'.date('H:i A', strtotime($value)).'</span>';
                                                    } 
                                                } ?> </li>
						<li>Total Content: <span><?php echo $order->get_item_count(); ?></span> </li>
                        <li class="order-total-list">Total: <?php echo $order->get_formatted_order_total(); ?> </li>
                        <li>Recurring Service: <span><?php echo $recurring_orders; ?></span> </li>
						</ul>
                       </div>                        
                    </div>
                </div>               
                <div class="order-summary-col col-lg-4 col-md-4 col-sm-4">
                    <p>Customer Info</p>
                    <div class="order-summary-content">
					<div class="order-middle-list">
						<ul class="order-summary-list">							
						<li>Name:<span><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></span></li>
                        <?php if($order->get_billing_phone()) {?><li>Company:<span><?php echo $order->get_billing_company(); ?></span> </li><?php }?>
                        <li>Email: <span><?php echo $order->get_billing_email(); ?></span></li>
                       <?php if($order->get_billing_phone()) {?> <li>Phone: <span> <?php echo $order->get_billing_phone(); ?></span></li><?php }?>
                        <li>Address:<span> <?php echo $order->get_billing_address_1() .' '. $order->get_billing_address_2() ."<br>"; echo $order->get_billing_city().", ".$order->get_billing_state()." ".$order->get_billing_postcode(); ?></span></li>  
					</ul>
					</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row admin-order-detail-list">
                <div class="col-lg-12">
                    <div class="admin-order-detail-list-top">
                    <h4>All Orders</h4>
                </div>
                <div class="admin-order-list-wrapper">
                    <div id="review-order-list">					
						<div class="mobile-content">
                        <?php $i = 1;
                            foreach($order->get_items() as $item_id => $item)
                            {
                                for($j=1;$j<=$item['quantity'];$j++)
                                {  
                                    $post_number =  numToOrdinalWord($j);
                                    $product_name = $item['name'];
                                    $form_title = "form-title".$i;
                                     $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
                                     $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
                                     $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false );

                                    $order_items_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}order_contents 
                                        WHERE order_id = '".$order_id."' and product_id = '".$item['product_id']."' and item_number = '".$j."' ORDER BY completed_date DESC"); 
                                    $row_id = '';
                                    $user_feedback_text ='';
                                    $user_feedback_file = '';
                                    $downloaded_content = '0';
                                    $order_status = '';
                                    if($order_items_data)
                                    { 
                                        $submitted = 1;
                                        foreach ($order_items_data as $order_content) {
                                           // print_r($order_content);
                                            $row_id = $order_content->id;
                                            $user_feedback_text = $order_content->user_feedback;
                                            $user_feedback_file = $order_content->user_feedback_file;
                                            $submitted_date = date('m/d/y \a\t g:i A', strtotime($order_content->completed_date));
                                            $downloaded_content = $order_content->downloaded_content;
                                            $order_status = $order_content->status;
                                        }
                                    }else{
                                        $submitted = 0;
                                    }
                                    $customer_user_id = $order->get_user_id();
                                     $post_number =  numToOrdinalWord($j);
                                     $item_name = $post_number . ' '. $item['name'];
                                     $submited_form_data = $wpdb->get_results("SELECT 
                                     p.id as data_id
                                      FROM {$wpdb->prefix}cf7_vdata p
                                         INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                                         ON ( p.id = ve.data_id )
                                        INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve1
                                         ON ( p.id = ve1.data_id )
                                         INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve2
                                         ON ( p.id = ve2.data_id )
                                         INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve3
                                         ON ( p.id = ve3.data_id )
                                         INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve4
                                         ON ( p.id = ve4.data_id )
                                         
                                        
                                      WHERE
                                      ( ve.name = 'order_id' AND CAST(ve.value AS DECIMAL) = '{$order_id}' )
                                      AND ( ve1.name = 'user_id' AND CAST(ve1.value AS DECIMAL) = '{$customer_user_id}' )
                                      AND ( ve2.name = 'ac_form_title' AND ve2.value LIKE '{$item_name}' )
                                        AND ( ve3.name = 'product_id' AND ve3.value = '{$item['product_id']}' )
                                        AND ( ve4.name = 'submit_type' AND ve4.value = 'Save &amp; Continue' )  
                                       
                                        GROUP BY p.id 
                                        ORDER BY p.id
                                        DESC"); 
                                    
                                    ?>
                                    <div class="admin-order-detail-list" id="form-title<?php echo $i;?>"> 
                                        <div class="status-icon">
                                            <?php if($order_status == 'sent'){ ?>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-waiting-icon.png">
                                        <?php } ?>
                                        <?php if($order_status == 'feedback'){ ?>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-feedback-icon.png">
                                        <?php } ?>
                                        <?php if($order_status == 'approved'){ ?>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/order-approved.png">
                                        <?php } ?>
                                        <?php if($submited_form_data) { ?>
                                          <div class="view-form-data"><a href="javascript:;" title="View Form Data"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                        <?php } ?>
                                        </div>
                                        <div class="product-icon">
                                            <?php if(get_field( 'product_icon', $item['product_id'] )) { ?>
                                            <img src="<?php the_field( 'product_icon', $item['product_id'] ); ?>" alt="">
                                            <?php } ?>
                                        </div>
                                        <div class="order-title-area">
                                            <div class="stat-icon dib flat-color-1">
                                                <div class="order-content">
                                                        <h3><?php echo $item['name']; ?></h3>
                                                    <?php if($item_word){ ?>
                                                    <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                    <?php } ?>
                                                    <?php if($item_post_count){ ?>
                                                    <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="order-complete-date">  
                                            <?php if($submitted == 1){  ?>
                                            <span class="name">Content Submitted</span>
                                            <span class="name">
                                              
                                                    <span class="name">
                                                    <?php echo 'Completed: '. $submitted_date; ?>
                                                    </span>
                                               
                                            </span>
                                        <?php } else{ ?>
                                            <span class="name">Content Not Submit</span>
                                             <div class="send-new-content">Submit Content</div>
                                        <?php } ?>
                                        </div>
                                        <div class="order-action order-download">  
                                            <span class="name">Downloaded Content</span>
                                            <?php if($downloaded_content == 1){ ?>
                                            <span class="name">Yes</span>
                                        <?php }else{ ?>
                                            <span class="name">No</span>
                                        <?php } ?>
                                        </div>    
                                        <div class="order-action order-feedback">  
                                            <span class="name">Has Feedback</span>
                                            <?php if($user_feedback_text != '' || $user_feedback_file != ''){  ?>
                                            <span class="have-feedback">Yes, View</span>
                                            <?php }elseif(($user_feedback_text == '' || $user_feedback_file == '') && $order_status == 'approved'){ ?>
                                                <span class="name">No</span>
                                            <?php }else{ ?>
                                                <span class="name">N/A</span>
                                            <?php } ?>
                                        </div>
                                        <div class="order-action order-approve">  
                                            <span class="name">Approved/Dispproved</span>
                                            <?php if($order_status == 'feedback') { ?>
                                            <span class="name">Dispproved</span>
                                            <?php } elseif ($order_status == 'approved') { ?>
                                             <span class="name">Approved</span>
                                            <?php } else { ?>
                                            <span class="name">N/A</span>
                                        <?php } ?>
                                        </div>
                                        <?php if($submitted == 0){ ?>
                                        <div class="send-new-content-box" >
                                            <div class="send-new-content-box-inner">
                                                <form enctype="multipart/form-data" id="admin-send-content">
                                                    <div class="upload-content-input">
                                                        <div class="email-content-section">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                                                <div class="filebutton button">
                                                                    Upload Content <input type="file" id="newcontent" name="newcontent">
                                                                </div>
                                                                <div class="uploaded-file-txt"></div>
                                                                <div class="new-content-msg"></div>
                                                                </div>
                                                        
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                                                                    <div class="upload-content-btn">
                                                                        <input class="save-support button" name="save_support" type="button" value="Send Content">
                                                                    </div>
                                                                </div>
                                                        
                                                         
                                                        </div>
                                                     
                                                    </div>
                                                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $order->get_customer_id(); ?>">
                                                    <input type="hidden" name="product_id" id="product_id" value="<?php echo $item['product_id']; ?>">
                                                    <input type="hidden" name="item_number" id="item_number" value="<?php echo $j; ?>">
                                                    <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>">
                                                    <input type="hidden" name="product_name" id="product_name" value="<?php echo $product_name; ?>">
                                                    <input type="hidden" name="user_email" value="<?php echo $order->get_billing_email(); ?>"  id="user_email">
                                                 </form>
                                                 
                                                <script type="text/javascript">
                                                    jQuery(document).ready(function($) 
                                                    {   
                                                        $('#<?php echo $form_title;?> .send-new-content').click(function () {
                                                            $('#<?php echo $form_title;?> .send-new-content-box').slideToggle("slow");
                                                        });
                                                    });

                                                     </script>
                                             </div>
                                        </div>
                                    <?php } ?>
                                        <?php if($user_feedback_text != '' || $user_feedback_file != '')
                                        {  ?>
                                        <div class="send-updated-content-box" >
                                            <div class="send-updated-content-box-inner">
                                                <div class="feedback-view-box">
                                                    <?php if($user_feedback_text != ''){ ?>
                                                    <div class="feedback-view-box-top">
                                                        <div class="client-feedback-label">Client's Feedback</div>
                                                        <div class="client-feedback-field"><textarea><?php echo stripslashes($user_feedback_text); ?></textarea></div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($user_feedback_file != ''){ ?>
                                                    <div class="feedback-view-box-btm">
                                                        <div class="download-feedback-file"><a href="<?php echo $user_feedback_file; ?>" class="button" target="_blank">Download PDF Feedback</a></div>
                                                    </div>
                                                <?php } ?>
                                                </div>
                                                <form enctype="multipart/form-data" id="admin-send-content">
                                                    <div class="upload-content-input">
                                                        <label>Updated Content</label>
                                                        <div class="content-file-input">
                                                        <div class="filebutton button">
                                                           Updated Content <input type="file" id="updated_content" name="newcontent">
                                                        </div>
                                                        
                                                    </div><div class="updated-file-text"></div>
                                                    </div>
                                                    <input type="hidden" name="item_row_id" id="item_row_id" value="<?php echo $row_id; ?>">
                                                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $order->get_customer_id(); ?>">
                                                    <input type="hidden" name="product_id" id="product_id" value="<?php echo $item['product_id']; ?>">
                                                    <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>">
                                                    <input type="hidden" name="product_name" id="product_name" value="<?php echo $product_name; ?>">
                                                    <input type="hidden" name="user_email" value="<?php echo $order->get_billing_email(); ?>"  id="user_email">

                                                    <div class="upload-content-btn">
                                                        <input class="send-updated-content-btn button" name="save_support" type="button" value="Complete Revision">
                                                        <div class="new-content-msg"></div>
                                                    </div>
                                                 </form>
                                                 
                                                <script type="text/javascript">
                                                    jQuery(document).ready(function($) 
                                                    {   
                                                        $('#<?php echo $form_title;?> .have-feedback').click(function () {
                                                            $('#<?php echo $form_title;?> .send-updated-content-box').slideToggle("slow");
                                                        });
                                                    });

                                                     </script>
                                             </div>
                                        </div>
                                        <?php } ?>
                                        <?php  //print_r($submited_form_data);  ?>
                                        <div class="user-filled-data">
                                          <?php if($submited_form_data)
                                          {
                                            $form_fields = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cf7_vdata_entry WHERE data_id = ".$submited_form_data[0]->data_id.""); 
                                            echo $wpdb->last_error;
                                            echo  $wpdb->print_error();
                                            
                                            foreach ($form_fields as $field) 
                                            {
                                              if($field->name != '_wpcf7cf_hidden_group_fields' && $field->name != '_wpcf7cf_hidden_groups' && $field->name != '_wpcf7cf_visible_groups' && $field->name != '_wpcf7cf_options' && $field->name != 'g-recaptcha-response'&& $field->name != 'user_id' && $field->name != 'product_id' && $field->name != 'submit_type' && $field->name != 'ac_form_title' && $field->name != 'order_id' && $field->name != 'recurring_form' && $field->name != 'user-email' && $field->name != 'submit_time' && $field->name != 'submit_ip' && $field->name != '_wpcf7cf_repeaters' && $field->name != '_wpcf7cf_steps' && $field->value != '') 
                                              { ?><div class="user-filled-data-row"><?php
                                                  echo '<div class="user-filled-data-lft">' .str_replace('-', ' ', $field->name) .'</div><div class="user-filled-data-rgt"> '. $field->value .'</div>';
                                                  ?></div><?php
                                              }
                                            }
                                          }  ?>
                                        </div>
                                          <script type="text/javascript">
                                            jQuery(document).ready(function($) 
                                            {   
                                                $('#<?php echo $form_title;?> .view-form-data a').click(function () {
                                                    $('#<?php echo $form_title;?> .user-filled-data').slideToggle("slow");
                                                });
                                            });
                                          </script>
                                    </div>
                                        <?php
                                 $i++; 
                                }
                            }
                       ?>
                    </div> <!-- /.table-stats -->
                 </div>
                </div>
                </div>
            </div>
   <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
<?php
function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirthy','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}
 ?>
<script type="text/javascript">
jQuery(document).ready(function () {
    jQuery('.send-new-content-box #newcontent').val('');

    jQuery('#newcontent').change(function(e) {
     var fileName = e.target.files[0].name;
     jQuery(this).parent().next().html('<span>'+fileName+'</span>');
    });
    jQuery(document).on('click', '.save-support', function (e) {
        jQuery('.wc-loader').fadeIn();   
        var selector = jQuery(this).closest('.send-new-content-box');
        selector.find(".new-content-msg .error").remove();
        selector.find(".new-content-msg .success").remove();
        var fileval = selector.find('#newcontent').val();
        var file_data = selector.find('#newcontent').prop('files')[0];
        var user_id = selector.find('#user_id').val();
        var product_id = selector.find('#product_id').val();
        var item_number = selector.find('#item_number').val();
        var order_id = selector.find('#order_id').val();
        var product_name = selector.find('#product_name').val();
        var user_email = selector.find('#user_email').val();
        var form_data = new FormData();
        if (fileval == '') {
            jQuery('.wc-loader').fadeOut();
            selector.find('.new-content-msg').append('<p class="error">Please Upload File.</p>');
            return false;
        } else {
            selector.find(".new-content-msg .error").remove();
        }

        form_data.append('file', file_data);
        form_data.append('order_id', order_id);
        form_data.append('product_id', product_id);
        form_data.append('user_id', user_id);
        form_data.append('item_number', item_number);
        form_data.append('product_name', product_name);
        form_data.append('user_email', user_email);
        form_data.append('action', 'admin_new_content_upload');

        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'post',
            contentType: false,
            processData: false,
            data: form_data,
            success:function(response) {
                if(response == 'success')
                {
                    selector.find('.new-content-msg').append("<p class='success'>Content submitted Successfully</p>");
                    selector.find("#admin-send-content")[0].reset();
                    jQuery('.wc-loader').fadeOut();
                    setTimeout(function(){
                       location.reload(); 
                    }, 3000); 
                }
                else
                {
                     selector.find('.new-content-msg').append(response);
                     jQuery('.wc-loader').fadeOut();
                }
            }  
            

        });
    });
     jQuery('.send-updated-content-box #updated_content').val('');
    jQuery('#updated_content').change(function(e) {
     var fileName = e.target.files[0].name;
     jQuery(this).parents('.content-file-input').next().html('<span>'+fileName+'</span>');
    });
    jQuery(document).on('click', '.send-updated-content-btn', function (e) {
        jQuery('.wc-loader').fadeIn();   
        var selector = jQuery(this).closest('.send-updated-content-box');
        selector.find(".new-content-msg .error").remove();
        selector.find(".new-content-msg .success").remove();
        var fileval = selector.find('#updated_content').val();
        var file_data = selector.find('#updated_content').prop('files')[0];
        var item_row_id = selector.find('#item_row_id').val();
        var user_id = selector.find('#user_id').val();
        var product_id = selector.find('#product_id').val();
        var item_number = selector.find('#item_number').val();
        var order_id = selector.find('#order_id').val();
        var product_name = selector.find('#product_name').val();
        var user_email = selector.find('#user_email').val();

        var form_data = new FormData();
        if (fileval == '') {
            jQuery('.wc-loader').fadeOut();
            selector.find('.new-content-msg').append('<p class="error">Please Upload File.</p>');
            return false;
        } else {
            selector.find(".new-content-msg .error").remove();
        }

        form_data.append('file', file_data);
        form_data.append('item_row_id', item_row_id);
        form_data.append('order_id', order_id);
        form_data.append('product_id', product_id);
        form_data.append('user_id', user_id);
        form_data.append('item_number', item_number);
        form_data.append('product_name', product_name);
        form_data.append('user_email', user_email);
        form_data.append('action', 'admin_updated_content_upload');

        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'post',
            contentType: false,
            processData: false,
            data: form_data,
            success:function(response) {
                if(response == 'success')
                {
                    selector.find('.new-content-msg').append("<p class='success'>Content submitted Successfully</p>");
                    selector.find("#admin-send-content")[0].reset();
                    jQuery('.wc-loader').fadeOut();
                    setTimeout(function(){
                       location.reload(); 
                    }, 3000); 
                }
                else
                {
                    selector.find('.new-content-msg').append(response);
                    jQuery('.wc-loader').fadeOut();
                }
            }  
        });
    });
    let pull_item = 1; let item_jsonFlag = true; let pagecount = <?php echo $per_page; ?>;
    jQuery("#order-item-loader").click(function(){
    if(item_jsonFlag)
    {  
        jQuery('.wc-loader').fadeIn();
        var odate = jQuery("#order-date").val();
        pull_item++; item_jsonFlag = false; 
        pagecount += <?php echo $per_page; ?>;
        var totalpage =  jQuery(this).data('total');
        console.log(pagecount);
        console.log(totalpage);
        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=all_order_lists&page=" +pull_item  + "&order-date=" +odate,
           success:function(data)
           {
                if(data)
                {
                   jQuery("#order-item-list").append(data);
                    item_jsonFlag = true;
                    jQuery('.wc-loader').fadeOut();
                 }
                else{
                    jQuery('#order-item-loader').hide(); 
                    jQuery('.wc-loader').fadeOut();
                }
                if ( pagecount >= totalpage ) {
                    jQuery('#order-item-loader').hide();
                } 
            }
        });
    }
    });
});
</script>
<?php get_footer('admin'); ?>