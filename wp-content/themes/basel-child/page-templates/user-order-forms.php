<?php 

/* Template name: User Order Forms */


get_header('user'); 
//require ABSPATH . 'wp-content/plugins/advanced-cf7-db/includes/vsz-cf7-db-function.php';
global $wpdb;
$customer_user_id = get_current_user_id();
$current_user = wp_get_current_user();
$user_email = $current_user->user_email;
?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>

<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <?php 
            $order_statuses = array('wc-processing');
            $customer_orders = wc_get_orders( array(
                'meta_key' => '_customer_user',
                'meta_value' => $customer_user_id,
                'post_status' => $order_statuses,
                'numberposts' => -1
            ) );
            $i=1;
            foreach($customer_orders as $order )
            {
                $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id;
                foreach($order->get_items() as $item_id => $item)
                {
                  $form_data_count = 0;
                  $form_shortcode =  get_field('form_shortcode', $item['product_id']);
                    for($j=1;$j<=$item['quantity'];$j++)
                    { 
                        $post_number =  numToOrdinalWord($j);
                       // echo $post_number . ' '; echo $item['name']; echo ' ' . $item['quantity'];  }
                     
                        $item_name = $post_number . ' '. $item['name'];
                        $form_str = explode(" ",$form_shortcode);
                        $form_id = preg_replace('/[id=""]/','',$form_str[1]);
                        
                  $submited_form_data = $wpdb->get_results("SELECT 
                         p.id as data_id
                          FROM {$wpdb->prefix}cf7_vdata p
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                             ON ( p.id = ve.data_id )
                            INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve1
                             ON ( p.id = ve1.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve2
                             ON ( p.id = ve2.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve3
                             ON ( p.id = ve3.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve4
                             ON ( p.id = ve4.data_id )
                          WHERE
                          ve.cf7_id = '".$form_id."'
                            AND ( ve.name = 'user_id' AND CAST(ve.value AS DECIMAL) = '".$customer_user_id."' )
                            AND ( ve1.name = 'order_id' AND CAST(ve1.value AS DECIMAL) = '".$order_id."' )
                            AND ( ve2.name = 'ac_form_title' AND ve2.value LIKE '".$item_name."' )
                            AND ( ve3.name = 'product_id' AND ve3.value = '".$item['product_id']."' )
                            AND ( ve4.name = 'submit_type' AND ve4.value = 'Save &amp; Continue' )  
                            GROUP BY p.id 
                            ORDER BY p.id
                            DESC"); 
                 
                              foreach ($submited_form_data as $data) {
                                //print_r($data->data_id);
                                $form_data_count++;
                              }
                       }
                     if($form_data_count != $item['quantity']) { 
                   // Get a specific meta data
                    $item_word = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_word-count') : wc_get_order_item_meta( $item_id, 'pa_word-count', false );
                    $item_post_count = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_number-of-posts') : wc_get_order_item_meta( $item_id, 'pa_number-of-posts', false );
                    $item_post = method_exists( $item, 'get_meta' ) ? $item->get_meta('pa_social-media') : wc_get_order_item_meta( $item_id, 'pa_social-media', false ); ?>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-five">
                                    <div class="stat-icon dib flat-color-1">
                                        <?php if(get_field( 'product_icon', $item['product_id'] )) { ?>
                                                <img src="<?php the_field( 'product_icon', $item['product_id'] ); ?>" alt="">
                                                <span class="post-count"><?php echo $item['quantity']; ?></span>
                                        <?php } ?>
                                        <div class="order-content">
                                            <h3><?php echo $item['name']; ?></h3>
                                            <?php if($item_word){ ?>
                                                <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                            <?php } ?>
                                            <?php if($item_post_count){ ?>
                                                <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="stat-content">
                                        <div class="text-left dib">
                                            <div class="stat-text"><h3>Quantity</h3></div>
                                            <div class="stat-heading"><?php echo $item['quantity']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 <?php
               }
                }
            }   ?>
        </div>
		  <?php if($customer_orders) {?><hr>
        <!-- /Widgets -->
          <?php dynamic_sidebar( 'user-order-form' ); ?>
      <?php }else{echo "<div class='order-data-blank'><p>There is no orders found or all form submitted</p></div>";} ?>
  
        <!-- Form Content-->
        <div class="accordion" id="accordionExample">
            <?php 
            $order_statuses = array('wc-processing');
            // Getting current customer orders
            $customer_orders = wc_get_orders( array(
                'meta_key' => '_customer_user',
                'meta_value' => $customer_user_id,
                'post_status' => $order_statuses,
                'numberposts' => -1
            ) );
            $i=1;
            // Loop through each customer WC_Order objects
            foreach($customer_orders as $order )
            { 
             
                // Order ID (added WooCommerce 3+ compatibility)
                $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id;
                // Iterating through current orders items
                foreach($order->get_items() as $item_id => $item)
                {   
                    $form_shortcode =  get_field('form_shortcode', $item['product_id']);
                    for($j=1;$j<=$item['quantity'];$j++)
                    { 
                        $post_number =  numToOrdinalWord($j);
                       // echo $post_number . ' '; echo $item['name']; echo ' ' . $item['quantity'];  }
                        if($i==1){
                        $expanded = 'true';
                        $showclass = 'show';
                        }else{
                                $expanded = 'false';    
                                $showclass = '';
                        }
                        $heading = 'heading'.$i;    
                        $collapse = 'collapse'.$i; 
                        $item_name = $post_number . ' '. $item['name'];
                        $form_str = explode(" ",$form_shortcode);
                        $form_id = preg_replace('/[id=""]/','',$form_str[1]);
                        
                         $submited_form_data = $wpdb->get_results("SELECT 
                         p.id as data_id
                          FROM {$wpdb->prefix}cf7_vdata p
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                             ON ( p.id = ve.data_id )
                            INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve1
                             ON ( p.id = ve1.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve2
                             ON ( p.id = ve2.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve3
                             ON ( p.id = ve3.data_id )
                             INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve4
                             ON ( p.id = ve4.data_id )
                          WHERE
                          ve.cf7_id = '".$form_id."'
                            AND ( ve.name = 'user_id' AND CAST(ve.value AS DECIMAL) = '".$customer_user_id."' )
                            AND ( ve1.name = 'order_id' AND CAST(ve1.value AS DECIMAL) = '".$order_id."' )
                            AND ( ve2.name = 'ac_form_title' AND ve2.value LIKE '".$item_name."' )
                            AND ( ve3.name = 'product_id' AND ve3.value = '".$item['product_id']."' )
                            AND ( ve4.name = 'submit_type' AND ve4.value = 'Save &amp; Continue' )  
                            GROUP BY p.id 
                            ORDER BY p.id
                            DESC"); 
                         if($submited_form_data){

                         }
                         else 
                         {
                          $saved_form_data = $wpdb->get_results("SELECT 
                           p.id as data_id
                            FROM {$wpdb->prefix}cf7_vdata p
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve
                               ON ( p.id = ve.data_id )
                              INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve1
                               ON ( p.id = ve1.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve2
                               ON ( p.id = ve2.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve3
                               ON ( p.id = ve3.data_id )
                               INNER JOIN {$wpdb->prefix}cf7_vdata_entry ve4
                               ON ( p.id = ve4.data_id )
                            WHERE
                            ve.cf7_id = '".$form_id."'
                              AND ( ve.name = 'user_id' AND CAST(ve.value AS DECIMAL) = '".$customer_user_id."' )
                              AND ( ve1.name = 'order_id' AND CAST(ve1.value AS DECIMAL) = '".$order_id."' )
                              AND ( ve2.name = 'ac_form_title' AND ve2.value LIKE '".$item_name."' )
                              AND ( ve3.name = 'product_id' AND ve3.value = '".$item['product_id']."' )
                              AND ( ve4.name = 'submit_type' AND ve4.value = 'Save &amp; Finish later' )  
                              GROUP BY p.id 
                              ORDER BY p.id
                              DESC"); 

                          echo $wpdb->last_error;
                          echo  $wpdb->print_error();
                          
                          if($saved_form_data)
                          {
                            $form_fields = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cf7_vdata_entry WHERE data_id = ".$saved_form_data[0]->data_id.""); 
							              echo $wpdb->last_error;
                            echo  $wpdb->print_error();
                            
              							foreach ($form_fields as $field) 
              							{
              								//echo $field->name .'- '. $field->value .'<br>';
                            }
                          } 
                       
                             ?>
                               <div class="card user-form-accordion-header">
                                  <div class="card-header" id="<?php echo $heading;?>">                                   
                                      <a class="card-link" data-toggle="collapse" href="#<?php echo $collapse;?>" aria-expanded="<?php echo $expanded;?>" aria-controls="<?php echo $collapse;?>">
                                        <?php echo $item_name;  ?>
                                        <span class="collapsed"><p><b>+</b></p></span>
                                        <span class="expanded"><p><b>x</b></p></span>
                                      </a>
                                  </div>

                                  <div id="<?php echo $collapse;?>" class="collapse <?php echo $showclass; ?>" aria-labelledby="<?php echo $heading;?>" data-parent="#accordionExample">
                                    <div class="order-status-form-inner">
                										<div class="row">
                										<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
                                     <div id="form-title<?php echo $i;?>">
                                      <?php $form_title = "form-title".$i; ?>
                                      <?php echo do_shortcode($form_shortcode);  ?>
                                      <?php if($order->created_via == 'subscription'){
                                        $recurring = 'yes';
                                      }else{ $recurring = "no"; } ?>
                                      <script type="text/javascript">
                                        jQuery(document).ready(function($) 
                                        {
                                            $('#<?php echo $form_title;?> .wpcf7-form #user_id').val('<?php echo $customer_user_id; ?>');
                                            $('#<?php echo $form_title;?> .wpcf7-form #order_id').val('<?php echo $order_id; ?>');
                                            $('#<?php echo $form_title;?> .wpcf7-form #product_id').val('<?php echo $item['product_id']; ?>');
                                            $('#<?php echo $form_title;?> .wpcf7-form #ac_form_title').val('<?php echo $item_name; ?>');
                                            $('#<?php echo $form_title;?> .wpcf7-form #recurring_form').val('<?php echo $recurring; ?>');
                                            
                                            $('#<?php echo $form_title; ?> .submit_click').on('click', function () {
                                                var type = $(this).val();                                               
                                                    $('#<?php echo $form_title;?> .wpcf7-form .submit_type').val(type);
                                            });

                                            var filled_rel = 0; 
                                            <?php if($saved_form_data) 
                                            { ?>
                                              $("#<?php echo $form_title;?> .order-form-fields.submit-fields #finish-later").hide();
                                            <?php  foreach ($form_fields as $field) 
                                            {
                                              if(($field->name == 'rel-link-1' || $field->name == 'rel-link-2' || $field->name == 'rel-link-3' || $field->name == 'rel-link-4' || $field->name == 'rel-link-5' || $field->name == 'rel-link-6' || $field->name == 'rel-link-7' || $field->name == 'rel-link-8' || $field->name == 'rel-link-9' || $field->name == 'rel-link-10') && $field->data_id == $saved_form_data[0]->data_id)
                                              { 
                                                if($field->value != '')
                                                { ?>
                                                  filled_rel++; 
                                                  jQuery("#<?php echo $form_title; ?> .rel-wrapper").append('<div class="order-form-fields"><input type="text" name="rel-link-'+filled_rel+'" value="<?php echo $field->value;  ?>"><a href="javascript:;" class="remove_field" data-removed='+filled_rel+'>X</a></div>'); 
                                                <?php  }   
                                              } 

                                              if($field->name == 'home-tone-content' || $field->name == 'press-photo-available' || $field->name == 'press-video-available' || $field->name == 'press-opening' || $field->name == 'press-companies-entities' || $field->name == 'social-customers' || $field->name == 'social-tone-voice' || $field->name == 'email-tone')
                                              { ?>
                                                jQuery('#<?php echo $form_title; ?> .order-form-fields.radio-btn input:radio[name="<?php echo $field->name; ?>"]').filter('[value="<?php echo $field->value; ?>"]').attr('checked', true);
                                              <?php }
                                              elseif ( $field->name == 'order-file' || $field->name == 'press-document') { ?>
                                                  jQuery('#<?php echo $form_title; ?> #<?php echo $field->name; ?>').val("<?php echo $field->value; ?>");
                                              <?php }
                                              elseif ( $field->name == 'email-html') {  
                                                 $email_val = addslashes($field->value); 
                                                 $email_val = str_replace(array("\r","\n"),"",$email_val); ?>
                                                jQuery('#<?php echo $form_title; ?> #<?php echo $field->name; ?>').val('<?php echo html_entity_decode($email_val); ?>');
                                              <?php }
                                              elseif ( $field->name == 'blog-post-target-audience' || $field->name == 'press-thoughts' || $field->name == 'email-communicate' || $field->name == 'ebook-purpose' || $field->name == 'ebook-quotes') 
                                              {  
                                                $textarea_val = addslashes($field->value); ?>
                                                jQuery('#<?php echo $form_title; ?> #<?php echo $field->name; ?>').val('<?php echo html_entity_decode($textarea_val); ?>');
                                              <?php }
                                              elseif ( $field->name == '_wpcf7cf_hidden_group_fields' || $field->name == '_wpcf7cf_hidden_groups' || $field->name == '_wpcf7cf_visible_groups' || $field->name == '_wpcf7cf_options' ) { ?>
                                                jQuery('#<?php echo $form_title; ?> input[name="<?php echo $field->name; ?>"]').val('<?php echo html_entity_decode(stripslashes($field->value)); ?>');
                                              <?php }
                                              else { ?>
                                                jQuery('#<?php echo $form_title; ?> input[name="<?php echo $field->name; ?>"]').val("<?php echo $field->value; ?>");
                                            <?php } ?>
                                          <?php } ?>
                                        <?php }  ?> 
                                              
                                            var max_fields = 10;
                                            var rm = [];
                                            if(filled_rel == 0){
                                              var x = 1; 
                                            }else{
                                              var x = filled_rel + 1; 
                                            }
                                            jQuery("#<?php echo $form_title;?> #blog-more-link").click(function(){ 
                                              //alert(rm.length);
                                                if(x <= max_fields)
                                                { 
                                                  if(rm.length != 0)
                                                  {
                                                     $("#<?php echo $form_title; ?> .rel-wrapper").append('<div class="order-form-fields"><input type="text" name="rel-link-'+ rm[0] +'"><a href="javascript:;" class="remove_field" data-removed='+rm[0]+'>X</a></div>');   
                                                     rm.splice(rm.indexOf(rm[0]), 1);

                                                  }
                                                  else
                                                  {
                                                  //jQuery('#<?php echo $form_title;?> .more-rel-links.link-' + x).show(); 
                                                    $("#<?php echo $form_title; ?> .rel-wrapper").append('<div class="order-form-fields"><input type="text" name="rel-link-'+ x +'"><a href="javascript:;" class="remove_field" data-removed='+x+'>X</a></div>');
                                                  }
                                                    x++; 
                                                }
                                              });
                                              $("#<?php echo $form_title;?> .order-form-fields").on("click",".remove_field", function(e){ 
                                                e.preventDefault(); $(this).parent('div').remove(); x--;
                                                rm.push($(this).data("removed"));
                                                //alert(rm);
                                            })
                                        });
                                      
                                      </script>
                                    </div>
                									</div>
                									</div>
                                  </div>
                                  </div>
                              </div>
                              <?php   $i++; 
                         }  
                    } 
                }
            } 
             ?>   
        
       </div>   
        <!-- Form Content-->
        <!--  /Traffic -->
        <div class="clearfix"></div>
        <!-- To Do and Live Chat -->
    <!-- .animated -->
</div>
<!-- /.content -->
      
<?php 

function numToOrdinalWord($num)
{
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirthy','Forty','Fifty');

    if($num <= 20)
        return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
}
 ?>
 <script type="text/javascript">
  jQuery(document).ready(function($) {
    setTimeout(function(){
$(".order-form-fields.submit-fields").find('#finish-later').next().remove();
}, 3000);
});
document.addEventListener( 'wpcf7mailsent', function( event ) {
    setTimeout(function(){
     location.reload(); 
  }, 2000);
  //   jQuery(".wpcf7-form.sent").parents('.card').hide();
   // jQuery(".wpcf7-form.sent").parents('.card').next().find('.card-header .card-link').click();
}, false );
</script>
<?php /*<script type="text/javascript">
jQuery(document).ready(function($) {
  var otherval = $(".home-page-content .home-tone-content input[name='home-tone-content']:checked").val();

    if(otherval == 'other'){
        $('.order-form-fields.other-content').fadeIn();
      }else{
      $('.order-form-fields.other-content').hide();
    }
  $(".home-page-content .home-tone-content input[name='home-tone-content']").click(function(){
    if($(this).val() == "other"){
        $('.order-form-fields.other-content').fadeIn();
    }else{
      $('.order-form-fields.other-content').hide();
    }
  });
});
</script> */?>
<?php  get_footer('user'); ?>
 