<?php 

/* Template name: Admin Dashboard */


get_header('admin');
$per_page = 10;
global $wpdb, $paged;
?>
<?php get_sidebar('admin'); ?>
<?php get_header('admin-top'); ?>
<div class="content admin-dashboard admin-order-lists-wrapper">
    <div class="wc-loader"></div>
    <div class="animated fadeIn">
     
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo site_url(); ?>/admin-new-orders-status/">
                        <div class="mx-auto d-block order-summary">
                            <h3 class="text-sm-center">Total New Orders</h3>
                            <div class="count text-sm-center mt-3 mb-3"><?php do_action('get_total_orders_counts', $status = 'processing' ); ?>
                            </div>
                            <div class="location text-sm-center mb-3">View New Orders</div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo site_url(); ?>/admin-need-content/">
                            <div class="mx-auto d-block order-summary">
                                <h3 class="text-sm-center">Orders that Need Content</h3>
                                <div class="count text-sm-center mt-3 mb-3"><?php do_action('get_total_orders_counts', $status = 'need-content' ); ?></div>
                                <div class="location text-sm-center mb-3">View uncompleted Progress</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 order-summary-col">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo site_url(); ?>/admin-history-completed-orders/">
                             <div class="mx-auto d-block order-summary">
                                <h3 class="text-sm-center">Orders Completed & Delivered</h3>
                                <div class="count text-sm-center mt-3 mb-3"><?php do_action('get_total_orders_counts', $status = 'completed' ); ?>
                                </div>
                                <div class="location text-sm-center mb-3">Review Completed content</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row order-middle-area">
            <div class="col-lg-12 total-orders-counts">
                <div class="card">
                    <div class="card-header"><h4>Orders - Total: <span><?php do_action('get_total_orders_counts', $status = 'all' ); ?></span></h4></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-8 col-md-7 col-sm-7 order-filters">
                    <ul>
                        <?php
                        ob_start();
                          do_action('get_total_orders_counts', $status = 'cancelled' );
                            $cancelled_orders_count = ob_get_contents();
                        ob_end_clean();
                         ?>
                        <li class="active"><a href="<?php echo site_url(); ?>/admin-dashboard/">All Orders (<?php do_action('get_total_orders_counts', $status = 'all' ); ?>)</a></li>
                        <li><a href="<?php echo site_url(); ?>/admin-new-orders-status/">New Orders (<?php do_action('get_total_orders_counts', $status = 'processing' ); ?>)</a></li>
                        <li><a href="<?php echo site_url(); ?>/admin-need-content/">Need Content (<?php do_action('get_total_orders_counts', $status = 'need-content' ); ?>)</a></li>
                        <?php if($cancelled_orders_count > 0) { ?>
                            <li><a href="<?php echo site_url(); ?>/admin-cancelled-orders/">Cancelled (<?php do_action('get_total_orders_counts', $status = 'cancelled' ); ?>)</a></li>
                        <?php } ?>
                        <li><a href="<?php echo site_url(); ?>/admin-history-completed-orders/">All Completed & Delivered (<?php do_action('get_total_orders_counts', $status = 'completed' ); ?>)</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-5 admin-order-search">
                    <form method="post" action="" name="order-search">
                        <input type="text" name="order-date" value="<?php if($_POST['order-date']) echo $_POST['order-date'] ?>" placeholder="Date" id="order-date" >
                        <input type="submit" name="submit" value="Filter">
                    </form>
                </div>
            </div>
        </div>
        
        <?php
            $odate = '';
             if($_POST['order-date']){
                $odate = date("Y-m-d", strtotime($_POST['order-date']));
            }
            $total_all_orders = wc_get_orders( array(
            'post_status' => array_keys( wc_get_order_statuses() ),
            'date_created' => $odate,
            'numberposts' => -1,
            ) );

            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $all_orders = wc_get_orders( array(
            'post_status' => array_keys( wc_get_order_statuses() ),
            'numberposts' => 10,
            'date_created' => $odate,
            'paged' => $paged
        ) ); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card1">
                    <div class="table-stats admin-order-table ov-h">
                        <?php if($all_orders)
                        {  ?>
					<div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th class="order-id">Order</th>
                                    <th class="order-quantity">Total Quantity</th>
                                    <th class="order-total">Order Total</th>
                                    <th class="order-services">Total Services</th>
                                    <th class="order-status">Status</th>
                                    <th class="order-customer">Customer</th>
                                    <th class="order-date">Date</th>
                                    <th class="order-action">Action</th>
                                </thead>
                                <tbody id="order-item-list">
                                    <?php 
                                    foreach($all_orders as $order )
                                    { ?>
                                        <tr>
                                            <td class="order-id"><?php echo 'Order ' . $order->get_id(); ?></td>
                                            <td class="order-quantity"><?php echo $order->get_item_count(); ?></td>
                                            <td class="order-total"><?php echo $order->get_formatted_order_total(); ?></td>
                                            <td class="order-services"><?php echo sizeof($order->get_items()); ?></td>
                                            <td class="order-status">
                                                <?php if($order->get_status() == 'processing'){ ?>
                                                    <span class="new-order">New Order</span>
                                                <?php } 
                                                elseif($order->get_status() == 'completed'){ ?>
                                                    <span class="completed-order"><?php echo $order->get_status(); ?></span>
                                                <?php } 
                                                elseif($order->get_status() == 'cancelled'){ ?>
                                                    <span class="cancelled-order"><?php echo $order->get_status(); ?></span>
                                                <?php } 
                                                else { ?>
                                                    <span class="other-order"><?php echo $order->get_status(); ?></span>
                                                <?php } ?>
                                            </td>
                                            <td class="order-customer"><?php echo $order->get_billing_first_name(). ' ' .$order->get_billing_last_name(); ?></td>
                                            <td class="order-date">
                                                <?php $completed = $order->date_created;
                                                foreach ($completed as $key => $value) {
                                                    if($key == 'date'){
                                                        echo date('m/d/Y', strtotime($value));
                                                    } 
                                                } ?>
                                            </td>
                                            <td class="order-action">
                                                <div class="order-action-inner">
                                                    <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="view-order"><i class="fa fa-eye" aria-hidden="true"></i></a></span>
                                                    <span><a href="<?php echo site_url() ?>/admin-order-detail/?id=<?php echo $order->get_id(); ?>" class="edit-order"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> 
                                               </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
							</div>
                        <?php 
                        } 
                        else
                        { echo "<div class='order-data-blank'><p>There is no orders found</p></div>"; }  ?>
                    </div>
                    <?php
                     if( count($total_all_orders) > $per_page){ ?>
                        <div class="admin-order-loader">
                           <div id="order-item-loader" class="loading-banner" data-total="<?php echo count($total_all_orders); ?>"><a class="btn" href="javascript:;">Load more Orders...</a></div>
                        </div> 
                    <?php } ?>
                </div>
            </div>
        </div>
    <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
jQuery(document).ready(function () {
    let pull_item = 1; let item_jsonFlag = true; let pagecount = <?php echo $per_page; ?>;
    jQuery("#order-item-loader").click(function(){
    if(item_jsonFlag)
    {  
        jQuery('.wc-loader').fadeIn();
        var odate = jQuery("#order-date").val();
        pull_item++; item_jsonFlag = false; 
        pagecount += <?php echo $per_page; ?>;
        var totalpage =  jQuery(this).data('total');
        console.log(pagecount);
        console.log(totalpage);
        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=all_order_lists&page=" +pull_item  + "&order-date=" +odate,
           success:function(data)
           {
                if(data)
                {
                   jQuery("#order-item-list").append(data);
                    item_jsonFlag = true;
                    jQuery('.wc-loader').fadeOut();
                 }
                else{
                    jQuery('#order-item-loader').hide(); 
                    jQuery('.wc-loader').fadeOut();
                }
                if ( pagecount >= totalpage ) {
                    jQuery('#order-item-loader').hide();
                } 
            }
        });
    }
    });
});
</script>
<?php get_footer('admin'); ?>