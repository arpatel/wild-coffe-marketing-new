<?php 

/* Template name: User All Order status*/


get_header('user');
$per_page = 4;
global $wpdb, $paged;
?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
 <?php  $customer_user_id = get_current_user_id();  ?> 
       <!-- Content -->
        <div class="content"> <div class="wc-loader"></div>
            <!-- Animated -->
            <div class="animated fadeIn user-all-order-page">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Current Order Progress,</strong>
                                <p>Checkout your order(s) and their progress.</p>
                            </div>
                            <div class="table-stats order-table ov-h">
							<div class="table-responsive">
                                <table class="table ">
                                    <tbody id="current-order-item-list">
                                        <?php
                                         //https://stackoverflow.com/questions/23977298/where-is-a-woocommerce-order-placed-in-wordpress-database              
                                        $total_currentorders = $wpdb->get_results("SELECT
                                            p.ID as order_id,
                                            p.post_date,
                                            oi.order_item_id as order_items_id,
                                            oi.order_item_name as order_items
                                            from
                                                {$wpdb->prefix}posts p 
                                                join {$wpdb->prefix}postmeta pm on p.ID = pm.post_id
                                                join {$wpdb->prefix}woocommerce_order_items oi on p.ID = oi.order_id
                                                join {$wpdb->prefix}woocommerce_order_itemmeta oim on oi.order_item_id = oim.order_item_id
                                            where
                                                post_type = 'shop_order'  AND
                                                post_status = 'wc-processing' AND 
                                                oi.order_item_type = 'line_item' AND 
                                                pm.meta_key LIKE '_customer_user' AND pm.meta_value = '".$customer_user_id."' 
                                            group by oi.order_item_id 
                                            ORDER BY p.post_date DESC");

                                        echo $wpdb->last_error;
                                        echo  $wpdb->print_error();

                                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                        $offset = ($paged-1)*$per_page;                                     
                                        $order_data = $wpdb->get_results("SELECT
                                            p.ID as order_id,
                                            p.post_date,
                                            oi.order_item_id as order_items_id,
                                            oi.order_item_name as order_items
                                            from
                                                {$wpdb->prefix}posts p 
                                                join {$wpdb->prefix}postmeta pm on p.ID = pm.post_id
                                                join {$wpdb->prefix}woocommerce_order_items oi on p.ID = oi.order_id
                                                join {$wpdb->prefix}woocommerce_order_itemmeta oim on oi.order_item_id = oim.order_item_id
                                            where
                                                post_type = 'shop_order'  AND
                                                post_status = 'wc-processing' AND 
                                                oi.order_item_type = 'line_item' AND 
                                                pm.meta_key LIKE '_customer_user' AND pm.meta_value = '".$customer_user_id."' 
                                            group by oi.order_item_id 
                                            ORDER BY p.post_date DESC LIMIT ".$offset.", ".$per_page."");

                                        if ($order_data)
                                        {
                                           
                                            foreach ($order_data as $order) 
                                            {
                                               
                                                $product_id = wc_get_order_item_meta( $order->order_items_id, '_product_id', true );
                                                $item_word =  wc_get_order_item_meta( $order->order_items_id, 'pa_word-count', true );
                                                $item_post_count =  wc_get_order_item_meta( $order->order_items_id, 'pa_number-of-posts', true );
                                                $item_post = wc_get_order_item_meta( $order->order_items_id, 'pa_social-media', true );
                                                $item_qty =  wc_get_order_item_meta( $order->order_items_id, '_qty', true );

                                                $any_order_item_qty = 'No';
                                                 $complete_date = '';
                                                for($j=1;$j<=$item_qty;$j++)
                                                {  
                                                    $order_data = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}order_contents 
                                                    WHERE order_id = '".$order->order_id."' AND product_name LIKE '".$order->order_items."%' AND status in('sent','updated')"); 
                                                    if($order_data)
                                                    {
                                                        $any_order_item_qty = 'Yes';
                                                        $complete_date = $order_data->completed_date;
                                                    }
                                                } 
                                               ?>
                                               <tr>
                                                    <td class="nametitle">
                                                        <?php if(get_field( 'product_icon', $product_id )) { ?>
                                                        <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="stat-icon dib flat-color-1">
                                                            <div class="order-content">
                                                                <h3><?php echo $order->order_items; ?></h3>
                                                                <?php if($item_word){ ?>
                                                                <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                            <?php } ?>
                                                             <?php if($item_post_count){ ?>
                                                                <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                            <?php } ?>
                                                           
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="name_processing">  
                                                        <span class="name">
                                                        <?php if($any_order_item_qty == 'Yes') {  
                                                                    echo 'Completed: '. date('m/d/Y \A\t H:i A', strtotime($complete_date));
                                                         } else { echo 'Processing'; } ?>     
                                                        </span>
                                                    </td>
                                                    <td style="width: 200px;">
                                                        <div class="progress mb-3" style="height: 8px">
                                                            <?php if($any_order_item_qty == 'Yes') {  ?>
                                                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="20"></div>
                                                            <?php }else { ?>    
                                                                <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="20"></div>
                                                            <?php } ?>    
                                                        </div>
                                                    </td>
                                                    <td>
                                                       <?php if($any_order_item_qty == 'Yes') {  ?>
                                                    <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
                                                <?php } ?>
                                                    </td>
                                                </tr> 
                                               <?php
                                            }
                                        }
                                        else
                                        { echo "<div class='order-data-blank'><p>There is no orders found</p></div>"; }
                                        ?>
                                    </tbody>
                                </table>
								</div>
                            </div> <!-- /.table-stats -->
                                <?php  if(sizeof($total_currentorders) > $per_page){ ?>
                                    <div class="card-footer">
                                       <div id="current-order-item-loader" class="loading-banner" data-total="<?php echo sizeof($total_currentorders); ?>"><a class="btn" href="javascript:;">Load more Orders...</a></div>
                                    </div> 
                                <?php } ?>
                        </div>
                    </div>
                </div>      
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Order History,</strong>
                                <p>Record of your past order(s) and their completion date.</p>
                            </div>
                            <div class="order-search">
                                <form method="post" action="" name="order-search">
                                    <input type="text" name="order-date" value="<?php if($_POST['order-date']) echo $_POST['order-date'] ?>" placeholder="Date" id="order-date" >
                                    <input type="text" name="order-name" value="<?php if($_POST['order-name']) echo $_POST['order-name'] ?>" placeholder="Order Name" id="order-name">
                                    <input type="submit" name="submit" value="Search with Filter(s)...">
                                </form>
                            </div>
                            <div class="table-stats order-table ov-h">
							<div class="table-responsive">
                                <table class="table ">
                                    <tbody id="past-order-list">
                                        <?php 
                                        $odate = '';
                                         if($_POST['order-date']){
                                            $odate = date("Y-m-d", strtotime($_POST['order-date']));
                                        }
                                         //https://stackoverflow.com/questions/23977298/where-is-a-woocommerce-order-placed-in-wordpress-database              
                                        $total_pastorders = $wpdb->get_results("SELECT 
                                            p.ID as order_id,
                                            p.post_date,
                                            oi.order_item_id as order_items_id,
                                            oi.order_item_name as order_items
                                            FROM {$wpdb->prefix}posts p
                                            INNER JOIN {$wpdb->prefix}postmeta m1
                                             ON ( p.ID = m1.post_id )
                                            INNER JOIN {$wpdb->prefix}postmeta m2
                                             ON ( p.ID = m2.post_id )
                                            INNER JOIN {$wpdb->prefix}woocommerce_order_items oi 
                                             ON ( p.ID = oi.order_id)
                                             INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta oim 
                                             ON ( oi.order_item_id = oim.order_item_id)
                                            WHERE
                                            p.post_type = 'shop_order'
                                            AND post_status = 'wc-completed'
                                            AND oi.order_item_type = 'line_item' 
                                            AND ( m1.meta_key = '_customer_user' AND CAST(m1.meta_value AS DECIMAL) = '".$customer_user_id."' )
                                            AND ( m2.meta_key = '_completed_date' AND m2.meta_value LIKE '".$odate."%' )
                                            AND oi.order_item_name like '".$_POST['order-name']."%'
                                            GROUP BY oi.order_item_id 
                                            ORDER BY p.post_date DESC");

                                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                        $offset = ($paged-1)*$per_page;

                                            // https://kuttler.eu/en/code/custom-wordpress-sql-query-for-multiple-meta-values/
                                            $order_data = $wpdb->get_results("SELECT 
                                            p.ID as order_id,
                                            p.post_date,
                                            oi.order_item_id as order_items_id,
                                            oi.order_item_name as order_items
                                            FROM {$wpdb->prefix}posts p
                                            INNER JOIN {$wpdb->prefix}postmeta m1
                                             ON ( p.ID = m1.post_id )
                                            INNER JOIN {$wpdb->prefix}postmeta m2
                                             ON ( p.ID = m2.post_id )
                                            INNER JOIN {$wpdb->prefix}woocommerce_order_items oi 
                                             ON ( p.ID = oi.order_id)
                                             INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta oim 
                                             ON ( oi.order_item_id = oim.order_item_id)
                                            WHERE
                                            p.post_type = 'shop_order'
                                            AND post_status = 'wc-completed'
                                            AND oi.order_item_type = 'line_item' 
                                            AND ( m1.meta_key = '_customer_user' AND CAST(m1.meta_value AS DECIMAL) = '".$customer_user_id."' )
                                            AND ( m2.meta_key = '_completed_date' AND m2.meta_value LIKE '".$odate."%' )
                                            AND oi.order_item_name like '".$_POST['order-name']."%'
                                            GROUP BY oi.order_item_id 
                                            ORDER BY p.post_date
                                            DESC LIMIT ".$offset.", ".$per_page."");
                                        if ($order_data)
                                        { 
                                            foreach ($order_data as $order) 
                                            {
                                                $product_id = wc_get_order_item_meta( $order->order_items_id, '_product_id', true ); 
                                                $item_word =  wc_get_order_item_meta( $order->order_items_id, 'pa_word-count', true );
                                                $item_post_count =  wc_get_order_item_meta( $order->order_items_id, 'pa_number-of-posts', true );
                                                $item_post = wc_get_order_item_meta( $order->order_items_id, 'pa_social-media', true );
                                                $completed_date = get_post_meta( $order->order_id, '_completed_date', true );
                                               ?>
                                               <tr>
                                                    <td>
                                                        <?php if(get_field( 'product_icon', $product_id )) { ?>
                                                        <img src="<?php the_field( 'product_icon', $product_id ); ?>" alt="">
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="stat-icon dib flat-color-1">
                                                            <div class="order-content">
                                                                <h3><?php echo $order->order_items; ?></h3>
                                                                <?php if($item_word){ ?>
                                                                <p><?php  echo str_replace('-words', '', $item_word);  ?> Word Count</p>
                                                            <?php } ?>
                                                             <?php if($item_post_count){ ?>
                                                                <p><?php  echo $item_post;  ?> (<?php  echo str_replace('-posts', '', $item_post_count);  ?>)</p>
                                                            <?php } ?>
                                                           
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="name_processing">  
                                                        <?php if($completed_date) { ?>
                                                        <span class="name">
                                                        <?php echo 'Completed: '. date('m/d/Y \A\t H:i A', strtotime($completed_date)); ?>
                                                        </span>
                                                    <?php } ?>
                                                    </td>
                                                    <td style="width: 200px;">
                                                        <div class="progress mb-3" style="height: 8px">
                                                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="20"></div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span class="badge badge-complete"><a href="<?php echo site_url(); ?>/user-order-detail/">View Content</a></span>
                                                    </td>
                                                </tr> 
                                               <?php
                                            }
                                        }
                                        else
                                        { echo "<div class='order-data-blank'><p>There is no orders found</p></div>"; }
                                        ?>
                                    </tbody>
                                </table>
								</div>
                            </div> <!-- /.table-stats -->
                                <?php if(sizeof($total_pastorders) > $per_page){ ?>
                                    <div class="card-footer">
                                       <div id="past-order-loader" class="loading-banner" data-total="<?php echo sizeof($total_pastorders); ?>"><a class="btn" href="javascript:;">Load more Orders...</a></div>
                                    </div> 
                                <?php } ?>
                        </div>
                    </div>
                </div>        
                
                
            <!-- /#add-category -->
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
<script type="text/javascript">
jQuery(document).ready(function () {
    let pull_item = 1; let item_jsonFlag = true; let pagecount = <?php echo $per_page; ?>;
    jQuery("#current-order-item-loader").click(function(){
    if(item_jsonFlag)
    {  
        jQuery('.wc-loader').fadeIn();
        pull_item++; item_jsonFlag = false; 
        pagecount += <?php echo $per_page; ?>;
        var totalpage =  jQuery(this).data('total');
        //console.log(pagecount);
        //console.log(totalpage);
        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=current_order_lists_item&page=" +pull_item,
           success:function(data)
           {
                if(data)
                {
                   jQuery("#current-order-item-list").append(data);
                    item_jsonFlag = true;
                    jQuery('.wc-loader').fadeOut();
                 }
                else{
                    jQuery('#current-order-item-loader').hide(); 
                    jQuery('.wc-loader').fadeOut();
                }
                if ( pagecount >= totalpage ) {
                    jQuery('#current-order-item-loader').hide();
                } 
            }
        });
    }
    });

    let past_pull_page = 1; let past_jsonFlag = true;  let past_pagecount = <?php echo $per_page; ?>;
    jQuery("#past-order-loader").click(function(){
    if(past_jsonFlag)
    {  
        jQuery('.wc-loader').fadeIn();
        var odate = jQuery("#order-date").val();
        var oname = jQuery("#order-name").val();
        past_pull_page++; past_jsonFlag = false;
        past_pagecount += <?php echo $per_page; ?>;
        var totalpage =  jQuery(this).data('total');
        //console.log(past_pagecount);
       // console.log(totalpage);
        jQuery.ajax({
           type:"post",
           url:"<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
           data:"action=past_order_lists&page=" +past_pull_page + "&order-date=" +odate + "&order-name=" +oname,
           success:function(data)
           {
                if(data)
                {
                   jQuery("#past-order-list").append(data);
                    past_jsonFlag = true;
                    jQuery('.wc-loader').fadeOut();
                }
                else{
                    jQuery('#past-order-loader').hide(); 
                    jQuery('.wc-loader').fadeOut();
                }
                if (past_pagecount >= totalpage ) {
                    jQuery('#past-order-loader').hide();
                }  
            }
        });
    }
    });

    

});
</script>

  
<?php get_footer('user'); ?>