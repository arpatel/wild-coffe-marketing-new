<?php 

/* Template name: User Template */

get_header('user'); 
?>
<?php get_sidebar('user'); ?>
<?php get_header('user-top'); ?>
        <!-- Content -->
        <div class="content">
<?php 
    
    // Get content width and sidebar position
    

?>
<div class="row">
                    <div class="col-lg-12">
                        <div class="card">


        <?php /* The loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages(); ?>
                    </div>

                    <?php basel_entry_meta(); ?>

                </article><!-- #post -->

              

        <?php endwhile; ?>

</div></div></div>

             
        </div>
        <!-- /.content -->
      
      
<?php get_footer('user'); ?>